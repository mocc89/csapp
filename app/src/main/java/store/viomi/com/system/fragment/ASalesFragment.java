package store.viomi.com.system.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.SalesReportAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.SalesData;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.widget.PinnedSectionListView;

/**
 * Created by viomi on 2016/10/18.
 */


@ContentView(R.layout.a_sales_fragment)
public class ASalesFragment extends BaseFragment {

    @ViewInject(R.id.sale_fg_loading)
    private RelativeLayout sale_fg_loading;
    @ViewInject(R.id.pslistView)
    private PinnedSectionListView pslistView;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;


    private boolean isOK1 = false;
    private boolean isOK2 = false;

    private String result1 = "";
    private String result2 = "";

    private String typeNO;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasload;
    private Callback.Cancelable cancelable1;
    private Callback.Cancelable cancelable2;

    private boolean isdestroy;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!isdestroy) {
                switch (msg.what) {
                    case 0:
                        isOK1 = true;
                        result1 = (String) msg.obj;
                        LogUtil.mlog("viomi1", result1);
                        parseJson();
                        break;
                    case 1:
                        isOK2 = true;
                        result2 = (String) msg.obj;
                        LogUtil.mlog("viomi2", result2);

                        parseJson();
                        break;
                    case 2:
                        loadingfinish();
                        reconnect_layout.setVisibility(View.VISIBLE);

                        if (cancelable1 != null) {
                            cancelable1.cancel();
                        }
                        if (cancelable2 != null) {
                            cancelable2.cancel();
                        }
                        break;
                }
            }
        }
    };


    public static Fragment getInstance(String type) {
        ASalesFragment fragment = new ASalesFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNO = arguments.getString("type", "null");
        }
        isPrepared = true;
        if (isVisible && isPrepared) {
            loadData(typeNO);
        }
    }

    private void loadData(String type) {

        RequestParams requestParams1 = RequstUtils.getHasTokenInstance(MURL.SALESREPORTHEADER);
        requestParams1.addBodyParameter("type", type);
        loading();
        cancelable1 = RequstUtils.getRquest(requestParams1, mhandler, 0, 2);

        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST1);
        requestParams2.addBodyParameter("type", type);
        cancelable2 = RequstUtils.getRquest(requestParams2, mhandler, 1, 2);
    }

    private void parseJson() {
        if (isOK1 && isOK2) {
            loadingfinish();
            reconnect_layout.setVisibility(View.GONE);
            hasload = true;

            String order_count_txt = "0";
            String sales_count_txt = "0";
            String firstpercent_txt = "0";
            String secondpercent_txt = "0";
            String storepercent_txt = "0";
            String salesfee_txt = "0";
            List<SalesData> dataList = new ArrayList<>();

            dataList.add(new SalesData(0, "渠道名称", "订单数", "销售金额", "提成金额"));

            try {
                JSONObject json1 = new JSONObject(result1);
                JSONObject json2 = new JSONObject(result2);

                JSONObject mobBaseRes1 = JsonUitls.getJSONObject(json1, "mobBaseRes");
                String code1 = JsonUitls.getString(mobBaseRes1, "code");
                if ("100".equals(code1)) {

                    JSONObject overAll = JsonUitls.getJSONObject(mobBaseRes1, "overAll");
                    JSONObject top = JsonUitls.getJSONObject(mobBaseRes1, "top");
                    JSONObject second = JsonUitls.getJSONObject(mobBaseRes1, "second");
                    JSONObject terminal = JsonUitls.getJSONObject(mobBaseRes1, "terminal");

                    order_count_txt = JsonUitls.getString(overAll, "orderCount");
                    sales_count_txt = "¥ " + JsonUitls.getString(overAll, "turnOver");
                    firstpercent_txt = "¥ " + JsonUitls.getString(top, "profit");
                    secondpercent_txt = "¥ " + JsonUitls.getString(second, "profit");
                    storepercent_txt = "¥ " + JsonUitls.getString(terminal, "profit");
                    String salesFee_raw = JsonUitls.getString(top, "salesFee");
                    salesFee_raw = "-".equals(salesFee_raw) ? "0.0" : salesFee_raw;
                    salesfee_txt = "¥ " + salesFee_raw;
                }


                JSONObject mobBaseRes2 = JsonUitls.getJSONObject(json2, "mobBaseRes");
                String code2 = JsonUitls.getString(mobBaseRes2, "code");
                if ("100".equals(code2)) {
                    JSONArray subReport = JsonUitls.getJSONArray(mobBaseRes2, "datas");
                    for (int i = 0; i < subReport.length(); i++) {
                        JSONObject item = subReport.getJSONObject(i);
                        JSONObject channelInfo = JsonUitls.getJSONObject(item, "channelInfo");
                        JSONObject salesReportBean = JsonUitls.getJSONObject(item, "salesReportBean");

                        String name = JsonUitls.getString(channelInfo, "name");
                        String order = JsonUitls.getString(salesReportBean, "orderCount");
                        String sales = "¥ " + JsonUitls.getString(salesReportBean, "turnOver");
                        String percentage = "¥ " + JsonUitls.getString(salesReportBean, "profit");
                        SalesData data = new SalesData(1, name, order, sales, percentage);
                        dataList.add(data);
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            setMyAdapter(order_count_txt, sales_count_txt, firstpercent_txt, secondpercent_txt, storepercent_txt, salesfee_txt, dataList);

        }
    }

    private void setMyAdapter(String order_count_txt, String sales_count_txt, String firstpercent_txt, String secondpercent_txt, String storepercent_txt, String salesfee_txt, List<SalesData> dataList) {
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.sales_report_header, null);

        //订单数量
        TextView order_count = (TextView) headerView.findViewById(R.id.order_count);
        //销售金额
        TextView sales_count = (TextView) headerView.findViewById(R.id.sales_count);
        //一级渠道提成金额
        TextView firstpercent = (TextView) headerView.findViewById(R.id.firstpercent);
        //二级渠道提成金额
        TextView secondpercent = (TextView) headerView.findViewById(R.id.secondpercent);
        //门店提成金额
        TextView storepercent = (TextView) headerView.findViewById(R.id.storepercent);
        //销售费用
        TextView salesfee = (TextView) headerView.findViewById(R.id.salesfee);

        order_count.setText(order_count_txt);
        sales_count.setText(sales_count_txt);
        firstpercent.setText(firstpercent_txt);
        secondpercent.setText(secondpercent_txt);
        storepercent.setText(storepercent_txt);
        salesfee.setText(salesfee_txt);

        pslistView.addHeaderView(headerView);

        SalesReportAdapter adapter = new SalesReportAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);

    }


    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(typeNO);
            }
        });
    }


    //处理预加载
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            if (isVisible && isPrepared && !hasload) {
                loadData(typeNO);
            }
        } else {
            isVisible = false;

        }
    }

    @Override
    protected void loading() {
        sale_fg_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        sale_fg_loading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mhandler.removeCallbacksAndMessages(null);
        isdestroy = true;
    }
}