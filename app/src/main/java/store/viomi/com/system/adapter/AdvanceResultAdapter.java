package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.AdvanceREntity;

/**
 * Created by viomi on 2017/1/9.
 */

public class AdvanceResultAdapter extends BaseAdapter {

    private List<AdvanceREntity> list;
    private Context context;
    private LayoutInflater inflater;

    public AdvanceResultAdapter(List<AdvanceREntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.advance_result_item_layout, null);
            holder = new ViewHolder();
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.store_name = (TextView) convertView.findViewById(R.id.store_name);
            holder.agency_name = (TextView) convertView.findViewById(R.id.agency_name);
            holder.city_name = (TextView) convertView.findViewById(R.id.city_name);
            holder.cleaner_count = (TextView) convertView.findViewById(R.id.cleaner_count);
            holder.store_order_count = (TextView) convertView.findViewById(R.id.store_order_count);
            holder.store_order_sum = (TextView) convertView.findViewById(R.id.store_order_sum);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.date.setText(list.get(position).getDay());
        holder.store_name.setText(list.get(position).getChannelName());
        holder.agency_name.setText(list.get(position).getSecondChannel());
        holder.city_name.setText(list.get(position).getRootChannel());
        holder.cleaner_count.setText(list.get(position).getPurchaseMachineCount());
        holder.store_order_count.setText(list.get(position).getPurchaseOrderCount());
        holder.store_order_sum.setText("¥"+list.get(position).getPurchaseOrderAmount());

        return convertView;
    }

    class ViewHolder {
        TextView date, store_name, agency_name, city_name, cleaner_count, store_order_count, store_order_sum;
    }
}
