package store.viomi.com.system.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.fragment.OrderListFragment;
import store.viomi.com.system.fragment.OrderListThirdFragment;
import store.viomi.com.system.fragment.TimePickerFragment;
import store.viomi.com.timepickerlibrary.timepicker.DateUtils;
import store.viomi.com.timepickerlibrary.timepicker.WheelView;

public class AdvanceOrder2FilterActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.select)
    TextView select;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.line1)
    View line1;
    @BindView(R.id.line2)
    View line2;
    @BindView(R.id.line3)
    View line3;
    @BindView(R.id.top)
    LinearLayout top;
    @BindView(R.id.layout_nian)
    LinearLayout layoutNian;
    @BindView(R.id.layout_wheel)
    FrameLayout layoutWheel;
    @BindView(R.id.activity_advance_order)
    RelativeLayout activityAdvanceOrder;
    private long startTime, endtime;
    TimePickerFragment fragmentYue;
    TimePickerFragment fragmentRi;
    Fragment currentFragment;

    @Override
    protected void init() {
        setContentView(R.layout.activity_advance_order_2_filter);
        ButterKnife.bind(this);
        fragmentYue = new TimePickerFragment();
        fragmentYue.setModeNianYue();
        fragmentRi = new TimePickerFragment();
        setNavigation(tv3, tv2, tv1, line3, line2, line1, 3);
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    startTime = 1451577600; // 2016/1/1 00:00:00
                    endtime = System.currentTimeMillis() / 1000;
                } else if (type == 2) {
                    if (startTime > System.currentTimeMillis() / 1000) {
                        Toast.makeText(activity, "请选择正确的开始时间。", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    startTime = fragmentYue.getStartTime();
                    endtime = fragmentYue.getEndTime();
                } else if (type == 3) {
                    startTime = fragmentRi.getStartTime();
                    if (startTime > System.currentTimeMillis() / 1000) {
                        Toast.makeText(activity, "请选择正确的开始时间。", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    endtime = fragmentRi.getEndTime();
                }
                Intent intent = new Intent();
                intent.putExtra("start", startTime);
                intent.putExtra("end", endtime);
                intent.putExtra("timeType", type);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv1, tv2, tv3, line1, line2, line3, 1);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv2, tv1, tv3, line2, line1, line3, 2);
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv3, tv2, tv1, line3, line2, line1, 3);
            }
        });
    }


    int type = 3;

    private void setNavigation(TextView tv1, TextView tv2, TextView tv3, View line1, View line2, View line3, int type) {
        tv1.setTextColor(getResources().getColor(R.color.selectItem));
        tv2.setTextColor(getResources().getColor(R.color.unselectItem));
        tv3.setTextColor(getResources().getColor(R.color.unselectItem));
        line1.setVisibility(View.VISIBLE);
        line2.setVisibility(View.INVISIBLE);
        line3.setVisibility(View.INVISIBLE);
        this.type = type;
        if (type == 1) {
            layoutWheel.setVisibility(View.GONE);
            layoutNian.setVisibility(View.VISIBLE);
            return;
        } else if (type == 2) {
            layoutWheel.setVisibility(View.VISIBLE);
            layoutNian.setVisibility(View.GONE);
        } else if (type == 3) {
            layoutWheel.setVisibility(View.VISIBLE);
            layoutNian.setVisibility(View.GONE);
        }

        Fragment fragment = null;
        switch (type) {
            case 2: {
                fragment = fragmentYue;
                break;
            }
            case 3: {
                fragment = fragmentRi;
                break;
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (currentFragment != null)
            transaction.hide(currentFragment);
        if (fragment.isAdded()) {
            transaction.show(fragment);
        } else {
            transaction.add(R.id.layout_wheel, fragment);
        }
        transaction.commit();
        currentFragment = fragment;
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

}
