package store.viomi.com.system.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.adapter.DataSpinnerAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.Area;
import store.viomi.com.system.bean.OrderSearchParameter;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;
import store.viomi.com.system.widget.DatePickerTwo;

public class OrderSelectNewActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.select)
    TextView select;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.t3)
    TextView t3;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.order_source)
    RelativeLayout orderSource;
    @BindView(R.id.t1)
    TextView t1;
    @BindView(R.id.start_txt)
    TextView startTxt;
    @BindView(R.id.start_time)
    RelativeLayout startTime;
    @BindView(R.id.t4)
    TextView t4;
    @BindView(R.id.edt_no)
    EditText edtNo;
    @BindView(R.id.t5)
    TextView t5;
    @BindView(R.id.spinner_type)
    Spinner spinnerType;
    @BindView(R.id.t6)
    TextView t6;
    @BindView(R.id.tv_product_na)
    EditText tvProductNa;
    @BindView(R.id.t7)
    TextView t7;
    @BindView(R.id.tv_shop_name)
    EditText tvShopName;
    @BindView(R.id.t8)
    TextView t8;
    @BindView(R.id.spinner_sheng)
    Spinner spinnerSheng;
    @BindView(R.id.t9)
    TextView t9;
    @BindView(R.id.spinner_shi)
    Spinner spinnerShi;
    @BindView(R.id.t10)
    TextView t10;
    @BindView(R.id.spinner_qu)
    Spinner spinnerQu;
    @BindView(R.id.reconnect_btn)
    TextView reconnectBtn;
    @BindView(R.id.reconnect_layout)
    RelativeLayout reconnectLayout;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.loading)
    RelativeLayout loading;
    @BindView(R.id.layout_no)
    RelativeLayout layoutNo;
    @BindView(R.id.layout_status)
    RelativeLayout layoutStatus;
    @BindView(R.id.layout_product_name)
    RelativeLayout layoutProductName;
    @BindView(R.id.layout_seller_name)
    RelativeLayout layoutSellerName;
    @BindView(R.id.layout_addr_provice)
    RelativeLayout layoutAddrProvice;
    @BindView(R.id.layout_addr_city)
    RelativeLayout layoutAddrCity;
    @BindView(R.id.layout_addr_district)
    RelativeLayout layoutAddrDistrict;
    private long starttime;
    private String starttimeshow = "";
    private long endtime;
    private String endtimeshow = "";
    private List<String> datas;
    private List<String> datasZhuangtai;
    private List<String> datasSheng;
    private List<String> datasShi;
    private List<String> datasQu;
    private int orderStatus = -1;
    private int souceType = -1;
    List<Area> provice = new ArrayList<>();
    List<Area> city = new ArrayList<>();
    List<Area> districts = new ArrayList<>();
    Area selectedProvice, selectedCity, selectedDisitrict;

    Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0://success
                    loadingfinish();
                    String result = (String) message.obj;
                    parseJSON(result);
                    break;
                case 1://fail
                    loadingfinish();
                    ResponseCode.onErrorHint(message.obj);
                    break;
            }
            return false;
        }
    });

    private void parseJSON(String result) {
        List<Area> list = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(result);
            jsonObject = jsonObject.optJSONObject("mobBaseRes");
            JSONArray jsonArray = jsonObject.optJSONArray("result");
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                    Area area = new Area();
                    area.id = jsonObject1.optInt("id");
                    area.version = jsonObject1.optInt("version");
                    area.code = jsonObject1.optInt("code");
                    area.parent = jsonObject1.optInt("parent");
                    area.level = jsonObject1.optInt("level");
                    area.display = jsonObject1.optInt("display");
                    area.name = jsonObject1.optString("name");
                    list.add(area);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        if (spinnermode == 0) {//省
            provice.clear();
            Area area1 = new Area();
            area1.code = -1;
            area1.name = "全部";
            provice.add(area1);
            provice.addAll(list);
            List<String> listP = new ArrayList<>();
            for (Area area : provice) {
                listP.add(area.name);
            }
            DataSpinnerAdapter proviceAdapter = new DataSpinnerAdapter(this, listP);
            spinnerSheng.setAdapter(proviceAdapter);
        } else if (spinnermode == 1) {//市
            city.clear();
            city.addAll(list);
            List<String> listC = new ArrayList<>();
            for (Area area : city) {
                listC.add(area.name);
            }
            DataSpinnerAdapter proviceAdapter = new DataSpinnerAdapter(this, listC);
            spinnerShi.setAdapter(proviceAdapter);
        } else {//区
            districts.clear();
            districts.addAll(list);
            List<String> listD = new ArrayList<>();
            for (Area area : districts) {
                listD.add(area.name);
            }
            DataSpinnerAdapter proviceAdapter = new DataSpinnerAdapter(this, listD);
            spinnerQu.setAdapter(proviceAdapter);
        }
    }

    @Override
    protected void init() {
        setContentView(R.layout.activity_order_select_new);
        ButterKnife.bind(this);
        initData();
        endtime = System.currentTimeMillis() / 1000;
        starttime = endtime - 60L * 60L * 24L * 30L;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            starttime = sdf1.parse(sdf1.format(new Date(starttime * 1000L))).getTime() / 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startTxt.setText(sdf1.format(new Date(starttime * 1000L)) + "至" + sdf1.format(new Date(endtime * 1000L)));
        loadData();
    }

    void initData() {
        datas = new ArrayList<String>();
        datas.add("云米商城");
        datas.add("米家");
        datas.add("天猫");
        datas.add("预付款提货");
        DataSpinnerAdapter selectAdapter = new DataSpinnerAdapter(this, datas);
        spinner.setAdapter(selectAdapter);

        datasZhuangtai = new ArrayList();
        datasZhuangtai.add("全部");
        datasZhuangtai.add("待发货");
        datasZhuangtai.add("已发货");
        datasZhuangtai.add("待审核");
        datasZhuangtai.add("待退款");
        datasZhuangtai.add("交付分仓发货");
        datasZhuangtai.add("待销管审核");

        DataSpinnerAdapter zhuangtaiAdapter = new DataSpinnerAdapter(this, datasZhuangtai);
        spinnerType.setAdapter(zhuangtaiAdapter);
    }

    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        startTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerTwo picker = new DatePickerTwo(OrderSelectNewActivity.this);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = (c.get(Calendar.MONTH));
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }

                picker.setOnDatePickTwoListener(new DatePickerTwo.OnDatePickTwoListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day, String year_end, String month_end, String day_end) {
                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                        try {
                            starttime = sdf1.parse(year + "-" + month + "-" + day + " 00:00:00.000").getTime() / 1000L;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        try {
                            endtime = sdf1.parse(year_end + "-" + month_end + "-" + day_end + " 23:59:59.999").getTime() / 1000L;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        starttimeshow = year + "-" + month + "-" + day + " 至 " + year_end + "-" + month_end + "-" + day_end;
                        startTxt.setText(starttimeshow);
                    }
                });
                picker.show();
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                souceType = position;
                if (position == 3) {//预付款提货
                    layoutAddrProvice.setVisibility(View.VISIBLE);
                    layoutAddrCity.setVisibility(View.VISIBLE);
                    layoutAddrDistrict.setVisibility(View.VISIBLE);
                    layoutNo.setVisibility(View.VISIBLE);
                    layoutProductName.setVisibility(View.VISIBLE);
                    layoutSellerName.setVisibility(View.VISIBLE);
                    layoutStatus.setVisibility(View.VISIBLE);
                } else {
                    layoutAddrProvice.setVisibility(View.GONE);
                    layoutAddrCity.setVisibility(View.GONE);
                    layoutAddrDistrict.setVisibility(View.GONE);
                    layoutNo.setVisibility(View.GONE);
                    layoutProductName.setVisibility(View.GONE);
                    layoutSellerName.setVisibility(View.GONE);
                    layoutStatus.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                orderStatus = position;
                if (position == 5) {
                    orderStatus = 6;
                } else if (position == 6) {
                    orderStatus = 8;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerSheng.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedProvice = provice.get(position);
                spinnermode = 1;
                parentCode = selectedProvice.code;
                city.clear();
                districts.clear();
                selectedCity = null;
                selectedDisitrict = null;
                DataSpinnerAdapter cityAdapter = new DataSpinnerAdapter(activity, null);
                spinnerShi.setAdapter(cityAdapter);
                DataSpinnerAdapter districtAdapter = new DataSpinnerAdapter(activity, null);
                spinnerQu.setAdapter(districtAdapter);
                loadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerShi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = city.get(position);
                spinnermode = 2;
                parentCode = selectedCity.code;
                loadData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerQu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedDisitrict = districts.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (starttime == 0) {
                    ToastUtil.show("请选择开始时间");
                    return;
                }
                if (endtime == 0) {
                    ToastUtil.show("请选择结束时间");
                    return;
                }
                Intent intent = new Intent();
                OrderSearchParameter orderSearchParameter = new OrderSearchParameter();

                orderSearchParameter.souceType = souceType;
                orderSearchParameter.start = starttime;
                orderSearchParameter.end = endtime;
                orderSearchParameter.orderNo = edtNo.getText().toString().trim();
                orderSearchParameter.orderStatus = orderStatus;
                orderSearchParameter.productName = tvProductNa.getText().toString().trim();
                orderSearchParameter.sellerName = tvShopName.getText().toString().trim();
                orderSearchParameter.city = selectedCity;
                orderSearchParameter.province = selectedProvice;
                orderSearchParameter.district = selectedDisitrict;

                intent.putExtra("param", orderSearchParameter);
                setResult(RESULT_OK, intent);
                finish();
//                Intent intent = new Intent(OrderSelectNewActivity.this, OrderSearchResultActivity.class);
//                intent.putExtra("starttime", starttime);
//                intent.putExtra("endtime", endtime);
//                intent.putExtra("souceType", souceType);
//                startActivity(intent);
            }
        });
    }

    int parentCode = 0;
    /**
     * 0省，1市 2区
     */
    int spinnermode;

    private void loadData() {
        if (parentCode < 0)
            return;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ORDER_AREA);

        requestParams.addBodyParameter("channelId", 1 + "");
        boolean chooseAll = true;
        if (parentCode > 0)
            chooseAll = false;
        requestParams.addBodyParameter("chooseAll", chooseAll + "");

        requestParams.addBodyParameter("parentCode", parentCode + "");
        loading();
        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

}
