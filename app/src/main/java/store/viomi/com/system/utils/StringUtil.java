package store.viomi.com.system.utils;

public class StringUtil {

    public static String getDateDay(String str){
        int i = str.indexOf("-");
        String substring = str.substring(i+1, str.length());
        return substring.replace("-","/");
    }
}
