package store.viomi.com.system.activity;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.fragment.OrderListFragment;
import store.viomi.com.system.fragment.OrderListThirdFragment;

@ContentView(R.layout.activity_order_search_result)
public class OrderSearchResultActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @Override
    protected void init() {
        Intent intent = getIntent();
        String starttime = intent.getStringExtra("starttime");
        String endtime = intent.getStringExtra("endtime");
        String souceType = intent.getStringExtra("souceType");

        switch (souceType) {
            case "0": {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.lin, OrderListFragment.getInstance(starttime,endtime));
                transaction.commit();
                break;
            }
            case "1": {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.lin, OrderListThirdFragment.getInstance("1",starttime,endtime));
                transaction.commit();
                break;
            }
            case "2": {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.lin, OrderListThirdFragment.getInstance("2",starttime,endtime));
                transaction.commit();
                break;
            }
        }

    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }
}
