package store.viomi.com.system.constants;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by viomi on 2016/11/22.
 * 上线前 检查三次检查三次检查三次
 * 还有版本更新页@UpdateActivity 更新UI
 */

public class Config {

    //当前版本号
    private final static int CURRENTVERSION = 222;
    //是否调试模式 发布false
    public final static boolean DEBUG_MODE = true;

    //测试服更新参数
//    public final static String UPDATEENVIRONMENT = "viomi_test";
    //正式服更新参数
    public final static String UPDATEENVIRONMENT = "viomi";


    //开发
//    public final static String BaseUrl = "http://192.168.1.250/services/";//废弃
    //    测试
//    public final static String BaseUrl = "https://vj.viomi.com.cn/services/";//没有
    //生产
    public final static String BaseUrl = "https://s.viomi.com.cn/services/";

    //    public final static String BaseUrl2 = "http://ms.viomi.com.cn/analysis/";
    public static String getCurrentversion(Context context) {
        int code = 0;
        try {
            code = context.getPackageManager().
                    getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (code == 0)
            code = CURRENTVERSION;
        return code + "";
    }
}
