package store.viomi.com.system.widget;

/**
 * Created by Mocc on 2017/10/10
 */

public interface ScrollViewListener {
    void onScrollChanged(int x, int y, int oldx, int oldy);
}
