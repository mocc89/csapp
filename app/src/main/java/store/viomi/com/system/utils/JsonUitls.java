package store.viomi.com.system.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by viomi on 2016/10/25.
 * json解析封装
 */

public class JsonUitls {

    public static String getString(JSONObject json, String name) {
        String result;
        if (json != null) {
            result = json.optString(name, "-");
        } else {
            result = "-";
        }
        result = "null".equals(result) ? "-" : result;
        return result;
    }

    public static JSONObject getRawJSONObject(String json) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
            return jsonObject;
        }
        return jsonObject;
    }

    public static JSONObject getJSONObject(JSONObject json, String name) {
        JSONObject jsonObject = null;

        if (json != null)
            jsonObject = json.optJSONObject(name);
        if (jsonObject == null)
            jsonObject = new JSONObject();
        return jsonObject;
    }

    public static JSONObject getJSONObjectFromArray(JSONArray array, int i) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = array.getJSONObject(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONArray getJSONArray(JSONObject json, String name) {
        JSONArray jsonArray = null;

        if (json != null)
            jsonArray = json.optJSONArray(name);
        if (jsonArray == null)
            jsonArray = new JSONArray();
        return jsonArray;
    }

    public static int getInt(JSONObject json, String name) {
        int result = 0;
        if (json == null) {
            return result;
        }
        result = json.optInt(name);
        return result;
    }

    public static double getDouble(JSONObject json, String name) {
        double result = 0d;
        if (json == null) {
            return result;
        }
        result = json.optDouble(name);
        return result;
    }

    public static long getLong(JSONObject json, String name) {
        long result = 0L;
        if (json == null) {
            return result;
        }
        result = json.optLong(name);
        return result;
    }

}
