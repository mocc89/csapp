package store.viomi.com.system.utils;

import android.os.Handler;
import android.os.Message;

import org.xutils.common.Callback;
import org.xutils.common.util.KeyValue;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import store.viomi.com.system.base.BaseApplication;

/**
 * Created by viomi on 2016/10/25.
 * 请求封装
 */

public class RequstUtils {


    //返回带token的请求体
    public static RequestParams getHasTokenInstance(String url) {
//        ToastUtil.show(url);
        RequestParams requestParams = new RequestParams(url);
        requestParams.setConnectTimeout(10 * 1000);
        requestParams.addBodyParameter("token", SharePreferencesUtils.getInstance().getToken(BaseApplication.getApp()));
        return requestParams;
    }


    //返回不带token的请求体
    public static RequestParams getNoTokenInstance(String url) {
        RequestParams requestParams = new RequestParams(url);
        requestParams.setConnectTimeout(10 * 1000);

        return requestParams;
    }


    //get请求封装---------------仅测试用-------------真家伙在下面↓↓↓↓↓↓
    public static Callback.Cancelable getRquest(RequestParams requestParams, final Handler mhandler, final int successCode, final int failCode, final String tag) {
        Callback.Cancelable cancelable = x.http().get(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Message msg = mhandler.obtainMessage();
                msg.obj = tag + result;
                msg.what = successCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Message msg = mhandler.obtainMessage();
                msg.obj = ex;
                msg.what = failCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });

        return cancelable;
    }


    //get请求封装
    public static Callback.Cancelable getRquest(RequestParams requestParams, final Handler mhandler, final int successCode, final int failCode) {
        String url = requestParams.getUri();
        List<KeyValue> keys = requestParams.getQueryStringParams();
        if (keys != null && keys.size() > 0) {
            url += "?";
            for (int i = 0; i < keys.size(); i++) {
                url += keys.get(i).key + "=" + keys.get(i).value;
                if (i != keys.size() - 1) {
                    url += "&";
                }
            }
        }
        requestParams.setConnectTimeout(60 * 1000);
        LogUtil.mlog("getRquest", url);
        Callback.Cancelable cancelable = x.http().get(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                LogUtil.mlog("getRquest", result);
                Message msg = mhandler.obtainMessage();
                msg.obj = result;
                msg.what = successCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                LogUtil.mlog("onError", ex.getMessage());
                Message msg = mhandler.obtainMessage();
                msg.obj = ex;
                msg.what = failCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                LogUtil.mlog("onCancelled", cex.getMessage());
            }

            @Override
            public void onFinished() {

            }
        });
        return cancelable;
    }

    //post请求封装
    public static Callback.Cancelable postRquest(RequestParams requestParams, final Handler mhandler, final int successCode, final int failCode) {
        LogUtil.mlog("postRquest", requestParams.getUri());
        Callback.Cancelable cancelable = x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                LogUtil.mlog("postRquest onSuccess", result);
                Message msg = mhandler.obtainMessage();
                msg.obj = result;
                msg.what = successCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                LogUtil.mlog("postRquest onError", ex.getMessage());
                Message msg = mhandler.obtainMessage();
                msg.obj = ex;
                msg.what = failCode;
                mhandler.sendMessage(msg);
            }

            @Override
            public void onCancelled(CancelledException cex) {
                LogUtil.mlog("postRquest onError", cex.getMessage());
            }

            @Override
            public void onFinished() {

            }
        });

        return cancelable;
    }


    //put请求喔
    public static String doPut(String url) {

        BufferedReader in = null;
        String result = "";
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(5000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);

            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

}
