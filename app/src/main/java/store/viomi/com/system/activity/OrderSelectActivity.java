package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import store.viomi.com.system.R;
import store.viomi.com.timepickerlibrary.timepicker.DatePicker;

public class OrderSelectActivity extends AppCompatActivity {

    private ImageView back;
    private TextView start_txt;
    private TextView end_txt;
    private Button select;

    private String starttime = "";
    private String starttimeshow = "";
    private String endtime = "";
    private String endtimeshow = "";

    private final int RESULTCODE = 1006;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_select);

        back = (ImageView) findViewById(R.id.back);
        start_txt = (TextView) findViewById(R.id.start_txt);
        end_txt = (TextView) findViewById(R.id.end_txt);
        select = (Button) findViewById(R.id.select);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        start_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(OrderSelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2025, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = (c.get(Calendar.MONTH));
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }

                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        starttime = year + "-" + month + "-" + day + " 00:00";
                        starttimeshow = year + "-" + month + "-" + day;
                        start_txt.setText(starttimeshow);
                    }
                });
                picker.show();
            }
        });

        end_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(OrderSelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2025, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }


                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        endtime = year + "-" + month + "-" + day + " 23:59";
                        endtimeshow = year + "-" + month + "-" + day;
                        end_txt.setText(endtimeshow);
                    }
                });
                picker.show();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (starttime.isEmpty()) {
//                    ToastUtil.show("请选择开始时间");
//                    return;
//                }
//                if (endtime.isEmpty()) {
//                    ToastUtil.show("请选择结束时间");
//                    return;
//                }

                Intent intent = new Intent();
                intent.putExtra("starttime", starttime);
                intent.putExtra("endtime", endtime);
                setResult(RESULTCODE, intent);

                finish();
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}
