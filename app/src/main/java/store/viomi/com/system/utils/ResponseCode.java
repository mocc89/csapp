package store.viomi.com.system.utils;

import android.content.Context;
import android.content.Intent;

import store.viomi.com.system.constants.HintText;

/**
 * Created by viomi on 2016/10/25.
 * 返回码封装处理
 */

public class ResponseCode {


    public static boolean isSuccess(String code, String desc) {

        boolean issuccess = false;

        switch (code) {
            //处理成功
            case "100": {
                issuccess = true;
                break;
            }

            //token值为空
            case "918": {
                issuccess = false;
                ToastUtil.show(desc);
            }

            //登录已过期
            case "919": {
                issuccess = false;
                ToastUtil.show(desc);

            }

            default: {
                issuccess = false;
                ToastUtil.show(desc);
                break;
            }
        }
        return issuccess;
    }

    public static boolean isSuccess2(String code, String desc) {

        boolean issuccess = false;

        switch (code) {
            //处理成功
            case "100": {
                issuccess = true;
                break;
            }

            //token值为空
            case "918": {
                issuccess = false;
                ToastUtil.show(desc);
            }

            //登录已过期
            case "919": {
                issuccess = false;
                ToastUtil.show(desc);

            }

            default: {
                issuccess = false;
                if (desc != null && desc.contains("无法识别")) {
                    ToastUtil.show("天猫订单功能尚未开通");
                } else {
                    ToastUtil.show(desc);
                }

                break;
            }
        }
        return issuccess;
    }

    public static void onErrorHint(Object obj) {

        String result = obj.toString();
        if (result == null) {
            result = "";
        }
        int type = 0;

        //网络连接超时
        if (result.contains("SocketTimeoutException")) {
            type = 1;
        }

        //没有网络
        if (result.contains("Network is unreachable")) {
            type = 2;
        }

        //没有网络
        if (result.contains("UnknownHostException")) {
            type = 2;
        }

        //服务器端
        if (result.contains("errorCode")) {
            type = 3;
        }

        switch (type) {
            case 0:
                ToastUtil.show(HintText.CONNECTFAIL);
                break;
            case 1:
                ToastUtil.show(HintText.CONNECTTIMEOUT);
                break;
            case 2:
                ToastUtil.show(HintText.CONNECTFAIL);
                break;
            case 3:
                ToastUtil.show(HintText.SERVICEERROR);
                break;
        }
    }

    //重启app
    private void restartApplication(Context context) {
        final Intent intent = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }
}
