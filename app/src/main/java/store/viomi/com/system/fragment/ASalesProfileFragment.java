package store.viomi.com.system.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.SalesProfileAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.ProfileBean;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.other.LebalSet;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.NumberUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.widget.PinnedSectionListView;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;

/**
 * Created by viomi on 2016/11/11.
 */

@ContentView(R.layout.a_sales_profile_fragment)
public class ASalesProfileFragment extends BaseFragment {

    @ViewInject(R.id.sale_fg_loading)
    private RelativeLayout sale_fg_loading;
    @ViewInject(R.id.pslistView)
    private PinnedSectionListView pslistView;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    private String typeNO;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasload;

    private boolean isdestroy = false;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    if (!isdestroy) {
                        loadingfinish();
                        hasload = true;
                        String result1 = (String) msg.obj;
                        LogUtil.mlog("oook1", result1);
                        reconnect_layout.setVisibility(View.GONE);
                        parseJson(result1);
                    }
                    break;
                case 1:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };


    public static Fragment getInstance(String type) {
        ASalesProfileFragment fragment = new ASalesProfileFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNO = arguments.getString("type", "null");
        }
        isPrepared = true;
        if (isVisible && isPrepared) {
            loadData(typeNO);
        }
    }


    private void loadData(String type) {
        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST1);
        requestParams2.addBodyParameter("type", type);
        loading();
        RequstUtils.getRquest(requestParams2, mhandler, 0, 1);
    }


    private void parseJson(String result) {

        List<ProfileBean> dataList = new ArrayList<>();
        dataList.add(new ProfileBean(0, "渠道名称", "销售金额", "订单数"));
        double total = 0;

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if ("900".equals(code)) {
                desc = "该功能暂未开通，敬请期待";
            }

            if (ResponseCode.isSuccess(code,desc)) {

                JSONArray subReport = JsonUitls.getJSONArray(mobBaseRes, "datas");

                for (int i = 0; i < subReport.length(); i++) {

                    JSONObject item = subReport.getJSONObject(i);
                    JSONObject channelInfo = JsonUitls.getJSONObject(item, "channelInfo");
                    JSONObject salesReportBean = JsonUitls.getJSONObject(item, "salesReportBean");

                    String name = JsonUitls.getString(channelInfo, "name");
                    int order = salesReportBean.getInt("orderCount");
                    double sales = salesReportBean.getDouble("turnOver");
                    total += sales;
                    dataList.add(new ProfileBean(1, name, order, sales));
                }

                if (dataList.size()<5) {
                    int size = dataList.size();
                    for (int i = 0; i < 6 - size; i++) {
                        dataList.add(new ProfileBean(2, "", 0, 0));
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setMyAdapter(dataList, total);

    }

    private void setMyAdapter(List<ProfileBean> dataList, double total) {

        //排序
        dataList = sortArray(dataList);

//        List<ProfileBean> bbb = new ArrayList<>();
//        bbb.add(dataList.get(0));
//        bbb.add(dataList.get(1));
//        bbb.add(dataList.get(2));
//        bbb.add(dataList.get(3));
//        bbb.add(dataList.get(4));
//        bbb.add(dataList.get(5));
//        dataList=bbb;

        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.sales_profile_header, null);

        TextView total_sales = (TextView) headerView.findViewById(R.id.total_sales);
        String total_f = String.format("%.2f", total);
        total_sales.setText("¥ " + total_f);

        TextView tv1 = (TextView) headerView.findViewById(R.id.tv1);
        TextView tv2 = (TextView) headerView.findViewById(R.id.tv2);
        TextView tv3 = (TextView) headerView.findViewById(R.id.tv3);
        TextView tv4 = (TextView) headerView.findViewById(R.id.tv4);
        TextView tv5 = (TextView) headerView.findViewById(R.id.tv5);

        TextView lebalTx=(TextView) headerView.findViewById(R.id.lebalTx);

        BarChart barChart = (BarChart) headerView.findViewById(R.id.barChart);

        int[] colors = new int[]{rgb("#34495e"), rgb("#ff4d1c"), rgb("#1d8acb"), rgb("#72c156"), rgb("#fae603")};

        StringBuffer stringBuffer = new StringBuffer();
        List<Integer> labeltag = new ArrayList<>();

        if (dataList.size() >= 6) {
            for (int i = 1; i < 6; i++) {
                if (dataList.get(i).getSales()>0) {
                    labeltag.add(stringBuffer.length());
                    stringBuffer.append("■" + dataList.get(i).getName()+"   ");
                }
            }

        } else {
            for (int i = 1; i < dataList.size(); i++) {
                labeltag.add(stringBuffer.length());
                stringBuffer.append("■" + dataList.get(i).getName()+"   ");
            }
        }
        String resultTx = stringBuffer.toString();
        SpannableString resultTxs = new SpannableString(resultTx);
        for (int i = 0; i < labeltag.size(); i++) {
            resultTxs.setSpan(new ForegroundColorSpan(colors[i]),labeltag.get(i), labeltag.get(i)+1, 0);
        }
        lebalTx.setText(resultTxs);



        //柱子没填充部分是否显示
        barChart.setDrawBarShadow(false);
        //值在柱子上/下
        barChart.setDrawValueAboveBar(true);
        //右下角图表说明文字
        barChart.getDescription().setEnabled(false);
        //是否加边框
        barChart.setDrawBorders(false);
        //超过( )个就不显示数值
        barChart.setMaxVisibleValueCount(30);
        //设置图表格子是否背景
        barChart.setDrawGridBackground(false);

        barChart.setScaleEnabled(false);


        List<BarEntry> entries = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        String[] lebalss = new String[5];

        int a;
        if (dataList.size() >= 6) {
            a = 6;
        } else {
            a = dataList.size();
        }
        for (int i = 1; i < a; i++) {
            long round = Math.round(dataList.get(i).getSales());
            entries.add(new BarEntry(i - 1, round));
            labels.add(dataList.get(i).getName());
            lebalss[i - 1] = dataList.get(i).getName();
        }

        setlebals(tv1, lebalss,0);
        setlebals(tv2, lebalss,1);
        setlebals(tv3, lebalss,2);
        setlebals(tv4, lebalss,3);
        setlebals(tv5, lebalss,4);

        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setValueFormatter(new LebalSet(labels));  // 在这里设置标签
        xAxis.setEnabled(false);//设置坐标轴不显示

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);//设置坐标轴不显示

        Legend l = barChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setXEntrySpace(5f);
        l.setYEntrySpace(5f);
        l.setYOffset(4f);
        l.setExtra(colors, lebalss);
        l.setWordWrapEnabled(true);//设置图例文字自动排列
        l.setEnabled(false);


        BarDataSet dataSet = new BarDataSet(entries, "");
        dataSet.setColors(colors);
        dataSet.setForm(Legend.LegendForm.NONE);

        dataSet.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                if (value == 0) {
                    return "";
                } else {
                    return  NumberUtil.getValue0(value);
//                    return value+"";
                }
            }
        });

        BarData data = new BarData(dataSet);
        barChart.setData(data);

        barChart.animateXY(0, 2000);


        pslistView.addHeaderView(headerView);
        SalesProfileAdapter adapter = new SalesProfileAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);
    }

    private void setlebals(TextView tv,String[] s,int i) {
        try {
            tv.setText(s[i]);
        } catch (Exception e) {
            tv.setText("");
        }
    }

    //排序
    private List<ProfileBean> sortArray(List<ProfileBean> dataList) {

        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 1; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getSales() < dataList.get(j + 1).getSales()) {
                    ProfileBean temp1 = dataList.get(j);
                    ProfileBean temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }
        return dataList;
    }


    //处理预加载
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            if (isVisible && isPrepared && !hasload) {
                loadData(typeNO);
            }
        } else {
            isVisible = false;
        }
    }


    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(typeNO);
            }
        });
    }

    @Override
    protected void loading() {
        sale_fg_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        sale_fg_loading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestroy = true;
    }
}
