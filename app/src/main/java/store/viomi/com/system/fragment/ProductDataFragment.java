package store.viomi.com.system.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.DataSpinnerAdapter;
import store.viomi.com.system.adapter.ProductDataAdapter;
import store.viomi.com.system.adapter.ProductbtmAdapter;
import store.viomi.com.system.adapter.SalesReportAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.base.ProductBean;
import store.viomi.com.system.bean.ProductDataBean;
import store.viomi.com.system.bean.ProductFormBean;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.DisplayUtil;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.ListviewRelayoutUtils;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.NumberUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.widget.MyScrollView;
import store.viomi.com.system.widget.NoScroListView;
import store.viomi.com.system.widget.ScrollViewListener;

/**
 * 商品数据
 * Created by Mocc on 2017/9/29
 */

@ContentView(R.layout.fragment_product_data)
public class ProductDataFragment extends BaseFragment {

    @ViewInject(R.id.myScrollView)
    private MyScrollView myScrollView;
//
//    @ViewInject(R.id.plist)
//    private ListView plist;

    @ViewInject(R.id.horsc)
    private HorizontalScrollView horsc;

    @ViewInject(R.id.lin_container)
    private LinearLayout lin_container;

    @ViewInject(R.id.lv_btm_detail)
    private NoScroListView lvBtm;
    @ViewInject(R.id.layout_indicator)
    private View layoutIndicator;
    @ViewInject(R.id.layout_indicator1)
    private View layoutIndicator1;


    //销售量
    @ViewInject(R.id.saleCount)
    private TextView saleCount;

    //销售额
    @ViewInject(R.id.saleAmount)
    private TextView saleAmount;

    //列表无数据提示
//    @ViewInject(R.id.list_nodata_hint)
//    private TextView list_nodata_hint;


    //加载中布局
    @ViewInject(R.id.loading_layout)
    private RelativeLayout loading_layout;
    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;
    @ViewInject(R.id.tv_time)
    private TextView tvTime;


    private String timeType = "day";
    //底部列表菜单
    private List<ProductDataBean> dataBeanList = new ArrayList<>();
    private String sourceType;
    private int pageNum;
    private List<List<ProductFormBean>> dataList = new ArrayList<>();
    private List<ProductBean> dataListBtom = new ArrayList<>();
    private ProductDataAdapter productDataAdapter;
    private int colunmSelect;
    private boolean isLoading;
    private boolean canLoadMore;
    private boolean isDestroy;

    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            if (!isDestroy) {
                switch (message.what) {
                    case 0: {
                        reconnect_layout.setVisibility(View.GONE);
                        loadingfinish();
                        String result = (String) message.obj;
                        parseJson(result);
                        break;
                    }

                    case 1: {
                        String result = message.obj.toString();
                        LogUtil.mlog("tag", result);
                        loadingfinish();
                        ResponseCode.onErrorHint(result);
                        reconnect_layout.setVisibility(View.VISIBLE);
                        break;
                    }

                    case 2: {
                        loadingfinish();

                        String result = (String) message.obj;
                        parseJson2(result);
                        isLoading = false;
                        break;
                    }

                    case 3: {
                        String result = message.obj.toString();
                        LogUtil.mlog("tag", result);
                        loadingfinish();
                        isLoading = false;
                        ResponseCode.onErrorHint(result);
                        break;
                    }
                    default:
                        break;
                }
            }
            return true;
        }
    });
    private ProductbtmAdapter productbtmAdapter;

    public static ProductDataFragment genInstance(String sourceType) {
        ProductDataFragment fragment = new ProductDataFragment();
        Bundle bundle = new Bundle();
        bundle.putString("sourceType", sourceType);
        fragment.setArguments(bundle);
        return fragment;
    }

    int scrolledY;


    @Override
    protected void init() {
        if (endTime == 0)
            endTime = System.currentTimeMillis() / 1000;
        if (beginTime == 0)
            beginTime = endTime - 60 * 60 * 24 * 30;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            beginTime = sdf1.parse(sdf1.format(new Date(beginTime * 1000L))).getTime() / 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setTimeView();
        sourceType = getArguments().getString("sourceType");
        loadTotal();

    }

    public void onNewFilter(Intent data) {
        beginTime = data.getLongExtra("start", beginTime);
        endTime = data.getLongExtra("end", endTime);
        int typeT = data.getIntExtra("timeType", 3);
        if (typeT == 3) {
            timeType = "day";
        } else if (typeT == 2) {
            timeType = "month";
        } else if (typeT == 1) {
            timeType = "year";
        }
        setTimeView();
//        if (plist == null)
//            return;
        clearData();
        if (loading_layout != null)
            loadTotal();
    }

    long beginTime, endTime;

    //platformType YUN_MI (1云米商城), MI_JIA (2米家商城), TIAN_MAO (3天猫商城), YU_FU_KUAN (4预付款提货)
    //timeType timeType可选值: day(d按天,默认),month(m按月),quart(q按季),year(y按年)
    private void loadTotal() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.WARES_SALES);
        requestParams.addBodyParameter("timeType", timeType);
        requestParams.addBodyParameter("platformType", sourceType);
        requestParams.addBodyParameter("beginTime", beginTime + "");
        requestParams.addBodyParameter("endTime", endTime + "");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    //修改到此位置，
    private void parseJson(String result) {
        JSONObject jsonObject = JsonUitls.getRawJSONObject(result);
        jsonObject = JsonUitls.getJSONObject(jsonObject, "mobBaseRes");
        String code = JsonUitls.getString(jsonObject, "code");
        String desc = JsonUitls.getString(jsonObject, "desc");
        if (ResponseCode.isSuccess(code, desc)) {
            JSONArray datas = JsonUitls.getJSONArray(jsonObject, "result");
            for (int i = datas.length() - 1; i >= 0; i--) {
                JSONObject item = JsonUitls.getJSONObjectFromArray(datas, i);
                String reportDate = JsonUitls.getString(item, "data_time");

                int quantity = JsonUitls.getInt(item, "quantity");
                long amount = JsonUitls.getLong(item, "sales");
                double amount_d = amount / 100.00d;

                dataBeanList.add(0, new ProductDataBean(reportDate, quantity, amount_d));
            }
        }
        setView1(dataBeanList);
        loadList(0);
    }

    private void setView1(final List<ProductDataBean> dataBeanList) {
        double max = 0;
        for (int i = 0; i < dataBeanList.size(); i++) {
            if (dataBeanList.get(i).getAmount() > max) {
                max = dataBeanList.get(i).getAmount();
            }
        }
        int mymaxheight = (int) (DisplayUtil.getdensity(getActivity()) * 175);
        if (max <= 0) {
            max = 1;
            mymaxheight = 0;
        }

        for (int i = 0; i < dataBeanList.size(); i++) {
            View child = LayoutInflater.from(getActivity()).inflate(R.layout.agency_daily_header_item, null);
            TextView label = (TextView) child.findViewById(R.id.label);
            TextView amount = (TextView) child.findViewById(R.id.amount);
            RelativeLayout click_area = (RelativeLayout) child.findViewById(R.id.click_area);

            ViewGroup.LayoutParams layoutParams = amount.getLayoutParams();
            layoutParams.height = (int) (mymaxheight * (dataBeanList.get(i).getAmount() / max)) + 1;

            String reportDate = dataBeanList.get(i).getReportDate();
            try {
                switch (timeType) {
                    case "day":
                        reportDate = reportDate.substring(5);
                        reportDate = reportDate.replace("-", "/");
                        break;
                    case "month":
                        reportDate = reportDate.substring(5) + "月";
                        break;
                    case "year":
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
            }

            label.setText(reportDate);

            final int finalI = i;
            click_area.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    columnClick(finalI);
                }
            });
            lin_container.addView(child);
        }
        columnClick(dataBeanList.size() - 1);
        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                horsc.fullScroll(ScrollView.FOCUS_RIGHT);
            }
        }, 100);
    }


    void setTimeView() {
        if (tvTime == null)
            return;
        if (timeType.equals("year")) {
            tvTime.setText("按年显示");
        } else if (timeType.equals("month")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            tvTime.setText(format.format(new Date(beginTime * 1000)) + " 至 " + format.format(new Date(endTime * 1000)));
        } else if (timeType.equals("day")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            tvTime.setText(format.format(new Date(beginTime * 1000)) + " 至 " + format.format(new Date(endTime * 1000)));
        }
    }

    private void clearData() {
        pageNum = 1;
        if (lin_container != null)
            lin_container.removeAllViews();
        if (dataList != null)
            dataList.clear();
        if (dataBeanList != null)
            dataBeanList.clear();
    }

    private void columnClick(int currentSelect) {
        for (int k = 0; k < lin_container.getChildCount(); k++) {
            View vk = lin_container.getChildAt(k);
            TextView amountv = (TextView) vk.findViewById(R.id.amount);
            TextView labeltv = (TextView) vk.findViewById(R.id.label);
            amountv.setBackgroundResource(R.color.agencyunselectItem);
            labeltv.setTextColor(getResources().getColor(R.color.unselectItem));
        }

        View vk = lin_container.getChildAt(currentSelect);
        if (vk == null)
            return;
        TextView amountv = (TextView) vk.findViewById(R.id.amount);
        TextView labeltv = (TextView) vk.findViewById(R.id.label);
        amountv.setBackgroundResource(R.color.agencyselectItem);
        labeltv.setTextColor(getResources().getColor(R.color.selectItem));


        saleCount.setText(dataBeanList.get(currentSelect).getQuantity() + "");

        double amount = dataBeanList.get(currentSelect).getAmount();
        double amount_d = 0d;
        String amount_str;
        if (amount >= 100000) {
            amount_d = amount / 10000.00d;
            amount_str = "￥" + NumberUtil.getValue2(amount_d) + "万";
        } else {
            amount_str = "￥" + amount;
        }

        saleAmount.setText(amount_str);

//        loadList(currentSelect);
    }

    private void loadList(int currentSelect) {
        colunmSelect = currentSelect;
        dataList.clear();
        if (productDataAdapter != null) {
            productDataAdapter.notifyDataSetChanged();
        }
        pageNum = 1;

        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.WARES_SALESDETAIL);
        requestParams.addBodyParameter("platformType", sourceType);//平台类型：YUN_MI (1云米商城), MI_JIA (2米家商城), TIAN_MAO (3天猫商城), YU_FU_KUAN (4预付款提货)
        requestParams.addBodyParameter("timeType", timeType);//timeType可选值: day(d按天,默认),month(m按月),quart(q按季),year(y按年)
        requestParams.addBodyParameter("beginTime", beginTime + "");
        requestParams.addBodyParameter("endTime", endTime + "");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 2, 3);
    }

    private void loadMoreList() {
        if (!isLoading && canLoadMore) {
            isLoading = true;
            pageNum++;
            RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.PRODUCTDATADETAILLIST);
            requestParams.addBodyParameter("reportType", sourceType);
            requestParams.addBodyParameter("reportDate", dataBeanList.get(colunmSelect).getReportDate());
            requestParams.addBodyParameter("sourceType", sourceType);
            requestParams.addBodyParameter("pageNum", pageNum + "");
            loading();
            RequstUtils.getRquest(requestParams, mhandler, 2, 3);
        }
    }

    //detail数据集
    private void parseJson2(String result) {
        dataListBtom.clear();
        JSONObject json = JsonUitls.getRawJSONObject(result);
        json = JsonUitls.getJSONObject(json, "mobBaseRes");
        String code = JsonUitls.getString(json, "code");
        String desc = JsonUitls.getString(json, "desc");
        if (ResponseCode.isSuccess(code, desc)) {

            //解释报表头
//            JSONObject templeteInfo = JsonUitls.getJSONObject(result1, "templeteInfo");
            JSONArray columnHeaders = JsonUitls.getJSONArray(json, "result");

            List<ProductFormBean> headlist = new ArrayList<>();
            for (int i = 0; i < columnHeaders.length(); i++) {
                JSONObject item = JsonUitls.getJSONObjectFromArray(columnHeaders, i);
//                String name = JsonUitls.getString(item, "name");
//                int sortIdx = JsonUitls.getInt(item, "sortIdx");
//                headlist.add(new ProductFormBean(name, sortIdx));
                //创建对象
                String name1 = JsonUitls.getString(item, "name");
                String quantity = JsonUitls.getString(item, "quantity");
                String sales = JsonUitls.getString(item, "sales");
                ProductBean productBean = new ProductBean(name1, quantity, sales);
                dataListBtom.add(productBean);
            }

//            if (pageNum == 1) {
//                dataList.add(headlist);
//            }
//
//            JSONObject records = JsonUitls.getJSONObject(json, "records");
//            int totalCount = JsonUitls.getInt(records, "totalCount");
//            JSONArray list = JsonUitls.getJSONArray(records, "list");
//
////            if (pageNum == 1 && list.length() == 0) {
////                list_nodata_hint.setVisibility(View.VISIBLE);
////            } else {
////                list_nodata_hint.setVisibility(View.GONE);
////            }
//
//            canLoadMore = !(totalCount == list.length() + dataList.size() - 1);
//
//            for (int i = 0; i < list.length(); i++) {
//                JSONObject item = JsonUitls.getJSONObjectFromArray(list, i);
//                JSONArray columns = JsonUitls.getJSONArray(item, "columns");
//
//                List<ProductFormBean> bodylist = new ArrayList<>();
//
//                for (int j = 0; j < columns.length(); j++) {
//                    JSONObject item2 = JsonUitls.getJSONObjectFromArray(columns, j);
//
//                    String value = JsonUitls.getString(item2, "value");
//                    try {
//                        if (j == 2) {
//                            double value_d = Double.valueOf(value);
//                            value_d = value_d / 100.00d;
//                            value = NumberUtil.getValue2(value_d);
//                        }
//                    } catch (Exception e) {
//                    }
//
//                    int idx = JsonUitls.getInt(item2, "idx");
//                    bodylist.add(new ProductFormBean(value, idx));
//                }
//                dataList.add(bodylist);
//            }
            setView2(dataListBtom);
        }
    }

    private void setView2(List<ProductBean> dataList) {
        productbtmAdapter = new ProductbtmAdapter(dataList, getActivity());
        lvBtm.setAdapter(productbtmAdapter);

//        productDataAdapter = new ProductDataAdapter(dataList, getActivity());
//        plist.setAdapter(productDataAdapter);
        ListviewRelayoutUtils.reLayout(lvBtm);
    }


    @Override
    protected void initListener() {


        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTotal();
            }
        });
        if (Build.VERSION.SDK_INT >= 21) {
            layoutIndicator1.setElevation(10);
            layoutIndicator1.setZ(10);
        }
        myScrollView.setScrollViewListener(new ScrollViewListener() {
            @Override
            public void onScrollChanged(int x, int y, int oldx, int oldy) {
                scrolledY = y;
                if (scrolledY >= indicatorTop) {
                    layoutIndicator1.setVisibility(View.VISIBLE);
                } else layoutIndicator1.setVisibility(View.GONE);

                if (myScrollView.getChildAt(0).getMeasuredHeight() - 2 <= myScrollView.getScrollY() + myScrollView.getHeight()) {
//                    loadMoreList();
                }
//                Log.e("----------", "scrolledY    " + scrolledY);
//                Log.e("----------", (myScrollView.getChildAt(0).getMeasuredHeight() - 2 - myScrollView.getScrollY() - myScrollView.getHeight()) + "");
            }
        });
        layoutIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (layoutIndicator.getTop() == 0)
                    return;
                indicatorTop = layoutIndicator.getTop();
                layoutIndicator.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    int indicatorTop;

    @Override
    protected void loading() {
        if (loading_layout != null)
            loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_layout.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        isDestroy = true;
        mhandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
