package store.viomi.com.system.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mocc on 2017/8/24
 */

public class OrderListBean implements Serializable {

    private String orderNO;
    private String payMode;
    private String payStatus;
    private String payPrice;
    private String createdTime;
    private List<OrderListProductBean> productBeanList;

    public String linkmanName;
    public String fullDivisionName;
    public String address;
    public String linkmanPhone;
    private String discount;
    private String settleStatesDesc;
    private double deliveryFee;

    public String channelName;
    public int orderStatus;
    public String orderStatusStr;
    public String totalPrice;

    public String expressType;
    public String expressCorpId;
    public String expressNumber;
    public long orderId;

    public OrderListBean() {
    }

    public OrderListBean(String orderNO, String payMode, String payStatus, String payPrice, String createdTime, List<OrderListProductBean> productBeanList) {
        this.orderNO = orderNO;
        this.payMode = payMode;
        this.payStatus = payStatus;
        this.payPrice = payPrice;
        this.createdTime = createdTime;
        this.productBeanList = productBeanList;
    }

    public OrderListBean(String orderNO, String payMode, String payStatus, String payPrice, String createdTime, List<OrderListProductBean> productBeanList, String linkmanName, String address, String linkmanPhone, String discount, String settleStatesDesc, double deliveryFee) {
        this.orderNO = orderNO;
        this.payMode = payMode;
        this.payStatus = payStatus;
        this.payPrice = payPrice;
        this.createdTime = createdTime;
        this.productBeanList = productBeanList;
        this.linkmanName = linkmanName;
        this.address = address;
        this.linkmanPhone = linkmanPhone;
        this.discount = discount;
        this.settleStatesDesc = settleStatesDesc;
        this.deliveryFee = deliveryFee;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLinkmanPhone() {
        return linkmanPhone;
    }

    public void setLinkmanPhone(String linkmanPhone) {
        this.linkmanPhone = linkmanPhone;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSettleStatesDesc() {
        return settleStatesDesc;
    }

    public void setSettleStatesDesc(String settleStatesDesc) {
        this.settleStatesDesc = settleStatesDesc;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(String payPrice) {
        this.payPrice = payPrice;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public List<OrderListProductBean> getProductBeanList() {
        return productBeanList;
    }

    public void setProductBeanList(List<OrderListProductBean> productBeanList) {
        this.productBeanList = productBeanList;
    }

    public double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }
}
