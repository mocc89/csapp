package store.viomi.com.system.constants;

/**
 * Created by viomi on 2016/10/18.
 */

public class MURL {

    public final static String BaseUrl = Config.BaseUrl;
//    public final static String BaseUrl2 = Config.BaseUrl2;


    //版本更新检测
    public final static String UPDATE = "http://app.mi-ae.com.cn/getdata";

    //登陆接口
    public final static String LOGINURL = BaseUrl + "/channel/login/1.json";

    //我的-修改密码
    public final static String MODIFYPASSWORD = BaseUrl + "/channel/user/changePassword";

    //找回密码-发送验证码
    public final static String SENDCODE = BaseUrl + "/channel/login/sendAuthCode";
    //重置密码
    public final static String RESTPASSWORD = BaseUrl + "channel/login/recoverPassword";


    // 超级管理员/区域管理员：首页显示今日，本月订单销售，销售数据上面头部
//    public final static String SALESREPORTHEADER = BaseUrl + "orders/analyse/channel/salesReport/eachLevel.json";
    public final static String SALESREPORTHEADER = BaseUrl + "channel/report/salesReportNew/eachLevel.json";

    //超级管理/区域管理员：销售数据下面列表、销售概况、订单概况
    //城市运营：销售概况、订单概况
//    public final static String SALESREPORTLIST1 = BaseUrl + "channel/report/salesReport/subs.json";
    public final static String SALESREPORTLIST1 = BaseUrl + "channel/report/salesReportNew/subs.json";

    //城市运营：首页显示今日，本月订单销售，销售数据
//    public final static String SALESREPORTLIST = BaseUrl + "channel/report/salesReport.json";
    public final static String SALESREPORTLIST = BaseUrl + "channel/report/salesReportNew.json";

    //订单列表数据--- 超级管理
    public final static String ORDERDATALIST = BaseUrl + "fd/order/list.json";
    //订单详情--- 超级管理
    public final static String ORDERDETAIL = BaseUrl + "fd/order/";

    //订单列表数据-城市运营
    public final static String ORDERDATALISTC = BaseUrl + "orders/info/queryOrderProfitList.json";
    //订单详情--- 城市运营
    public final static String ORDERDETAILC = BaseUrl + "orders/info/queryOrderDetail/";


    //店员管理 促销员列表
    public final static String CLERKLIST = BaseUrl + "channel/user/listChannelUser";


    //经销商管理 经销商列表 门店管理 门店列表
    public final static String CHANNELLIST = BaseUrl + "channel/info/listSubChannels.json";
    //经销商详情
    public final static String CHANNELDETAIL = BaseUrl + "channel/info/queryChannelInfo/";
    //门店店头照片等图片信息
    public final static String CHANNELIMG = BaseUrl + "channel/info/queryChannelAuthAttr";


    //门店日报 列表
    public final static String STOREDAILY = BaseUrl + "channel/report/listChannelDailyReport.json";

    //首页轮播图点进去
    public final static String SALERIMPROVE = "http://viomi-faq.mi-ae.net/gradedesc/gradeDesc.html";

    //预付款订单一级列表 日/周/月
    public final static String ADVANCELIST = BaseUrl + "channel/report/queryPurchaseChart";
    //预付款订单-详情
    public final static String ADVANCEDETAIL = BaseUrl + "channel/report/queryPurchaseChartDetail";
    //预付款订单-查询
    public final static String ADVANCESEARCH = BaseUrl + "channel/report/listPurchaseDailyReport";

    //经销商日报
    public final static String AGENCYDAILY = BaseUrl + "channel/report/queryChannelChart";
    //经销商日报详情
    public final static String AGENCYDAILYSEARCH = BaseUrl + "channel/report/queryChannelChartDetail";
    //经销商日报查询
    public final static String AGENCYDAILYSELECT = BaseUrl + "channel/report/listChannelDailyReport";

    //第三方订单数据列表
    public final static String THIRDORDERLIST = BaseUrl + "order/third/list.json";

    //商品数据-头部统计表 reportType：1日报表，0月报表，6年报表；sourceType：1云分销，2米家，3天猫
    public final static String PRODUCTDATAHEADFORM = BaseUrl + "/report/waresSales/query/salesDateStat.json";

    //商品数据-下面的详情列表 reportType：1日报表，0月报表，6年报表；sourceType：1云分销，2米家，3天猫
    public final static String PRODUCTDATADETAILLIST = BaseUrl + "/report/waresSales/query/salesReport.json";

    //预付款提货汇总柱状图
    public final static String ORDERCHART = BaseUrl + "reports/channelPurchase/orderChart";
    //预付款提货报表明细
    public final static String ORDERCHARTDETAIL = BaseUrl + "reports/channelPurchase/orderChartDetail";
    //商品的销售量,销售额
    public final static String WARES_SALES = BaseUrl+ "reports/wares/sales";
    //商品的销售量,销售额明细
    public final static String WARES_SALESDETAIL = BaseUrl + "reports/wares/salesDetail";
    //商品的库存量,待发货数
    public final static String WARES_SALESSTOCK = BaseUrl+ "reports/wares/stock";
    //商品分类列表
    public final static String WARES_CATLOG = BaseUrl + "wares/catalog/query/304.json";

    //预付款订单物流信息
    public final static String LOGISTICS_IFORMMATION = BaseUrl + "orders/info/tracking/purchase.json";
    //预付款订单列表
    public final static String PREPAYMENT_ORDER = BaseUrl + "channel/purchase/order/list.json";
    //省市区
    public final static String ORDER_AREA = BaseUrl + "dictionary/queryChannelBusinessDivision";
    //预付款提货-报表明细
    public final static String ORDER_PURCHASECHARTDETAIL = BaseUrl + "analysis/report/channel/purchaseChartDetail.json";
    //查询已拆单订单拆分详情
    public final static String QUERYSPLITINFO = BaseUrl + "channel/purchase/order/queryAllSplitInfo/";
    public final static String TRACKING = BaseUrl + "orders/info/tracking/purchase.json";

}
