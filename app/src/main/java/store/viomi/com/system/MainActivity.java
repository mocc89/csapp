package store.viomi.com.system;


import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xutils.view.annotation.ContentView;

import store.viomi.com.system.activity.LoginActivity;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.fragment.a.HomeAFragment;
import store.viomi.com.system.fragment.a.ManageAFragment;
import store.viomi.com.system.fragment.a.TableAFragment;
import store.viomi.com.system.service.DownloadService;
import store.viomi.com.system.utils.SharePreferencesUtils;
import store.viomi.com.system.utils.ToastUtil;
import viomi.com.umsdk.MyMobclickAgent;

@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity implements View.OnClickListener {

    // 图标1234 -
    private RelativeLayout icon1;
    private RelativeLayout icon2;
    private RelativeLayout icon4;

    private ImageView iv1;
    private ImageView iv2;
    private ImageView iv4;
    private TextView tv1;
    private TextView tv2;
    private TextView tv4;

    public static final int EXIT_GAP = 2000;
    public long lastClickTime = 0;
    private Fragment currentFragment;

    private final static int PERMISSION_WRITE_EX_REQCODE = 100100;
    private boolean isfirst = true;


    @Override
    protected void init() {
        icon1 = (RelativeLayout) findViewById(R.id.icon1);
        icon2 = (RelativeLayout) findViewById(R.id.icon2);
        icon4 = (RelativeLayout) findViewById(R.id.icon4);

        iv1 = (ImageView) findViewById(R.id.iv1);
        iv2 = (ImageView) findViewById(R.id.iv2);
        iv4 = (ImageView) findViewById(R.id.iv4);

        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv4 = (TextView) findViewById(R.id.tv4);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isfirst) {
            isfirst = false;
            currentFragment = TableAFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.lin, currentFragment).commit();
            iconselect1();
        }
    }


    @Override
    protected void initListener() {
        icon1.setOnClickListener(this);
        icon2.setOnClickListener(this);
        icon4.setOnClickListener(this);
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

    private void iconselect1(){
        MyMobclickAgent.onEvent(this,"super_main");

        iv1.setImageResource(R.drawable.icon_home_press);
        iv2.setImageResource(R.drawable.icon_manage_nor);
        iv4.setImageResource(R.drawable.icon_profile_nor);
        tv1.setTextColor(getResources().getColor(R.color.main_txt_select));
        tv2.setTextColor(getResources().getColor(R.color.main_txt_notselect));
        tv4.setTextColor(getResources().getColor(R.color.main_txt_notselect));
    }

    private void iconselect2(){
        MyMobclickAgent.onEvent(this,"super_manager");

        iv1.setImageResource(R.drawable.icon_home_nor);
        iv2.setImageResource(R.drawable.icon_manage_press);
        iv4.setImageResource(R.drawable.icon_profile_nor);
        tv1.setTextColor(getResources().getColor(R.color.main_txt_notselect));
        tv2.setTextColor(getResources().getColor(R.color.main_txt_select));
        tv4.setTextColor(getResources().getColor(R.color.main_txt_notselect));
    }
    private void iconselect4(){
        MyMobclickAgent.onEvent(this,"super_mine");

        iv1.setImageResource(R.drawable.icon_home_nor);
        iv2.setImageResource(R.drawable.icon_manage_nor);
        iv4.setImageResource(R.drawable.icon_profile_press);
        tv1.setTextColor(getResources().getColor(R.color.main_txt_notselect));
        tv2.setTextColor(getResources().getColor(R.color.main_txt_notselect));
        tv4.setTextColor(getResources().getColor(R.color.main_txt_select));

    }


    @Override
    public void onClick(View v) {

        Fragment fragment = null;

        switch (v.getId()) {
            case R.id.icon1:
                iconselect1();
                fragment = TableAFragment.getInstance();
                commanButtonControl();
                break;
            case R.id.icon2:
                iconselect2();
                fragment = ManageAFragment.getInstance();
                commanButtonControl();
                break;
            case R.id.icon4:
                iconselect4();
                fragment = HomeAFragment.getInstance();
                commanButtonControl();
                break;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.hide(currentFragment).show(fragment);
        } else {
            transaction.hide(currentFragment).add(R.id.lin, fragment);
        }
        transaction.commit();
        currentFragment = fragment;
    }


    //共同控制图标属性
    private void commanButtonControl() {

    }


    //捕捉后退键事件
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            if ((SystemClock.uptimeMillis() - lastClickTime) > EXIT_GAP) {
                lastClickTime = SystemClock.uptimeMillis();
                Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
            } else {
                MainActivity.this.finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
//        restartApplication();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    //重启app
    private void restartApplication() {
        final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_WRITE_EX_REQCODE) {

            int grantResult = grantResults[0];
            if (grantResult == 0) {
                ToastUtil.show(HintText.BACKGROUNDDOWNLOAD);
                Intent intent = new Intent(this, DownloadService.class);
                intent.putExtra("downlink", SharePreferencesUtils.getInstance().getDownlink(this));
                intent.putExtra("version", SharePreferencesUtils.getInstance().getAppversion(this));
                startService(intent);
            } else {
                ToastUtil.show(HintText.PERMISSIONDENY_WRITEEX);
            }
        }
    }

}
