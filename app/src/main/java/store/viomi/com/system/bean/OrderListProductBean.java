package store.viomi.com.system.bean;

import java.io.Serializable;

/**
 * Created by Mocc on 2017/8/24
 */

public class OrderListProductBean implements Serializable {

    private String name;
    private String quantity;
    private String imgUrl;
    public String paymentPrice;
    public String price;

    public String skuName;

    public OrderListProductBean() {
    }

    public OrderListProductBean(String name, String quantity, String imgUrl, String paymentPrice) {
        this.name = name;
        this.quantity = quantity;
        this.imgUrl = imgUrl;
        this.paymentPrice = paymentPrice;
    }

    public String getPaymentPrice() {
        return paymentPrice;
    }

    public void setPaymentPrice(String paymentPrice) {
        this.paymentPrice = paymentPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
