package store.viomi.com.system.bean;

/**
 * Created by viomi on 2017/1/9.
 */

public class AdvanceREntity {

    private String day;
    private String channelName;
    private String secondChannel;
    private String rootChannel;
    private String purchaseMachineCount;
    private String purchaseOrderCount;
    private String purchaseOrderAmount;

    public AdvanceREntity() {
    }

    public AdvanceREntity(String day, String channelName, String secondChannel, String rootChannel, String purchaseMachineCount, String purchaseOrderCount, String purchaseOrderAmount) {
        this.day = day;
        this.channelName = channelName;
        this.secondChannel = secondChannel;
        this.rootChannel = rootChannel;
        this.purchaseMachineCount = purchaseMachineCount;
        this.purchaseOrderCount = purchaseOrderCount;
        this.purchaseOrderAmount = purchaseOrderAmount;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getSecondChannel() {
        return secondChannel;
    }

    public void setSecondChannel(String secondChannel) {
        this.secondChannel = secondChannel;
    }

    public String getRootChannel() {
        return rootChannel;
    }

    public void setRootChannel(String rootChannel) {
        this.rootChannel = rootChannel;
    }

    public String getPurchaseMachineCount() {
        return purchaseMachineCount;
    }

    public void setPurchaseMachineCount(String purchaseMachineCount) {
        this.purchaseMachineCount = purchaseMachineCount;
    }

    public String getPurchaseOrderCount() {
        return purchaseOrderCount;
    }

    public void setPurchaseOrderCount(String purchaseOrderCount) {
        this.purchaseOrderCount = purchaseOrderCount;
    }

    public String getPurchaseOrderAmount() {
        return purchaseOrderAmount;
    }

    public void setPurchaseOrderAmount(String purchaseOrderAmount) {
        this.purchaseOrderAmount = purchaseOrderAmount;
    }
}
