package store.viomi.com.system.constants;

/**
 * Created by viomi on 2016/10/24.
 */

public class HintText {

    public final static String EMPTYNAME = "请输入用户名";
    public final static String EMPTYPASSWORD = "请输入密码";
    public final static String EMPTYOLDPASSWORD = "旧密码不能为空";
    public final static String EMPTYNEWPASSWORD = "新密码不能为空";
    public final static String NOTSAMEPASSWORD = "两次输入密码不正确";
    public final static String MODIFYPASSWORDOK = "密码修改成功";
    public final static String PERMISSIONDENY_WRITEEX = "请先开启应用存储权限";
    public final static String BACKGROUNDDOWNLOAD = "后台下载中，请稍后";
    public final static String CANNOTEMPTY = "输入不能为空";
    public final static String UPDATENONEED = "当前是最新版本";
    public final static String UNABLELOGIN = "您的权限不足，无法登录";
    public final static String FAIL_REFLESH = "刷新失败";
    public final static String MODIFYPASSWORD = "请输入6-20位英文数字组合";
    public final static String CONNECTTIMEOUT = "网络连接超时，请重试！";
    public final static String CONNECTFAIL = "网络连接失败，请检查网络！";
    public final static String SERVICEERROR = "加载数据失败，请重试！";

    //    public final static String

}
