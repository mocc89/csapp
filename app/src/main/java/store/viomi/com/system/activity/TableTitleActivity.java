package store.viomi.com.system.activity;

import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.MURL;

@ContentView(R.layout.activity_table_title)
public class TableTitleActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.wb)
    private WebView wb;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    @Override
    protected void init() {

        WebSettings settings = wb.getSettings();
        settings.setJavaScriptEnabled(true);
        wb.loadUrl(MURL.SALERIMPROVE);
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        wb.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                loadingfinish();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
//                loading();
            }
        });

    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
