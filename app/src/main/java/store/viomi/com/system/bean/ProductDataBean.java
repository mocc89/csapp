package store.viomi.com.system.bean;

/**
 * Created by Mocc on 2017/10/6
 */

public class ProductDataBean {

    private String reportDate;
    private int quantity;
    private double amount;

    public ProductDataBean() {
    }

    public ProductDataBean(String reportDate, int quantity, double amount) {
        this.reportDate = reportDate;
        this.quantity = quantity;
        this.amount = amount;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

}
