package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import store.viomi.com.system.R;

/**
 * Created by viomi on 2016/11/22.
 */

public class UpdateTxAdapter extends BaseAdapter {

    private String[] list;
    private Context context;
    private LayoutInflater inflater;

    public UpdateTxAdapter(String[] list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.length;
    }

    @Override
    public Object getItem(int position) {
        return list[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.update_txt_item, null);
        TextView txt = (TextView) convertView.findViewById(R.id.txt);
        txt.setText(list[position]);

        return convertView;
    }
}
