package store.viomi.com.system.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.activity.OrderDetailThirdActivity;
import store.viomi.com.system.adapter.OrderListAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;
import store.viomi.com.system.bean.OrderSearchParameter;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

/**
 * Created by Mocc on 2017/8/24
 */

@ContentView(R.layout.fragment_order_list)
public class OrderListThirdFragment extends BaseFragment {

    @ViewInject(R.id.orderlist)
    private ListView orderlist;

    @ViewInject(R.id.order_count)
    private TextView order_count;
    @ViewInject(R.id.tv_time)
    private TextView tv_time;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;
    //提示无数据
    @ViewInject(R.id.no_resouce_layout)
    private RelativeLayout no_resouce_layout;
    //提示无数据
    @ViewInject(R.id.tv_no_resource2)
    private TextView tv_no_resource2;


    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;       // 当前窗口可见项总数

    private int currentPage = 1;
    private int pageSize = 50;

    private long beginTime;
    private long endTime;

    private boolean isFirsrPage = true;

    private List<OrderListBean> datalist;

    private int totalCount;
    private int totalPageNum;
    private OrderListAdapter adapter;
    OrderSearchParameter param;

    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (finish)
                return false;
            switch (msg.what) {
                case 0:
                    currentPage++;
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJSON(result);
                    break;
                case 1:
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                case 7:
                    currentPage++;
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result2 = (String) msg.obj;
                    parseJSON2(result2);
                    break;
                case 8:
                    currentPage++;
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result1 = (String) msg.obj;
                    parseJSON(result1);
                    break;
                case 9:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
            }
            return false;
        }
    });
    /**
     * 1 米家 2天猫
     */
    private String orderType;

    public static OrderListThirdFragment getInstance(String orderTypeT) {
        OrderListThirdFragment of = new OrderListThirdFragment();
        Bundle bundle = new Bundle();
        bundle.putString("orderType", orderTypeT);
        of.setArguments(bundle);
        return of;
    }

    public static OrderListThirdFragment getInstance(String orderType, String startTime, String endTime) {
        OrderListThirdFragment of = new OrderListThirdFragment();
        Bundle bundle = new Bundle();
        bundle.putString("orderType", orderType);
        bundle.putString("startTime", startTime);
        bundle.putString("endTime", endTime);
        of.setArguments(bundle);
        return of;
    }

    private void loadFirstList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.THIRDORDERLIST);

        requestParams.addBodyParameter("currentPage", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf1.format(new Date(param.start * 1000L));
        String format1 = sdf1.format(new Date(param.end * 1000L));

        requestParams.addParameter("beginTime", format + "");
        requestParams.addParameter("endTime", format1 + "");


        //第三方订单来源方式:1米家 2天猫
        requestParams.addBodyParameter("source", orderType);
        //订单处理状态
        requestParams.addBodyParameter("orderStatus", "");
        //云米订单编号
        requestParams.addBodyParameter("vmOrderCode", "");
        //商家名称
        requestParams.addBodyParameter("sellerName", "");


        //收货人手机号
        requestParams.addBodyParameter("receiverPhone", "");
        //外部订单编号
        requestParams.addBodyParameter("thirdOrderCode", "");
        //订单支付状态
        requestParams.addBodyParameter("payStatus", "");
        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 8, 9);

    }


    private void loadList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.THIRDORDERLIST);

        requestParams.addBodyParameter("currentPage", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd+HH:mm");
        beginTime = beginTime * 1000L;
        String format = sdf1.format(new Date(param.start * 1000L));
        String format1 = sdf1.format(new Date(param.end * 1000L));

        requestParams.addBodyParameter("beginTime", format + "");
        requestParams.addBodyParameter("endTime", format1 + "");


        //第三方订单来源方式:1米家 2天猫
        requestParams.addBodyParameter("source", orderType);
        //订单处理状态
        requestParams.addBodyParameter("orderStatus", "");
        //云米订单编号
        requestParams.addBodyParameter("vmOrderCode", "");
        //商家名称
        requestParams.addBodyParameter("sellerName", "");


        //收货人手机号
        requestParams.addBodyParameter("receiverPhone", "");
        //外部订单编号
        requestParams.addBodyParameter("thirdOrderCode", "");
        //订单支付状态
        requestParams.addBodyParameter("payStatus", "");

        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void parseJSON(String result) {

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess2(code, desc)) {

                JSONObject resultJSON = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray list = JsonUitls.getJSONArray(resultJSON, "list");

                if (list.length() == 0 && currentPage == 2) {
                    no_resouce_layout.setVisibility(View.VISIBLE);
                } else {
                    no_resouce_layout.setVisibility(View.GONE);
                }

                for (int i = 0; i < list.length(); i++) {
                    JSONObject data = list.getJSONObject(i);
                    String orderNO = JsonUitls.getString(data, "orderCode");
                    String payMode = JsonUitls.getString(data, "payTypeDesc");
                    String payStatus = JsonUitls.getString(data, "dealPhaseDesc");
                    String payPrice = JsonUitls.getString(data, "paymentPrice");

                    long time = data.getLong("createdTime");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String createdTime = sdf.format(new Date(time));

                    String linkmanName = JsonUitls.getString(data, "linkmanName");
                    String address = JsonUitls.getString(data, "address");
                    String linkmanPhone = JsonUitls.getString(data, "linkmanPhone");
                    String discount = JsonUitls.getString(data, "discount");
                    String settleStatesDesc = JsonUitls.getString(data, "settleStatesDesc");
                    double deliveryFee = JsonUitls.getDouble(data, "deliveryFee");

                    List<OrderListProductBean> productBeanList = new ArrayList<>();

                    JSONArray skuInfoList = JsonUitls.getJSONArray(data, "skuInfoList");

                    for (int j = 0; j < skuInfoList.length(); j++) {
                        JSONObject info = skuInfoList.getJSONObject(j);
                        String name = JsonUitls.getString(info, "name");
                        String quantity = JsonUitls.getString(info, "quantity");
                        String imgUrl = JsonUitls.getString(info, "imgUrl");
                        String paymentPrice = JsonUitls.getString(info, "paymentPrice");
                        OrderListProductBean productBean = new OrderListProductBean(name, quantity, imgUrl, paymentPrice);
                        productBeanList.add(productBean);
                    }

//                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList);
                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList, linkmanName, address, linkmanPhone, discount, settleStatesDesc, deliveryFee);
                    datalist.add(bean);
                }

                if (isFirsrPage) {
                    totalCount = resultJSON.getInt("totalCount");
                    totalPageNum = resultJSON.getInt("totalPageNum");
                }

                if (isFirsrPage) {
                    setView();
                } else {
                    adapter.notifyDataSetChanged();
                    orderlist.setSelection(visibleLastIndex - visibleItemCount + 2);
                }
            } else {
                no_resouce_layout.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseJSON2(String result) {

        try {
            JSONObject mobBaseRes = new JSONObject(result);
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess2(code, desc)) {

                JSONObject resultJSON = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray list = JsonUitls.getJSONArray(resultJSON, "list");

                if (list.length() == 0 && currentPage == 2) {
                    no_resouce_layout.setVisibility(View.VISIBLE);
                } else {
                    no_resouce_layout.setVisibility(View.GONE);
                }

                for (int i = 0; i < list.length(); i++) {
                    JSONObject data = list.getJSONObject(i);
                    String orderNO = JsonUitls.getString(data, "orderCode");
                    String payMode = JsonUitls.getString(data, "payTypeDesc");
                    String payStatus = JsonUitls.getString(data, "dealPhaseDesc");
                    String payPrice = JsonUitls.getString(data, "paymentPrice");

                    String createdTime = data.getString("createdTime");

                    String linkmanName = JsonUitls.getString(data, "linkmanName");
                    String address = JsonUitls.getString(data, "address");
                    String linkmanPhone = JsonUitls.getString(data, "linkmanPhone");
                    String discount = JsonUitls.getString(data, "discount");
                    String settleStatesDesc = JsonUitls.getString(data, "settleStatesDesc");
                    double deliveryFee = JsonUitls.getDouble(data, "deliveryFee");

                    List<OrderListProductBean> productBeanList = new ArrayList<>();

                    JSONArray skuInfoList = JsonUitls.getJSONArray(data, "skuInfoList");

                    for (int j = 0; j < skuInfoList.length(); j++) {
                        JSONObject info = skuInfoList.getJSONObject(j);
                        String name = JsonUitls.getString(info, "name");
                        String quantity = JsonUitls.getString(info, "quantity");
                        String imgUrl = JsonUitls.getString(info, "imgUrl");
                        String paymentPrice = JsonUitls.getString(info, "paymentPrice");
                        OrderListProductBean productBean = new OrderListProductBean(name, quantity, imgUrl, paymentPrice);
                        productBeanList.add(productBean);
                    }

//                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList);
                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList, linkmanName, address, linkmanPhone, discount, settleStatesDesc, deliveryFee);
                    datalist.add(bean);
                }

                if (isFirsrPage) {
                    totalCount = resultJSON.getInt("totalCount");
                    totalPageNum = resultJSON.getInt("totalPageNum");
                }

                if (isFirsrPage) {
                    setView();
                } else {
                    adapter.notifyDataSetChanged();
                    orderlist.setSelection(visibleLastIndex - visibleItemCount + 2);
                }
            } else {
                no_resouce_layout.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setView() {
        isFirsrPage = false;
        order_count.setText("" + totalCount);
        adapter = new OrderListAdapter(getActivity(), datalist);
        orderlist.setAdapter(adapter);
    }

    private void loadmore() {
        loadList();
    }

    SimpleDateFormat sdf1;

    @Override
    protected void init() {
        Bundle bundle = getArguments();
        orderType = bundle.getString("orderType");
        if (param == null) {
            param = new OrderSearchParameter();
            endTime = System.currentTimeMillis() / 1000L;
            beginTime = endTime - 60L * 60L * 24L * 30L;
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                beginTime = sdf1.parse(sdf1.format(new Date(beginTime * 1000L))).getTime() / 1000L;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            param.end = endTime;
            param.start = beginTime;
        }
        sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        datalist = new ArrayList<>();
        tv_time.setText(sdf1.format(new Date(beginTime * 1000L)) + "至" + sdf1.format(new Date(endTime * 1000L)));
        if (orderType.equals("2")) {//天猫
            no_resouce_layout.setVisibility(View.VISIBLE);
            tv_no_resource2.setText("暂未开放");
            return;
        }
        loadFirstList();
    }


    public void onNewFilter(OrderSearchParameter param) {
        this.param = param;
        currentPage = 1;
        isFirsrPage = true;
        beginTime = this.param.start;
        endTime = this.param.end;
        if (orderlist == null)
            return;
        tv_time.setText(sdf1.format(new Date(beginTime * 1000L)) + "至" + sdf1.format(new Date(endTime * 1000L)));
        datalist.clear();
        loadFirstList();
    }

    @Override
    protected void initListener() {

        orderlist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                OrderListThirdFragment.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;

            }
        });

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), OrderDetailThirdActivity.class);
                OrderListBean bean = datalist.get((int) id);
                intent.putExtra("orderListBean", bean);
                startActivity(intent);
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstList();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

    boolean finish;

    @Override
    public void onDestroy() {
        finish = true;
        mhandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
