package store.viomi.com.system.bean;

/**
 * Created by viomi on 2017/1/11.
 */

public class AgencyDailyREntity {

    private String day;
    private String secondChannel;
    private String rootChannel;
    private String userCount;
    private String terminalCount;
    private String staffCount;
    private String parttimeCount;
    private String orderCount;
    private String orderAmount;

    public AgencyDailyREntity() {
    }

    public AgencyDailyREntity(String day, String secondChannel, String rootChannel, String userCount, String terminalCount, String staffCount, String parttimeCount) {
        this.day = day;
        this.secondChannel = secondChannel;
        this.rootChannel = rootChannel;
        this.userCount = userCount;
        this.terminalCount = terminalCount;
        this.staffCount = staffCount;
        this.parttimeCount = parttimeCount;
    }

    public AgencyDailyREntity(String day, String secondChannel, String rootChannel, String userCount, String terminalCount, String staffCount, String parttimeCount, String orderCount, String orderAmount) {
        this.day = day;
        this.secondChannel = secondChannel;
        this.rootChannel = rootChannel;
        this.userCount = userCount;
        this.terminalCount = terminalCount;
        this.staffCount = staffCount;
        this.parttimeCount = parttimeCount;
        this.orderCount = orderCount;
        this.orderAmount = orderAmount;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSecondChannel() {
        return secondChannel;
    }

    public void setSecondChannel(String secondChannel) {
        this.secondChannel = secondChannel;
    }

    public String getRootChannel() {
        return rootChannel;
    }

    public void setRootChannel(String rootChannel) {
        this.rootChannel = rootChannel;
    }

    public String getUserCount() {
        return userCount;
    }

    public void setUserCount(String userCount) {
        this.userCount = userCount;
    }

    public String getTerminalCount() {
        return terminalCount;
    }

    public void setTerminalCount(String terminalCount) {
        this.terminalCount = terminalCount;
    }

    public String getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(String staffCount) {
        this.staffCount = staffCount;
    }

    public String getParttimeCount() {
        return parttimeCount;
    }

    public void setParttimeCount(String parttimeCount) {
        this.parttimeCount = parttimeCount;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }
}
