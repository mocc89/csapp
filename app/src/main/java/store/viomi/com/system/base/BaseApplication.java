package store.viomi.com.system.base;

import android.app.Application;

import org.xutils.BuildConfig;
import org.xutils.x;

/**
 * Created by viomi on 2016/10/18.
 */

public class BaseApplication extends Application {

    private static String user_name;
    private static String channel_name;
    private static String token;
    private static BaseApplication app;

    private static String downlink;
    private static String appversion;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        //初始化xutils
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG);
    }

    public static BaseApplication getApp() {
        return app;
    }
}
