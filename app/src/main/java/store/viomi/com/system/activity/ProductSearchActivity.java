package store.viomi.com.system.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.Product;
import store.viomi.com.system.bean.ProductCat;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;

public class ProductSearchActivity extends BaseActivity implements View.OnClickListener {
    final int TEXT_CHANGE = 11;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.edt)
    EditText edt;
    @BindView(R.id.iv_delete)
    ImageView ivDelete;
    @BindView(R.id.select)
    TextView select;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.reconnect_btn)
    TextView reconnectBtn;
    @BindView(R.id.reconnect_layout)
    RelativeLayout reconnectLayout;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.loading_layout)
    RelativeLayout loadingLayout;
    @BindView(R.id.tv_6)
    TextView tv6;
    @BindView(R.id.tv_7)
    TextView tv7;
    boolean[] zhuangtaiValue = new boolean[2];
    TextView[] zhuagntaiView = new TextView[2];
    String saleStatus = "", prodName = "";
    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            int what = message.what;
            if (what == TEXT_CHANGE) {
                prodName = ((SpannableStringBuilder) message.obj).toString();
                if (TextUtils.isEmpty(prodName))
                    return false;
                for (ProductCat cat : mList) {
                    cat.selected = false;
                }
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
                refreshView();
            } else if (what == 0) {
                loadingfinish();
                String result = (String) message.obj;
                LogUtil.mlog(TAG, result);
                parseJsonList(result);
            } else if (what == 1) {
                reconnectLayout.setVisibility(View.VISIBLE);
                loadingfinish();
            }
            return true;
        }
    });

    private void parseJsonList(String result1) {
        try {
            JSONObject json = new JSONObject(result1);
            json = json.optJSONObject("mobBaseRes");
            String code = JsonUitls.getString(json, "code");
//            String desc = JsonUitls.getString(json, "desc");
            JSONArray result = JsonUitls.getJSONArray(json, "datas");
            if (result != null)
                for (int i = 0; i < result.length(); i++) {
                    JSONObject object = result.getJSONObject(i);
                    ProductCat resp = new ProductCat();
                    resp.id = object.optInt("id");
                    resp.value = object.optString("value");
                    mList.add(resp);
                }
            mAdapter = new ProductCatAdapter();
            gridCats.setAdapter(mAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @BindView(R.id.grid_cats)
    GridView gridCats;
    ArrayList<ProductCat> mList = new ArrayList<>();
    ProductCatAdapter mAdapter;

    @Override
    protected void init() {
        setContentView(R.layout.activity_product_search);
        ButterKnife.bind(this);
        zhuagntaiView[0] = tv6;
        zhuagntaiView[1] = tv7;
        reconnectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadList();
            }
        });
        prodName = getIntent().getStringExtra("prodName");
        edt.setText(prodName);
        edt.setSelection(prodName.length());
        loadList();
    }

    @Override
    protected void initListener() {
        for (TextView view : zhuagntaiView) {
            view.setOnClickListener(new ZhuangtaiClick());
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (zhuangtaiValue[0]) {
                    saleStatus = "0";
                } else if (zhuangtaiValue[1]) {
                    saleStatus = "1";
                } else {
                    saleStatus = "";
                }
                ArrayList<ProductCat> list = new ArrayList<>();
                for (ProductCat cat : mList) {
                    if (cat.selected) {
                        list.add(cat);
                    }
                }
                Intent itent = new Intent();
                itent.putExtra("prodName", prodName);
                itent.putExtra("saleStatus", saleStatus);
                itent.putParcelableArrayListExtra("catalogIds", list);
                setResult(RESULT_OK, itent);
                finish();
            }
        });
        ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edt.setText("");
            }
        });
        edt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Message message = new Message();
                message.what = TEXT_CHANGE;
                message.obj = charSequence;
                handler.sendMessage(message);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        gridCats.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                prodName = "";
                edt.setText(prodName);
                mList.get(i).selected = !mList.get(i).selected;
                mAdapter.notifyDataSetChanged();
            }
        });
    }


    class ProductCatAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int i) {
            return mList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ProductCat cat = mList.get(i);
            TextView textView = null;
            if (view == null) {
                textView = new TextView(activity);
                textView.setLayoutParams(new AbsListView.LayoutParams(-2, -2));
                textView.setMinWidth((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics()));
                textView.setMinHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.stock_item_bg));
                textView.setGravity(Gravity.CENTER);
                textView.setTextColor(Color.parseColor("#FF0EA5E5"));
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            } else {
                textView = (TextView) view;
            }
            textView.setText(cat.value);
            if (cat.selected) {
                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.stock_item_bg_selected));
                textView.setTextColor(Color.WHITE);
            } else {
                textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.stock_item_bg));
                textView.setTextColor(Color.parseColor("#FF0EA5E5"));
            }
            return textView;
        }
    }

    @Override
    protected void loading() {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loadingLayout.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        refreshView();
    }

    void loadList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.WARES_CATLOG);
        loading();
        RequstUtils.getRquest(requestParams, handler, 0, 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO: add setContentView(...) invocation
        ButterKnife.bind(this);
    }

    class ZhuangtaiClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            if (view == tv6) {
                zhuangtaiValue[0] = !zhuangtaiValue[0];
                zhuangtaiValue[1] = false;
            } else if (view == tv7) {
                zhuangtaiValue[0] = false;
                zhuangtaiValue[1] = !zhuangtaiValue[1];
            }
            refreshView();
        }
    }

    void refreshView() {
        for (int i = 0; i < zhuangtaiValue.length; i++) {
            if (zhuangtaiValue[i]) {
                zhuagntaiView[i].setBackgroundDrawable(getResources().getDrawable(R.drawable.stock_item_bg_selected));
                zhuagntaiView[i].setTextColor(Color.WHITE);
            } else {
                zhuagntaiView[i].setTextColor(Color.parseColor("#0EA5E5"));
                zhuagntaiView[i].setBackgroundDrawable(getResources().getDrawable(R.drawable.stock_item_bg));
            }
        }
    }
}
