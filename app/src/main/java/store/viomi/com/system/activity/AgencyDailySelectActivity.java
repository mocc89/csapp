package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;

import store.viomi.com.system.R;
import store.viomi.com.system.utils.SharePreferencesUtils;
import store.viomi.com.timepickerlibrary.timepicker.DatePicker;

public class AgencyDailySelectActivity extends AppCompatActivity {

    private EditText store_name;
    private EditText agency_name;
    private EditText city_name;
    private TextView start_txt;
    private TextView end_txt;
    private Button select;
    private ImageView back;

    private String starttimeshow = "";
    private String endtimeshow = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agency_daily_select);

        back = (ImageView) findViewById(R.id.back);
        store_name = (EditText) findViewById(R.id.store_name);
        agency_name = (EditText) findViewById(R.id.agency_name);
        city_name = (EditText) findViewById(R.id.city_name);
        start_txt = (TextView) findViewById(R.id.start_txt);
        end_txt = (TextView) findViewById(R.id.end_txt);
        select = (Button) findViewById(R.id.select);
        RelativeLayout city_input_layout = (RelativeLayout) findViewById(R.id.city_input_layout);

        if ( "C".equals(SharePreferencesUtils.getInstance().getRole(this))) {
            city_input_layout.setVisibility(View.GONE);
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        start_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(AgencyDailySelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2020, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = (c.get(Calendar.MONTH));
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }

                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        starttimeshow = year + "-" + month + "-" + day;
                        start_txt.setText(starttimeshow);
                    }
                });
                picker.show();
            }
        });

        end_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(AgencyDailySelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2020, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }


                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        endtimeshow = year + "-" + month + "-" + day;
                        end_txt.setText(endtimeshow);
                    }
                });
                picker.show();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgencyDailySelectActivity.this, AgencyDailyResultActivity.class);
                intent.putExtra("store_name", store_name.getText().toString());
                intent.putExtra("agency_name", agency_name.getText().toString());
                intent.putExtra("city_name", city_name.getText().toString());
                intent.putExtra("starttime", starttimeshow);
                intent.putExtra("endtime", endtimeshow);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}
