package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.AdvanceResultAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.AdvanceREntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_advance_result)
public class AdvanceResultActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.adrList)
    private ListView adrList;

    @ViewInject(R.id.loading_layout)
    private RelativeLayout loading_layout;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    //没有数据
    @ViewInject(R.id.nodata_layout)
    private RelativeLayout nodata_layout;
    @ViewInject(R.id.select_again)
    private TextView select_again;

    private String channelName;
    private String rootChannel;
    private String secondChannel;
    private String beginTime;
    private String endTime;

    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;       // 当前窗口可见项总数

    private int currentPage = 1;
    private int pageSize = 15;

    private int totalCount;
    private int totalPageNum;

    private boolean isFirsrPage = true;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi", result);
                    parseJson(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
                case 2: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi-more", result);
                    parseJson(result);
                    break;
                }
                case 3: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }
            }
        }
    };
    private AdvanceResultAdapter adapter;
    private List<AdvanceREntity> relist;


    @Override
    protected void init() {

        Intent intent = getIntent();
        if (intent != null) {
            channelName = intent.getStringExtra("store_name");
            secondChannel = intent.getStringExtra("agency_name");
            rootChannel = intent.getStringExtra("city_name");
            beginTime = intent.getStringExtra("starttime");
            endTime = intent.getStringExtra("endtime");
        }
        relist = new ArrayList<>();
        loadData();
    }

    private void loadData() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ADVANCESEARCH);
        requestParams.addBodyParameter("channelName", channelName);
        requestParams.addBodyParameter("rootChannel", rootChannel);
        requestParams.addBodyParameter("secondChannel", secondChannel);
        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);
        requestParams.addBodyParameter("pageNum", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void loadmore() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ADVANCESEARCH);
        requestParams.addBodyParameter("channelName", channelName);
        requestParams.addBodyParameter("rootChannel", rootChannel);
        requestParams.addBodyParameter("secondChannel", secondChannel);
        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);
        requestParams.addBodyParameter("pageNum", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 2, 3);
    }

    private void parseJson(String result) {

        try {
            JSONObject json = new JSONObject(result);
            try {
                JSONObject mobBaseRes = json.getJSONObject("mobBaseRes");
                String desc = mobBaseRes.getString("desc");
                ToastUtil.show(desc);
                if (isFirsrPage) {
                    reconnect_layout.setVisibility(View.VISIBLE);
                }
                return;
            }catch (JSONException e){
            }

            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code,desc)) {
                currentPage++;
                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");
                JSONArray jsonArray = JsonUitls.getJSONArray(resultjson, "list");

                if (isFirsrPage) {
                    totalCount = JsonUitls.getInt(resultjson, "totalCount");
                    totalPageNum = JsonUitls.getInt(resultjson, "totalPageNum");
                    if (jsonArray.length()==0) {
                        nodata_layout.setVisibility(View.VISIBLE);
                    }
                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject item = jsonArray.getJSONObject(i);
                    String day = JsonUitls.getString(item, "day");
                    String channelName = JsonUitls.getString(item, "channelName");
                    String secondChannel = JsonUitls.getString(item, "secondChannel");
                    String rootChannel = JsonUitls.getString(item, "rootChannel");
                    String purchaseMachineCount = JsonUitls.getString(item, "purchaseMachineCount");
                    String purchaseOrderCount = JsonUitls.getString(item, "purchaseOrderCount");
                    String purchaseOrderAmount = JsonUitls.getString(item, "purchaseOrderAmount");
                    relist.add(new AdvanceREntity(day, channelName, secondChannel, rootChannel, purchaseMachineCount, purchaseOrderCount, purchaseOrderAmount));
                }

                if (isFirsrPage) {
                    isFirsrPage = false;
                    adapter = new AdvanceResultAdapter(relist, this);
                    adrList.setAdapter(adapter);
                } else {
                    adapter.notifyDataSetChanged();
                    adrList.setSelection(visibleLastIndex - visibleItemCount + 2);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData();
            }
        });

        select_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        adrList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                AdvanceResultActivity.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
            }
        });

    }


    @Override
    protected void loading() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_layout.setVisibility(View.GONE);
    }
}
