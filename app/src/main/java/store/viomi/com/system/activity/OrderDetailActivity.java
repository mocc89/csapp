package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.adapter.OrderDetailProAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;
import store.viomi.com.system.bean.ProductEntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

public class OrderDetailActivity extends BaseActivity {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.order_Code)
    TextView orderCode;
    @BindView(R.id.delivery_name)
    TextView deliveryName;
    @BindView(R.id.delivery_phone)
    TextView deliveryPhone;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.delivery_address)
    TextView deliveryAddress;
    @BindView(R.id.productList)
    ListView productList;
    @BindView(R.id.order_count)
    TextView orderCount;
    @BindView(R.id.coupon)
    TextView coupon;
    @BindView(R.id.order_carriage)
    TextView orderCarriage;
    @BindView(R.id.created_time)
    TextView createdTime;
    @BindView(R.id.tv_order_played)
    TextView tvOrderPlayed;
    @BindView(R.id.tv_express)
    TextView tvExpress;
    @BindView(R.id.reconnect_btn)
    TextView reconnectBtn;
    @BindView(R.id.reconnect_layout)
    RelativeLayout reconnectLayout;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.loading)
    RelativeLayout loading;
    @BindView(R.id.activity_order_detail)
    RelativeLayout activityOrderDetail;
    @BindView(R.id.tv_express_no)
    TextView tvExpressNo;
    @BindView(R.id.layout_express_no)
    LinearLayout layoutExpressNo;
    @BindView(R.id.tv_express_name)
    TextView tvExpressName;
    @BindView(R.id.layout_express_name)
    LinearLayout layoutExpressName;
    @BindView(R.id.tv_express_status)
    TextView tvExpressStatus;
    @BindView(R.id.layout_express_status)
    LinearLayout layoutExpressStatus;


    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                case 1:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.VISIBLE);
                    break;
                case 3:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.GONE);
                    String result1 = (String) msg.obj;
                    LogUtil.mlog("oook", result1);
                    parseJSONWuliu(result1);
                    break;
                case 4:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.VISIBLE);
                    break;
            }
            return false;
        }
    });

    private void parseJSONWuliu(String result1) {
        try {
            JSONObject mobBaseRes = new JSONObject(result1);
            if (mobBaseRes.has("mobBaseRes"))
                mobBaseRes = mobBaseRes.getJSONObject("mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess2(code, desc)) {
                JSONArray list = JsonUitls.getJSONArray(mobBaseRes, "result");
                if (list != null)
                    tvExpressStatus.setText("拆分为" + list.length() + "个物流单");
            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String o_code;


    @Override
    protected void init() {
        Intent intent = getIntent();
        o_code = intent.getStringExtra("ordercode");
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        loadInfo();

    }

    private void loadInfo() {
        String url = MURL.PREPAYMENT_ORDER;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(url);
        requestParams.addBodyParameter("orderCode", o_code);
        loading();
        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void loadWuliuNo() {
        String url = MURL.QUERYSPLITINFO;
        url += orderId;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(url);
        loading();
        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 3, 4);
    }

    long orderId;
    OrderListBean bean;

    private void parseJSON(String result) {
        try {
            JSONObject mobBaseRes = new JSONObject(result);
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess(code, desc)) {
                bean = new OrderListBean();
                JSONObject resultJson = mobBaseRes.getJSONObject("result").getJSONArray("list").getJSONObject(0);
                orderId = JsonUitls.getLong(resultJson, "orderId");
                bean.orderId = orderId;
                //订单号
                String orderCodeT = JsonUitls.getString(resultJson, "orderCode");
                orderCode.setText(orderCodeT);

                //收货人姓名
                String linkmanName = JsonUitls.getString(resultJson, "linkmanName");
                deliveryName.setText(linkmanName);

                //收货地址
                String fullDivisionName = JsonUitls.getString(resultJson, "fullDivisionName");
                String address = JsonUitls.getString(resultJson, "address");
                String completeAddress = fullDivisionName + " " + address;
                deliveryAddress.setText(completeAddress);

                //收货人电话
                String linkmanPhone = JsonUitls.getString(resultJson, "linkmanPhone");
                deliveryPhone.setText(linkmanPhone);

                //支付方式
//                String payTypeDesc = JsonUitls.getString(resultJson, "payTypeDesc");
//                tvPlayed.setText(payTypeDesc);
//

                //优惠金额
                String discount = JsonUitls.getString(resultJson.getJSONObject("payment"), "profitPay");
                coupon.setText("¥ " + discount);

                double deliveryFee = JsonUitls.getDouble(resultJson, "deliveryFee");
                orderCarriage.setText("¥ " + deliveryFee);

                //订单金额
                String paymentPrice = JsonUitls.getString(resultJson.getJSONObject("payment"), "totalPay");
                orderCount.setText("¥ " + paymentPrice);

                //创建时间
                String createdTimeT = resultJson.optString("createdTime");
                createdTime.setText(createdTimeT);
                //订单状态
                String orderStatusStr = resultJson.optString("orderStatusStr");
                tvOrderPlayed.setText(orderStatusStr);

                //结算状态
//                String settleStatesDesc = JsonUitls.getString(resultJson, "settleStatesDesc");
//                if ("null".equals(settleStatesDesc)) {
//                    settleStatesDesc = "-";
//                }
//                balance_status.setText(settleStatesDesc);

                //产品列表
                JSONArray skuInfoList = JsonUitls.getJSONArray(resultJson, "orderSkus");
                List<ProductEntity> productEntityList = new ArrayList<>();
                for (int i = 0; i < skuInfoList.length(); i++) {
                    JSONObject data = skuInfoList.getJSONObject(i);
                    String quantity = JsonUitls.getString(data, "quantity");
                    String paymentPriceItem = JsonUitls.getString(data, "paymentPrice");
                    String name = JsonUitls.getString(data, "name");
                    String imgUrl = JsonUitls.getString(data, "imgUrl");
                    ProductEntity entity = new ProductEntity(quantity, paymentPriceItem, name, imgUrl);
                    entity.name = JsonUitls.getString(data, "skuName");
                    entity.paymentPriceItem = JsonUitls.getString(data, "price");
                    productEntityList.add(entity);
                }

                OrderDetailProAdapter adapter = new OrderDetailProAdapter(productEntityList, this);
                productList.setAdapter(adapter);
                resetListView(productList, adapter);
                bean.orderStatus = resultJson.optInt("orderStatus");
                bean.expressType = resultJson.optString("expressType");
                bean.expressNumber = resultJson.optString("expressNumber");
                bean.expressCorpId = resultJson.optString("expressCorpId");
                bean.address = resultJson.optString("address");
                bean.fullDivisionName = resultJson.optString("fullDivisionName");
                bean.linkmanName = resultJson.optString("linkmanName");
                bean.linkmanPhone = resultJson.optString("linkmanPhone");
                if (bean.orderStatus == 5) {//已拆单
                    layoutExpressStatus.setVisibility(View.VISIBLE);
                    layoutExpressName.setVisibility(View.GONE);
                    layoutExpressNo.setVisibility(View.GONE);
                    loadWuliuNo();
                } else {
                    layoutExpressStatus.setVisibility(View.GONE);
                    layoutExpressName.setVisibility(View.VISIBLE);
                    layoutExpressNo.setVisibility(View.VISIBLE);
                    tvExpressName.setText(bean.expressType);
                    tvExpressNo.setText(bean.expressNumber);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void resetListView(ListView listView, OrderDetailProAdapter adapter) {
        int count = adapter.getCount();
        int totalHeight = 0;
        for (int i = 0; i < count; i++) {
            View view = adapter.getView(i, null, listView);
            view.measure(0, 0);
            int measuredHeight = view.getMeasuredHeight();
            totalHeight += measuredHeight;
        }
        ViewGroup.LayoutParams lp = listView.getLayoutParams();
        lp.height = totalHeight + listView.getDividerHeight() * (count - 1);
        listView.setLayoutParams(lp);
        listView.requestLayout();
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvExpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(activity, ExpressActivity.class);
                intent.putExtra("order", bean);
                startActivity(intent);
            }
        });
        reconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadInfo();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

}
