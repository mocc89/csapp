package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.AgencyDailyDetailEntity;
import store.viomi.com.system.callback.ADItemOpenCallBack;
import store.viomi.com.system.utils.NumberUtil;
import store.viomi.com.system.widget.PinnedSectionListView;

/**
 * Created by viomi on 2017/1/10.
 */

public class AgencyDailyAdapter extends BaseAdapter implements PinnedSectionListView.PinnedSectionListAdapter {

    private List<AgencyDailyDetailEntity> list;
    private Context context;
    private ADItemOpenCallBack adItemOpenCallBack;
    private LayoutInflater inflater;


    public AgencyDailyAdapter(List<AgencyDailyDetailEntity> list, Context context, ADItemOpenCallBack adItemOpenCallBack) {
        this.list = list;
        this.context = context;
        this.adItemOpenCallBack = adItemOpenCallBack;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == 0;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getvType();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;

        int vtype = getItemViewType(position);
        if (convertView == null) {
            switch (vtype) {
                case 0: {
                    convertView = inflater.inflate(R.layout.agency_daily_item_1, null);
                    holder1 = new ViewHolder1();
                    holder1.date = (TextView) convertView.findViewById(R.id.date);
                    holder1.paramer_title = (TextView) convertView.findViewById(R.id.paramer_title);
                    convertView.setTag(holder1);
                    break;
                }
                case 1: {
                    convertView = inflater.inflate(R.layout.agency_daily_item_2, null);
                    holder2 = new ViewHolder2();
                    holder2.agency_name = (TextView) convertView.findViewById(R.id.agency_name);
                    holder2.city_name = (TextView) convertView.findViewById(R.id.city_name);
                    holder2.count = (TextView) convertView.findViewById(R.id.count);
                    holder2.new_store = (TextView) convertView.findViewById(R.id.new_store);
                    holder2.new_clerk = (TextView) convertView.findViewById(R.id.new_clerk);
                    holder2.new_pt_clerk = (TextView) convertView.findViewById(R.id.new_pt_clerk);
                    holder2.more = (ImageView) convertView.findViewById(R.id.more);
                    holder2.more_layout = (LinearLayout) convertView.findViewById(R.id.more_layout);
                    holder2.item_layout = (LinearLayout) convertView.findViewById(R.id.item_layout);
                    holder2.order_count = (TextView) convertView.findViewById(R.id.order_count);
                    holder2.order_amount = (TextView) convertView.findViewById(R.id.order_amount);
                    holder2.user_count = (TextView) convertView.findViewById(R.id.user_count);
                    convertView.setTag(holder2);
                    break;
                }

                case 2: {
                    convertView = inflater.inflate(R.layout.agency_daily_item_3, null);
                    holder3 = new ViewHolder3();
                    convertView.setTag(holder3);
                    break;
                }
            }
        } else {
            switch (vtype) {
                case 0: {
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;
                }
                case 1: {
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;
                }
                case 2: {
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
                }
            }

        }

        switch (vtype) {
            case 0: {
                holder1.date.setText(list.get(position).getDate());
                holder1.paramer_title.setText(list.get(position).getParamer_title());
                break;
            }
            case 1: {
                holder2.agency_name.setText(list.get(position).getAgencyName());
                holder2.city_name.setText(list.get(position).getCityName());
                holder2.count.setText(list.get(position).getSelectParams());
                holder2.new_store.setText("" + list.get(position).getTerminalCount());
                holder2.new_clerk.setText("" + list.get(position).getStaffCount());
                holder2.new_pt_clerk.setText("" + list.get(position).getParttimeCount());
                holder2.order_count.setText("" + list.get(position).getOrderCount());

                holder2.order_amount.setText("¥" + NumberUtil.getValue2(list.get(position).getOrderAmount()) );
                holder2.user_count.setText("" + list.get(position).getUserCount());

                if (list.get(position).isopen()) {
                    holder2.more_layout.setVisibility(View.VISIBLE);
                    holder2.more.setImageResource(R.drawable.up);
                } else {
                    holder2.more_layout.setVisibility(View.GONE);
                    holder2.more.setImageResource(R.drawable.down);
                }

                holder2.item_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        adItemOpenCallBack.openClickCallBack(position);
                    }
                });
                break;
            }

            case 2: {
                break;
            }
        }

        return convertView;
    }

    class ViewHolder1 {
        TextView date, paramer_title;
    }

    class ViewHolder2 {
        TextView agency_name, city_name, count, new_store, new_clerk, new_pt_clerk, order_count, order_amount, user_count;
        ImageView more;
        LinearLayout more_layout, item_layout;
    }

    class ViewHolder3 {

    }
}
