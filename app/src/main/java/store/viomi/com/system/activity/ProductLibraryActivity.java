package store.viomi.com.system.activity;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.adapter.Advance2Adapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.ChannelPurchaseOrderDetailResp;
import store.viomi.com.system.bean.Product;
import store.viomi.com.system.bean.ProductCat;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;

public class ProductLibraryActivity extends BaseActivity {
    final int REQUEST_SEARCH = 12;

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.select)
    ImageView select;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.tv_lib)
    TextView tvLib;
    @BindView(R.id.listview)
    ListView listview;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_stock)
    TextView tvStock;
    @BindView(R.id.tv_stock_waiting)
    TextView tvStockWaiting;
    @BindView(R.id.tv_type)
    TextView tvType;
    @BindView(R.id.reconnect_btn)
    TextView reconnectBtn;
    @BindView(R.id.reconnect_layout)
    RelativeLayout reconnectLayout;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.loading_layout)
    RelativeLayout loadingLayout;
    Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    loadingfinish();
                    String result = (String) message.obj;
                    LogUtil.mlog(TAG, result);
                    mList.clear();
                    parseJsonList(result);
                    break;
                case 1:
                    if (reconnectLayout != null)
                        reconnectLayout.setVisibility(View.VISIBLE);
                    loadingfinish();
                    break;
            }
            return true;
        }
    });
    List<Product> mList = new ArrayList<>();
    ThisAdapter adapter;
    String saleStatus = "", prodName = "";

    @Override
    protected void init() {
        setContentView(R.layout.activity_product_library);
        ButterKnife.bind(this);
        reconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadList();
            }
        });
        loadList();
    }

    private void parseJsonList(String result1) {
        try {
            JSONObject json = new JSONObject(result1).optJSONObject("mobBaseRes");
            String code = JsonUitls.getString(json, "code");
//            String desc = JsonUitls.getString(json, "desc");
            JSONArray result = JsonUitls.getJSONArray(json, "result");
            if (result != null)
                for (int i = 0; i < result.length(); i++) {
                    JSONObject object = result.getJSONObject(i);
                    Product resp = new Product();
                    resp.name = object.optString("name");
                    resp.type_name = object.optString("type_name");
                    resp.pending_count = object.optLong("pending_count");
                    resp.stock = object.optLong("stock");
                    mList.add(resp);
                }
            adapter = new ThisAdapter();
            listview.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class ThisAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mList.size();
        }

        @Override
        public Object getItem(int i) {
            return mList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder holder;
            Product product = mList.get(i);
            if (view == null) {
                view = View.inflate(activity, R.layout.product_library_item, null);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (i % 2 > 0) {
                holder.layoutItem.setBackgroundColor(Color.WHITE);
            } else {
                holder.layoutItem.setBackgroundColor(Color.parseColor("#FFF6F6F6"));
            }
            holder.tvName.setText(product.name);
            holder.tvType.setText(product.type_name);
            holder.tvStock.setText(product.stock + "");
            holder.tvStockWaiting.setText(product.pending_count + "");
            return view;
        }

        class ViewHolder {
            @BindView(R.id.tv_name)
            TextView tvName;
            @BindView(R.id.tv_stock)
            TextView tvStock;
            @BindView(R.id.tv_stock_waiting)
            TextView tvStockWaiting;
            @BindView(R.id.tv_type)
            TextView tvType;
            @BindView(R.id.layout_item)
            View layoutItem;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, ProductSearchActivity.class);
                intent.putExtra("prodName", prodName);
                startActivityForResult(intent, REQUEST_SEARCH);
            }
        });
        reconnectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadList();
            }
        });
    }

    ArrayList<ProductCat> mListCats = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SEARCH && data != null) {
                prodName = data.getStringExtra("prodName");
                saleStatus = data.getStringExtra("saleStatus");
                mListCats.clear();

                mListCats = data.getParcelableArrayListExtra("catalogIds");
                loadList();
                String s = "";
                List<String> list = new ArrayList<>();
                if (!TextUtils.isEmpty(prodName))
                    list.add(prodName);
                if (mListCats != null) {
                    for (ProductCat cat : mListCats) {
                        list.add(cat.value);
                    }
                }
                if (!TextUtils.isEmpty(saleStatus)) {
                    if (saleStatus.equals("0")) {
                        list.add("预售");
                    } else if (saleStatus.equals("1")) {
                        list.add("正常销售");
                    }
                }
                if (list.size() > 0)
                    for (int i = 0; i < list.size(); i++) {
                        s += list.get(i);
                        if (i != list.size() - 1) {
                            s += ",";
                        }
                    }
                tvLib.setText(s);
            }
        }
    }

    private void loadList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.WARES_SALESSTOCK);
        requestParams.addBodyParameter("prodName", prodName);
        requestParams.addBodyParameter("saleStatus", saleStatus); //商品在售状态：0预售,1在售,null时全部
        String s = "";
        if (mListCats.size() > 0) {
            for (int i = 0; i < mListCats.size(); i++) {
                s += mListCats.get(i).id;
                if (i < mListCats.size() - 1) {
                    s += ",";
                }
            }
        }
        requestParams.addBodyParameter("catalogIds", s); //商品所属类目id：多个以,分割
        requestParams.addBodyParameter("stockCountRange", "");
        requestParams.addBodyParameter("pendingCountRange", "");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    @Override
    protected void loading() {
        if (loadingLayout != null)
            loadingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        if (loadingLayout != null)
            loadingLayout.setVisibility(View.GONE);
    }

}
