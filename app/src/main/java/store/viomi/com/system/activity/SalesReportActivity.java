package store.viomi.com.system.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.ASalesFgAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.fragment.ASalesFragment;


@ContentView(R.layout.activity_sales_report)
public class SalesReportActivity extends BaseActivity {


    @ViewInject(R.id.back)
    private ImageView back;
    @ViewInject(R.id.tab)
    private TabLayout tab;
    @ViewInject(R.id.tab_vp)
    private ViewPager tab_vp;


    @Override
    protected void init() {

        String[] titles = new String[]{"昨日数据", "本月数据", "全部数据"};
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(ASalesFragment.getInstance("1"));
        fragments.add(ASalesFragment.getInstance("0"));
        fragments.add(ASalesFragment.getInstance("2"));
        ASalesFgAdapter adapter = new ASalesFgAdapter(getSupportFragmentManager(), fragments, titles);

        tab_vp.setOffscreenPageLimit(2);
        tab_vp.setAdapter(adapter);
        tab.setupWithViewPager(tab_vp);

        if ("month".equals(getIntent().getStringExtra("type"))) {
            tab_vp.setCurrentItem(1);
        }

    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

}
