package store.viomi.com.system.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.xutils.x;


public abstract class BaseFragment extends Fragment {
    public String TAG = this.getClass().getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = x.view().inject(this, inflater, container);
        init();
        initListener();
        return view;
    }

    protected abstract void init();

    protected abstract void initListener();

    protected abstract void loading();

    protected abstract void loadingfinish();

}
