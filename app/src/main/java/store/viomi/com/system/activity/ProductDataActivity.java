package store.viomi.com.system.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.fragment.ProductDataFragment;

/**
 * 商品数据
 */
@ContentView(R.layout.activity_product_data)
public class ProductDataActivity extends BaseActivity {
    final int REQUEAST_FILTER = 11;
    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.tv1)
    private TextView tv1;
    @ViewInject(R.id.tv2)
    private TextView tv2;
    @ViewInject(R.id.tv3)
    private TextView tv3;
    @ViewInject(R.id.tv4)
    private TextView tv4;

    @ViewInject(R.id.line1)
    private View line1;
    @ViewInject(R.id.line2)
    private View line2;
    @ViewInject(R.id.line3)
    private View line3;
    @ViewInject(R.id.line4)
    private View line4;
    @ViewInject(R.id.search)
    private View tvSearch;

    private ProductDataFragment fg1;
    private ProductDataFragment fg2;
    private ProductDataFragment fg3;
    private ProductDataFragment fg4;
    private Fragment currentFragment = new Fragment();

    @Override
    protected void init() {
        fg1 = ProductDataFragment.genInstance("YUN_MI");
        fg2 = ProductDataFragment.genInstance("MI_JIA");
        fg3 = ProductDataFragment.genInstance("TIAN_MAO");
        fg4 = ProductDataFragment.genInstance("YU_FU_KUAN");
        setNavigation(tv1, tv2, tv3, tv4, line1, line2, line3, line4, 1);
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv1, tv2, tv3, tv4, line1, line2, line3, line4, 1);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv2, tv1, tv3, tv4, line2, line1, line3, line4, 2);
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv3, tv2, tv1, tv4, line3, line2, line1, line4, 3);
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv4, tv3, tv2, tv1, line4, line3, line2, line1, 4);
            }
        });
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, AdvanceOrder2FilterActivity.class);
                startActivityForResult(intent, REQUEAST_FILTER);
            }
        });
    }

    private void setNavigation(TextView tv1, TextView tv2, TextView tv3, TextView tv4, View line1, View line2, View line3, View line4, int type) {
        tv1.setTextColor(getResources().getColor(R.color.selectItem));
        tv2.setTextColor(getResources().getColor(R.color.unselectItem));
        tv3.setTextColor(getResources().getColor(R.color.unselectItem));
        tv4.setTextColor(getResources().getColor(R.color.unselectItem));
        line1.setVisibility(View.VISIBLE);
        line2.setVisibility(View.INVISIBLE);
        line3.setVisibility(View.INVISIBLE);
        line4.setVisibility(View.INVISIBLE);

        Fragment fragment = null;
        switch (type) {
            case 1: {
                fragment = fg1;
                break;
            }
            case 2: {
                fragment = fg2;
                break;
            }
            case 3: {
                fragment = fg3;
                break;
            }
            case 4: {
                fragment = fg4;
                break;
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.hide(currentFragment).show(fragment);
        } else {
            transaction.hide(currentFragment).add(R.id.lin, fragment);
        }
        transaction.commit();
        currentFragment = fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == REQUEAST_FILTER && data != null) {
                fg1.onNewFilter(data);
                fg2.onNewFilter(data);
                fg3.onNewFilter(data);
                fg4.onNewFilter(data);
            }
        }
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }
}
