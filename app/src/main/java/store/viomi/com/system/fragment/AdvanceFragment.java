package store.viomi.com.system.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.AdvanceAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.AdvanceEntity;
import store.viomi.com.system.callback.ItemOpenCallBack;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.other.LebalSet;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.TimeUtils;


@ContentView(R.layout.fragment_advance)
public class AdvanceFragment extends BaseFragment implements ItemOpenCallBack {

    @ViewInject(R.id.orderlist)
    private ListView orderlist;

    //加载中布局
    @ViewInject(R.id.loading_layout)
    private RelativeLayout loading_layout;
    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    private String typeNo;
    private String text;
    private List<AdvanceEntity> aelist;
    private AdvanceAdapter adapter;
    private Map<Integer, String> datamap;
    private int currentTag;
    private int insertPosition;
    private boolean isdestroy = false;

    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (!isdestroy) {
                switch (msg.what) {
                    case 0: {
                        loadingfinish();
                        String result1 = (String) msg.obj;
                        LogUtil.mlog("viomi1", result1);
                        reconnect_layout.setVisibility(View.GONE);
                        parseJson(result1);
                        break;
                    }
                    case 1: {
                        reconnect_layout.setVisibility(View.VISIBLE);
                        loadingfinish();
                        break;
                    }
                    case 2: {
                        loadingfinish();
                        String result = (String) msg.obj;
                        LogUtil.mlog("viomi_detail", result);
                        datamap.put(currentTag, result);
                        parseJson2(result);
                        break;
                    }
                    case 3: {
                        loadingfinish();
                        ResponseCode.onErrorHint(msg.obj);
                        break;
                    }
                }
            }
            return false;
        }
    });

    public static AdvanceFragment newInstance(String type, String text) {
        AdvanceFragment fragment = new AdvanceFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        args.putString("text", text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void init() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNo = arguments.getString("type", "null");
            text = arguments.getString("text", "null");
        }
        datamap = new HashMap<>();
        loadList();
    }

    private void loadList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ADVANCELIST);
        requestParams.addBodyParameter("type", typeNo);
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void parseJson(String result1) {
        aelist = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(result1);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");

            JSONArray resultArray = JsonUitls.getJSONArray(json, "result");
            for (int i = 0; i < resultArray.length(); i++) {
                JSONObject item = resultArray.getJSONObject(i);

                String dateStrS = JsonUitls.getString(item, "dateStr");

                long begintime = JsonUitls.getLong(item, "beginDate");
                long endtime = JsonUitls.getLong(item, "endDate");
                JSONObject data = JsonUitls.getJSONObject(item, "data");
                int purchaseOrderCount = JsonUitls.getInt(data, "purchaseOrderCount");
                double purchaseOrderAmount = JsonUitls.getDouble(data, "purchaseOrderAmount");

                String dateStr = null;
                String dateStr2 = null;

                switch (typeNo) {
                    case "1":
                        dateStr = TimeUtils.stampToMMdd(begintime);
                        dateStr2 = TimeUtils.stampToMMdd(begintime);
                        if (i == 0) {
                            dateStr = "昨日";
                            dateStr2 = "昨日";
                        }
                        break;
                    case "2":
                        dateStr = TimeUtils.stampToMMdd(begintime) + "-\n" + TimeUtils.stampToMMdd(endtime);
                        dateStr2 = TimeUtils.stampToyyyyMMdd(begintime) + "-\n" + TimeUtils.stampToyyyyMMdd(endtime);
                        if (i == 0) {
                            dateStr = "本周";
                            dateStr2 = "本周";
                        }
                        break;
                    case "3":
                        dateStr = String.format("%tB", new Date(begintime));
                        dateStr2 = String.format("%tB", new Date(begintime));
                        if ("一月".equals(dateStr) || "十二月".equals(dateStr)) {
                            dateStr = dateStr + "\n" + new SimpleDateFormat("yyyy").format(new Timestamp(begintime));
                            dateStr2 = new SimpleDateFormat("yyyy").format(new Timestamp(begintime)) + "," + dateStr2;
                        }

                        break;
                }

                aelist.add(new AdvanceEntity(0, dateStr, dateStr2, purchaseOrderCount, begintime, endtime, purchaseOrderAmount, false, i + 1));
            }

            setdata();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setdata() {

        adapter = new AdvanceAdapter(aelist, getActivity(), this);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.advance_order_header, null);
        LineChart mChart = (LineChart) view.findViewById(R.id.chart1);
        TextView range = (TextView) view.findViewById(R.id.range);
        LinearLayout lebal_layout = (LinearLayout) view.findViewById(R.id.lebal_layout);
        TextView x1 = (TextView) view.findViewById(R.id.x1);
        TextView x2 = (TextView) view.findViewById(R.id.x2);
        TextView x3 = (TextView) view.findViewById(R.id.x3);

        range.setText(text);

        // no description text 图例描述取消
        mChart.getDescription().setEnabled(false);
        // enable touch gestures图表上不可点击
        mChart.setTouchEnabled(false);
        mChart.setDragDecelerationFrictionCoef(0.9f);
        // enable scaling and dragging
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(false);
        mChart.setDrawGridBackground(false);
        mChart.setHighlightPerDragEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        ArrayList<Entry> yVals1 = new ArrayList<>();
        List<String> slist = new ArrayList<>();

        float max = 0;
        for (int j = 0; j < aelist.size(); j++) {
            int i = aelist.size() - 1 - j;
            yVals1.add(new Entry(j, (float) aelist.get(i).getStoreOrderSum()));
            TextView tv = new TextView(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
            tv.setLayoutParams(lp);
            tv.setTextSize(10);
            tv.setGravity(Gravity.CENTER);

            tv.setText(aelist.get(i).getTime());
            lebal_layout.addView(tv);
            if (aelist.get(j).getStoreOrderSum() > max) {
                max = (float) aelist.get(j).getStoreOrderSum();
            }
        }

        int maxValue = 6;
        int[] yvalus = new int[]{6, 15, 30, 60, 90, 150, 300, 600, 1200, 2400, 4800, 9000, 18000, 36000, 72000, 144000, 300000, 600000, 1200000, 2400000, 4800000, 9600000, 19200000, 60000000, 120000000};
        for (int i = 0; i < yvalus.length; i++) {
            if (yvalus[i] > max) {
                maxValue = yvalus[i];
                break;
            }
        }

        x1.setText("¥" + maxValue);
        x2.setText("¥" + maxValue * 2 / 3);
        x3.setText("¥" + maxValue * 1 / 3);

        LineDataSet set1;

        if (mChart.getData() != null && mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);

            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(yVals1, "DataSet 1");

            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            //折线图线的颜色
            set1.setColor(Color.rgb(160, 206, 233));
            //折线图圆点颜色
            set1.setCircleColor(Color.rgb(29, 138, 203));
            set1.setLineWidth(2f);
            set1.setCircleRadius(4f);
            set1.setFillAlpha(65);
            set1.setFillColor(Color.rgb(244, 117, 117));
            set1.setHighLightColor(Color.rgb(244, 117, 117));
            set1.setDrawCircleHole(false);
            set1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return (int) value + "";
                }
            });

            // create a data object with the datasets
            LineData data = new LineData(set1);
            //线上数值颜色
            data.setValueTextColor(Color.rgb(26, 50, 73));
            data.setValueTextSize(9f);

            // set data
            mChart.setData(data);
        }
        mChart.animateX(1500);

        Legend l = mChart.getLegend();
        l.setEnabled(false);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextSize(11f);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(false);
//        xAxis.label
        xAxis.setValueFormatter(new LebalSet(slist));

        YAxis leftAxis = mChart.getAxisLeft();
        //左边Y轴坐标颜色
        leftAxis.setTextColor(Color.rgb(157, 157, 157));
        leftAxis.setAxisMaximum(maxValue);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        //左边y轴坐标竖线
        leftAxis.setDrawAxisLine(false);
        //设置表格里面横线的条数
        leftAxis.setLabelCount(3);
        //王炸
        leftAxis.setEnabled(false);

        YAxis rightAxis = mChart.getAxisRight();
        //不显示Y轴右边刻度
        rightAxis.setEnabled(false);

        orderlist.addHeaderView(view);
        orderlist.setAdapter(adapter);
    }

    private String getValueString(float value) {
        if (value >= 1000000000) {
            return (int) (value / 1000000000) + "kkk";
        }

        if (value >= 100000000) {
            return (int) (value / 1000000) + "kk";
        }

        if (value >= 10000000) {
            return (int) (value / 1000000) + "kk";
        }

        if (value >= 1000000) {
            return (int) (value / 100000) / 10.0 + "kk";
        }

        if (value >= 100000) {
            return (int) (value / 1000) + "k";
        }

        if (value >= 10000) {
            return (int) (value / 1000) + "k";
        }

        if (value >= 1000) {
            return (int) (value / 100) / 10.0 + "k";
        }

        if (value >= 100) {
            return "" + (int) value;
        }
        if (value >= 10) {
            return "" + (int) value;
        }
        return "" + (int) value;
    }


    @Override
    public void openClickCallBack(int tag) {
        queryDetail(tag);
    }

    private void queryDetail(int tag) {

        int position = 0;
        for (int i = 0; i < aelist.size(); i++) {
            if (tag == aelist.get(i).getTag()) {
                position = i;
                aelist.get(i).setIsopen(!aelist.get(i).isopen());
            } else {
                aelist.get(i).setIsopen(false);
            }
        }

        //判断展开 or 收起
        if (!aelist.get(position).isopen()) {
            Iterator<AdvanceEntity> iterator = aelist.iterator();
            while (iterator.hasNext()) {
                AdvanceEntity entity = iterator.next();
                if (entity.getType() == 1 || entity.getType() == 2) {
                    iterator.remove();
                }
            }
            adapter.notifyDataSetChanged();

        } else {

            //显示数据 判断订单数是否为0
            if (aelist.get(position).getOrderCount() == 0) {
                Iterator<AdvanceEntity> iterator = aelist.iterator();
                while (iterator.hasNext()) {
                    AdvanceEntity entity = iterator.next();
                    if (entity.getType() == 1 || entity.getType() == 2) {
                        iterator.remove();
                    }
                }
                for (int i = 0; i < aelist.size(); i++) {
                    if (tag == aelist.get(i).getTag()) {
                        position = i;
                        break;
                    }
                }
                aelist.add(position + 1, new AdvanceEntity(2));
                adapter.notifyDataSetChanged();
                orderlist.setSelection(position + 1);
            } else {

                Iterator<AdvanceEntity> iterator = aelist.iterator();
                while (iterator.hasNext()) {
                    AdvanceEntity entity = iterator.next();
                    if (entity.getType() == 1 || entity.getType() == 2) {
                        iterator.remove();
                    }
                }
                adapter.notifyDataSetChanged();
                for (int i = 0; i < aelist.size(); i++) {
                    if (tag == aelist.get(i).getTag()) {
                        position = i;
                        break;
                    }
                }

                currentTag = tag;
                insertPosition = position;
                //先从map获取，没有则从网络请求...
                String result = datamap.get(tag);

                if (result != null) {
                    parseJson2(result);
                } else {
                    RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ADVANCEDETAIL);
                    requestParams.addBodyParameter("beginDate", TimeUtils.stampToyyyyMMdd(aelist.get(position).getBegintime()));
                    requestParams.addBodyParameter("endDate", TimeUtils.stampToyyyyMMdd(aelist.get(position).getEndtime()));
                    loading();
                    RequstUtils.getRquest(requestParams, mhandler, 2, 3);
                }
            }
        }
    }

    private void parseJson2(String result) {
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                JSONArray resultArray = JsonUitls.getJSONArray(json, "result");
                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject item = resultArray.getJSONObject(i);
                    String channelName = JsonUitls.getString(item, "channelName");
                    String rootChannel = JsonUitls.getString(item, "rootChannel");
                    String secondChannel = JsonUitls.getString(item, "secondChannel");
                    int purchaseOrderCount = JsonUitls.getInt(item, "purchaseOrderCount");
                    double purchaseOrderAmount = JsonUitls.getDouble(item, "purchaseOrderAmount");
                    int purchaseMachineCount = JsonUitls.getInt(item, "purchaseMachineCount");
                    aelist.add(insertPosition + 1, new AdvanceEntity(1, channelName, secondChannel, rootChannel, purchaseMachineCount, purchaseOrderCount, purchaseOrderAmount));
                }
            } else {
                datamap.put(currentTag, null);
            }
            adapter.notifyDataSetChanged();
            orderlist.setSelection(insertPosition + 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //test
    private void setOpen(int tag) {
        Iterator<AdvanceEntity> iterator = aelist.iterator();
        while (iterator.hasNext()) {
            AdvanceEntity entity = iterator.next();
            if (entity.getType() == 1 || entity.getType() == 2) {
                iterator.remove();
            }
        }

        int position = 0;
        for (int i = 0; i < aelist.size(); i++) {
            if (tag == aelist.get(i).getTag()) {
                position = i;
                aelist.get(i).setIsopen(!aelist.get(i).isopen());
            } else {
                aelist.get(i).setIsopen(false);
            }
        }

        if (aelist.get(position).isopen()) {

            if (aelist.get(position).getOrderCount() == 0) {
                aelist.add(position + 1, new AdvanceEntity(2));
            } else {
                aelist.add(position + 1, new AdvanceEntity(1, "琶洲实体店", "云米经销商", "云米虚拟城市运营", 2, 2, 6666));
                aelist.add(position + 1, new AdvanceEntity(1, "琶洲实体店", "云米经销商", "云米虚拟城市运营", 2, 2, 6666));
                aelist.add(position + 1, new AdvanceEntity(1, "琶洲实体店", "云米经销商", "云米虚拟城市运营", 2, 2, 6666));
            }

        }
        adapter.notifyDataSetChanged();
        if (aelist.get(position).isopen()) {
            orderlist.setSelection(position + 1);
        }
    }

    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadList();
            }
        });
    }

    @Override
    protected void loading() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_layout.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mhandler.removeCallbacksAndMessages(null);
        isdestroy = true;
    }
}