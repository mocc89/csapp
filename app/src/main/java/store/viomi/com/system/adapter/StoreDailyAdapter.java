package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.StoreDailyEntity;

/**
 * Created by viomi on 2016/11/21.
 */

public class StoreDailyAdapter extends BaseAdapter {
    private List<StoreDailyEntity> list;
    private Context context;
    private LayoutInflater inflater;

    public StoreDailyAdapter(List<StoreDailyEntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.store_daily_item_layout, null);
            holder = new ViewHolder();
            holder.itemlayout = (LinearLayout) convertView.findViewById(R.id.itemlayout);
            holder.day = (TextView) convertView.findViewById(R.id.day);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.staff = (TextView) convertView.findViewById(R.id.staff);
            holder.order = (TextView) convertView.findViewById(R.id.order);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String day = list.get(position).getDay();
        holder.day.setText(day);
        holder.name.setText(list.get(position).getChannelName());
        holder.staff.setText(list.get(position).getUserCount());
        holder.order.setText(list.get(position).getOrderCount());

        if (position % 2 == 0) {
            holder.itemlayout.setBackgroundResource(R.color.item_white);
        } else {
            holder.itemlayout.setBackgroundResource(R.color.item_dark);
        }

        return convertView;
    }

    class ViewHolder {
        TextView day, name, staff, order;
        LinearLayout itemlayout;
    }
}
