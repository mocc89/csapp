package store.viomi.com.system.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import store.viomi.com.system.R;
import store.viomi.com.timepickerlibrary.timepicker.DatePicker;

public class StoreDailySelectActivity extends AppCompatActivity {

    private ImageView back;
    private EditText name;
    private TextView start_txt;
    private TextView end_txt;
    private EditText dis_agency_name;
    private Button select;

    private String starttime = "";
    private String starttimeshow = "";
    private String endtime = "";
    private String endtimeshow = "";

    private final int RESULTCODE = 1112;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_daily_select);

        back = (ImageView) findViewById(R.id.back);
        name = (EditText) findViewById(R.id.name);
        start_txt = (TextView) findViewById(R.id.start_txt);
        end_txt = (TextView) findViewById(R.id.end_txt);
        dis_agency_name=(EditText) findViewById(R.id.dis_agency_name);
        select = (Button) findViewById(R.id.select);

        LinearLayout lin = (LinearLayout) findViewById(R.id.lin);

        lin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        start_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(StoreDailySelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2020, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = (c.get(Calendar.MONTH));
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }

                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        starttime = year + "-" + month + "-" + day + " 00:00";
                        starttimeshow = year + "-" + month + "-" + day;
                        start_txt.setText(starttimeshow);
                    }
                });
                picker.show();
            }
        });

        end_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker picker = new DatePicker(StoreDailySelectActivity.this);
                picker.setRangeStart(2014, 1, 1);
                picker.setRangeEnd(2020, 12, 31);

                try {
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = (c.get(Calendar.MONTH));
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    picker.setSelectedItem(year, month + 1, day);
                } catch (Exception e) {
                    picker.setSelectedItem(2016, 1, 1);
                }

                picker.setOnDatePickListener(new DatePicker.OnYearMonthDayPickListener() {
                    @Override
                    public void onDatePicked(String year, String month, String day) {
                        endtime = year + "-" + month + "-" + day + " 23:59";
                        endtimeshow = year + "-" + month + "-" + day;
                        end_txt.setText(endtimeshow);
                    }
                });
                picker.show();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("starttime", starttime);
                intent.putExtra("endtime", endtime);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("disagency", dis_agency_name.getText().toString());
                setResult(RESULTCODE, intent);
                finish();
            }
        });
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}