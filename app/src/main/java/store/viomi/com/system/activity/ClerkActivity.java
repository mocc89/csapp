package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.ClerkAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.ClerkEntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_clerk)
public class ClerkActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;

    @ViewInject(R.id.clerklist)
    private ListView clerklist;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    //没有数据
    @ViewInject(R.id.nodata_layout)
    private RelativeLayout nodata_layout;
    @ViewInject(R.id.select_again)
    private TextView select_again;


    int currentPage = 1;
    int pageSize = 50;

    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;       // 当前窗口可见项总数
    private boolean isFirsrRequest = true;//是否第一次加载


    private int totalPageNum;
    private List<ClerkEntity> elist;
    private boolean isFirsrPage = true;
    private ClerkAdapter adapter;

    private final int REQUSTCODE = 1007;
    private final int RESULTCODE = 1008;

    private String usernamerq;
    private String phonerq;
    private String rolerq;
    private String channelNamerq;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }

                case 8: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                }

                case 9: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {
        elist = new ArrayList<>();
        loadFirstInfo();
    }

    private void loadFirstInfo() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CLERKLIST);

        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);

        requestParams.addBodyParameter("username", usernamerq);//登陆用户名,手机号
        requestParams.addBodyParameter("contactName", phonerq);//用户名称
        requestParams.addBodyParameter("role", rolerq);//用户角色
        requestParams.addBodyParameter("channelName", channelNamerq);//渠道名称
        requestParams.addBodyParameter("root", "true");//是否递归查询全部
        requestParams.addBodyParameter("beginTime", "");
        requestParams.addBodyParameter("endTime", "");

        loading();
        RequstUtils.getRquest(requestParams, mhandler, 8, 9);
    }


    private void loadInfo() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CLERKLIST);

        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);

        requestParams.addBodyParameter("username", usernamerq);//登陆用户名,手机号
        requestParams.addBodyParameter("contactName", phonerq);//用户名称
        requestParams.addBodyParameter("role", rolerq);//用户角色
        requestParams.addBodyParameter("channelName", channelNamerq);//渠道名称
        requestParams.addBodyParameter("root", "true");//是否递归查询全部
        requestParams.addBodyParameter("beginTime", "");
        requestParams.addBodyParameter("endTime", "");

        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void parseJSON(String result) {
        try {
            JSONObject json = new JSONObject(result);

            JSONObject commonGenericResultRes = JsonUitls.getJSONObject(json, "mobBaseRes");

            String code = JsonUitls.getString(commonGenericResultRes, "code");
            String desc = JsonUitls.getString(commonGenericResultRes, "desc");

            if (ResponseCode.isSuccess(code, desc)) {

                currentPage++;

                JSONObject resultjson = JsonUitls.getJSONObject(commonGenericResultRes, "result");
                JSONArray list = JsonUitls.getJSONArray(resultjson, "list");

                if (isFirsrPage && list.length() == 0) {
                    nodata_layout.setVisibility(View.VISIBLE);
                    if (isFirsrRequest) {
                        select_again.setVisibility(View.GONE);
                    } else {
                        select_again.setVisibility(View.VISIBLE);
                    }
                    return;
                } else {
                    nodata_layout.setVisibility(View.GONE);
                }
                isFirsrRequest = false;

                for (int i = 0; i < list.length(); i++) {
                    JSONObject item = list.getJSONObject(i);
                    String name = JsonUitls.getString(item, "name");
                    String channelName = JsonUitls.getString(item, "channelName");
                    String roleStr = JsonUitls.getString(item, "roleStr");
                    String createdTime = JsonUitls.getString(item, "createdTime");
                    String mobile = JsonUitls.getString(item, "mobile");
                    String wechatNickName = JsonUitls.getString(item, "wechatNickName");
                    ClerkEntity entity = new ClerkEntity(name, channelName, roleStr, createdTime, mobile, wechatNickName);
                    elist.add(entity);
                }

                if (isFirsrPage) {
                    totalPageNum = resultjson.getInt("totalPageNum");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isFirsrPage) {
            isFirsrPage = false;
            adapter = new ClerkAdapter(elist, this);
            clerklist.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            clerklist.setSelection(visibleLastIndex - visibleItemCount + 2);
        }

    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClerkActivity.this, ClerkSelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        select_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClerkActivity.this, ClerkSelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });


        clerklist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                ClerkActivity.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstInfo();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUSTCODE && resultCode == RESULTCODE) {

            currentPage = 1;
            visibleLastIndex = 0;
            visibleItemCount = 0;
            elist = new ArrayList<>();
            isFirsrPage = true;

            phonerq = data.getStringExtra("username");
            usernamerq = data.getStringExtra("phone");
            rolerq = data.getStringExtra("role");
            channelNamerq = data.getStringExtra("channel_name");

            loadFirstInfo();

        }

    }

    private void loadmore() {
        loadInfo();
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
