package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.base.ProductBean;

public class ProductbtmAdapter extends BaseAdapter {

    private List<ProductBean> list;
    private Context context;
    private final LayoutInflater inflater;

    public ProductbtmAdapter(List<ProductBean> list, Context context) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.product_btm_item, null);
            holder.tv_pro_data = (TextView) convertView.findViewById(R.id.tv_pro_data);
            holder.tv_sale_num = (TextView) convertView.findViewById(R.id.tv_sale_num);
            holder.tv_sale_mey = (TextView) convertView.findViewById(R.id.tv_sale_mey);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProductBean pb = list.get(position);
        holder.tv_pro_data.setText(pb.name);
        holder.tv_sale_num.setText(pb.quantity);
        holder.tv_sale_mey.setText(pb.sales);

        return convertView;
    }

    private class ViewHolder {
        TextView tv_pro_data;
        TextView tv_sale_num;
        TextView tv_sale_mey;
    }

}
