package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.ClerkEntity;

/**
 * Created by viomi on 2016/11/15.
 */

public class ClerkAdapter extends BaseAdapter {

    private List<ClerkEntity> list;
    private Context context;
    private LayoutInflater inflater;

    public ClerkAdapter(List<ClerkEntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.clerk_item_layout, null);
            holder = new ViewHolder();
            holder.itemlayout = (RelativeLayout) convertView.findViewById(R.id.itemlayout);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.store_role = (TextView) convertView.findViewById(R.id.store_role);
            holder.created_time = (TextView) convertView.findViewById(R.id.created_time);
            holder.phone = (TextView) convertView.findViewById(R.id.phone);
            holder.wcname = (TextView) convertView.findViewById(R.id.wcname);
            holder.bindlogo = (ImageView) convertView.findViewById(R.id.bindlogo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ClerkEntity entity = list.get(position);

        holder.name.setText(entity.getName());
        holder.store_role.setText(entity.getChannelName()+" | "+entity.getRoleStr());
        holder.created_time.setText(entity.getCreatedTime().substring(0,10));
        holder.phone.setText("null".equals(entity.getMobile()) ? "-" : entity.getMobile());


        if ("-".equals(entity.getWechatNickName())) {
            holder.wcname.setText("未绑定");
            holder.bindlogo.setImageResource(R.drawable.unbindwc);
        } else {
            holder.wcname.setText(entity.getWechatNickName());
            holder.bindlogo.setImageResource(R.drawable.bindwc);
        }

        if (position % 2 == 0) {
            holder.itemlayout.setBackgroundResource(R.color.item_white);
        } else {
            holder.itemlayout.setBackgroundResource(R.color.item_dark);
        }

        return convertView;
    }

    class ViewHolder {

        RelativeLayout itemlayout;
        TextView name, store_role, created_time, phone, wcname;
        ImageView bindlogo;
    }


}
