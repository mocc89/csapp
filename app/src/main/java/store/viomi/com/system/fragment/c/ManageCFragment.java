package store.viomi.com.system.fragment.c;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.activity.AgencyActivity;
import store.viomi.com.system.activity.ClerkActivity;
import store.viomi.com.system.activity.StoreActivity;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import viomi.com.umsdk.MyMobclickAgent;

@ContentView(R.layout.fragment_manage_c)
public class ManageCFragment extends BaseFragment {

    @ViewInject(R.id.ptr_scroll)
    private PullToRefreshScrollView ptr_scroll;

    @ViewInject(R.id.store_count)
    private TextView store_count;

    @ViewInject(R.id.agency_count)
    private TextView agency_count;

    //店员管理
    @ViewInject(R.id.clerk_list)
    private LinearLayout clerk_list;

    //门店管理
    @ViewInject(R.id.store_list)
    private LinearLayout store_list;

    //经销商管理
    @ViewInject(R.id.agency_list)
    private LinearLayout agency_list;

    private String result1;
    private String result2;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    result1 = (String) msg.obj;
                    LogUtil.mlog("voimi1", result1);
                    loaddata2();
                    break;
                case 1:
                    result2 = (String) msg.obj;
                    LogUtil.mlog("voimi2", result2);
                    parseJSON();
                    break;

                case 7:
                    result1 = (String) msg.obj;
                    LogUtil.mlog("voimi1", result1);
                    refreshdata2();
                    break;
                case 8:
                    result2 = (String) msg.obj;
                    LogUtil.mlog("voimi2", result2);
                    parseJSON();
                    ptr_scroll.onRefreshComplete();
                    break;
                case 9:
                    ptr_scroll.onRefreshComplete();
                    ResponseCode.onErrorHint(msg.obj);
                    break;


            }
        }
    };


    private static ManageCFragment fragment;

    public static ManageCFragment getInstance() {
        if (fragment == null) {
            synchronized (ManageCFragment.class) {
                if (fragment == null) {
                    fragment = new ManageCFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    protected void init() {
        loaddata1();

    }

    private void loaddata1() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", "");//渠道名称
        requestParams.addBodyParameter("pageNum", "" + "1");
        requestParams.addBodyParameter("pageSize", "" + "2");
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "2");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 2);
    }

    private void loaddata2() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", "");//渠道名称
        requestParams.addBodyParameter("pageNum", "" + "1");
        requestParams.addBodyParameter("pageSize", "" + "2");
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "1");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 1, 3);
    }

    private void refreshdata() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", "");//渠道名称
        requestParams.addBodyParameter("pageNum", "" + "1");
        requestParams.addBodyParameter("pageSize", "" + "2");
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "2");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 7, 9);
    }

    private void refreshdata2() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", "");//渠道名称
        requestParams.addBodyParameter("pageNum", "" + "1");
        requestParams.addBodyParameter("pageSize", "" + "2");
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "1");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 8, 9);
    }


    private void parseJSON() {

        try {
            JSONObject json1 = new JSONObject(result1);
            JSONObject json2 = new JSONObject(result2);

            String code1 = JsonUitls.getString(json1, "code");
            if ("100".equals(code1)) {
                JSONObject resultjson = JsonUitls.getJSONObject(json1, "result");
                String totalCount = JsonUitls.getString(resultjson, "totalCount");
                store_count.setText("null".equals(totalCount)?"-":totalCount);
            }

            String code2 = JsonUitls.getString(json2, "code");
            if ("100".equals(code2)) {
                JSONObject resultjson = JsonUitls.getJSONObject(json2, "result");
                String totalCount = JsonUitls.getString(resultjson, "totalCount");
                agency_count.setText("null".equals(totalCount)?"-":totalCount);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void initListener() {
        ptr_scroll.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                refreshdata();
            }
        });

        clerk_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_manager_icon1");

                Intent intent = new Intent(getActivity(), ClerkActivity.class);
                startActivity(intent);
            }
        });

        store_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_manager_icon2");

                Intent intent = new Intent(getActivity(), StoreActivity.class);
                startActivity(intent);
            }
        });

        agency_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_manager_icon3");

                Intent intent = new Intent(getActivity(), AgencyActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
    }

}
