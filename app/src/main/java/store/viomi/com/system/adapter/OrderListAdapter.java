package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;

/**
 * Created by Mocc on 2017/8/24
 */

public class OrderListAdapter extends BaseAdapter {

    private Context context;
    private List<OrderListBean> list;
    private LayoutInflater inflater;

    public OrderListAdapter(Context context, List<OrderListBean> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        switch (list.get(position).getProductBeanList().size()) {
            case 0:
                return 0;
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                return 2;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;

        if (convertView == null) {
            switch (getItemViewType(position)) {
                case 0: {
                    holder1 = new ViewHolder1();
                    convertView = inflater.inflate(R.layout.order_list_item_layout1, null);

                    holder1.orderNO = (TextView) convertView.findViewById(R.id.orderNO);
                    holder1.payStatus = (TextView) convertView.findViewById(R.id.payStatus);
                    holder1.payMode = (TextView) convertView.findViewById(R.id.payMode);
                    holder1.quantity1 = (TextView) convertView.findViewById(R.id.quantity1);
                    holder1.name1 = (TextView) convertView.findViewById(R.id.name1);
                    ;
                    holder1.createdTime = (TextView) convertView.findViewById(R.id.createdTime);
                    holder1.price = (TextView) convertView.findViewById(R.id.price);
                    holder1.iv1 = (ImageView) convertView.findViewById(R.id.iv1);

                    convertView.setTag(holder1);
                    break;
                }
                case 1: {
                    holder2 = new ViewHolder2();
                    convertView = inflater.inflate(R.layout.order_list_item_layout2, null);

                    holder2.orderNO = (TextView) convertView.findViewById(R.id.orderNO);
                    holder2.payStatus = (TextView) convertView.findViewById(R.id.payStatus);
                    holder2.payMode = (TextView) convertView.findViewById(R.id.payMode);
                    holder2.quantity1 = (TextView) convertView.findViewById(R.id.quantity1);
                    holder2.name1 = (TextView) convertView.findViewById(R.id.name1);
                    holder2.quantity2 = (TextView) convertView.findViewById(R.id.quantity2);
                    holder2.name2 = (TextView) convertView.findViewById(R.id.name2);
                    holder2.createdTime = (TextView) convertView.findViewById(R.id.createdTime);
                    holder2.price = (TextView) convertView.findViewById(R.id.price);
                    holder2.iv1 = (ImageView) convertView.findViewById(R.id.iv1);
                    holder2.iv2 = (ImageView) convertView.findViewById(R.id.iv2);

                    convertView.setTag(holder2);
                    break;
                }
                case 2: {
                    holder3 = new ViewHolder3();
                    convertView = inflater.inflate(R.layout.order_list_item_layout3, null);

                    holder3.orderNO = (TextView) convertView.findViewById(R.id.orderNO);
                    holder3.payStatus = (TextView) convertView.findViewById(R.id.payStatus);
                    holder3.payMode = (TextView) convertView.findViewById(R.id.payMode);
                    holder3.quantity1 = (TextView) convertView.findViewById(R.id.quantity1);
                    holder3.name1 = (TextView) convertView.findViewById(R.id.name1);
                    holder3.quantity2 = (TextView) convertView.findViewById(R.id.quantity2);
                    holder3.name2 = (TextView) convertView.findViewById(R.id.name2);
                    holder3.createdTime = (TextView) convertView.findViewById(R.id.createdTime);
                    holder3.price = (TextView) convertView.findViewById(R.id.price);
                    holder3.iv1 = (ImageView) convertView.findViewById(R.id.iv1);
                    holder3.iv2 = (ImageView) convertView.findViewById(R.id.iv2);

                    convertView.setTag(holder3);
                    break;
                }
            }
        } else {
            switch (getItemViewType(position)) {
                case 0: {
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;
                }
                case 1: {
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;
                }
                case 2: {
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
                }
            }
        }

        OrderListBean bean = list.get(position);

        switch (getItemViewType(position)) {
            case 0: {
//                holder1.orderNO.setText("NO." + (position +1) + " 订单编号：" + bean.getOrderNO());
                holder1.orderNO.setText((position +1) +" 订单编号：" + bean.getOrderNO());
                holder1.payStatus.setText(bean.getPayStatus());
                holder1.payMode.setText(bean.getPayMode() + " | ");
                holder1.createdTime.setText(bean.getCreatedTime());
                holder1.price.setText("￥" + bean.getPayPrice());

                List<OrderListProductBean> productBeanList = bean.getProductBeanList();
                if (productBeanList.size() >= 1) {
                    holder1.quantity1.setText("*" + productBeanList.get(0).getQuantity());
                    holder1.name1.setText(productBeanList.get(0).getName());
                    ImageOptions imageOptions = new ImageOptions.Builder()
                            .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                            .setFailureDrawableId(R.drawable.defult_img)
                            .setLoadingDrawableId(R.drawable.defult_img)
                            .build();
                    x.image().bind(holder1.iv1, productBeanList.get(0).getImgUrl(),imageOptions);
                }
                break;
            }

            case 1: {
//                holder2.orderNO.setText("NO." + (position +1) + " 订单编号：" + bean.getOrderNO());
                holder2.orderNO.setText((position +1) +" 订单编号：" + bean.getOrderNO());
                holder2.payStatus.setText(bean.getPayStatus());
                holder2.payMode.setText(bean.getPayMode() + " | ");
                holder2.createdTime.setText(bean.getCreatedTime());
                holder2.price.setText("￥" + bean.getPayPrice());

                List<OrderListProductBean> productBeanList = bean.getProductBeanList();
                if (productBeanList.size() >= 1) {
                    holder2.quantity1.setText("*" + productBeanList.get(0).getQuantity());
                    holder2.name1.setText(productBeanList.get(0).getName());
                    ImageOptions imageOptions = new ImageOptions.Builder()
                            .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                            .setFailureDrawableId(R.drawable.defult_img)
                            .setLoadingDrawableId(R.drawable.defult_img)
                            .build();
                    x.image().bind(holder2.iv1, productBeanList.get(0).getImgUrl(),imageOptions);
                }

                if (productBeanList.size() >= 2) {
                    holder2.quantity2.setText("*" + productBeanList.get(1).getQuantity());
                    holder2.name2.setText(productBeanList.get(1).getName());
                    x.image().bind(holder2.iv2, productBeanList.get(1).getImgUrl());
                }
                break;
            }

            case 2: {
//                holder3.orderNO.setText("NO." + (position +1)+ " 订单编号：" + bean.getOrderNO());
                holder3.orderNO.setText((position +1) +" 订单编号：" + bean.getOrderNO());
                holder3.payStatus.setText(bean.getPayStatus());
                holder3.payMode.setText(bean.getPayMode() + " | ");
                holder3.createdTime.setText(bean.getCreatedTime());
                holder3.price.setText("￥" + bean.getPayPrice());

                List<OrderListProductBean> productBeanList = bean.getProductBeanList();
                if (productBeanList.size() >= 1) {
                    holder3.quantity1.setText("*" + productBeanList.get(0).getQuantity());
                    holder3.name1.setText(productBeanList.get(0).getName());
                    x.image().bind(holder3.iv1, productBeanList.get(0).getImgUrl());
                }

                if (productBeanList.size() >= 2) {
                    holder3.quantity2.setText("*" + productBeanList.get(1).getQuantity());
                    holder3.name2.setText(productBeanList.get(1).getName());
                    ImageOptions imageOptions = new ImageOptions.Builder()
                            .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                            .setFailureDrawableId(R.drawable.defult_img)
                            .setLoadingDrawableId(R.drawable.defult_img)
                            .build();
                    x.image().bind(holder3.iv2, productBeanList.get(1).getImgUrl(),imageOptions);
                }
                break;
            }
        }


        return convertView;
    }

    class ViewHolder1 {
        TextView orderNO, payStatus, payMode, quantity1, name1, createdTime, price;
        ImageView iv1;
    }

    class ViewHolder2 {
        TextView orderNO, payStatus, payMode, quantity1, name1, quantity2, name2, createdTime, price;
        ImageView iv1, iv2;
    }

    class ViewHolder3 {
        TextView orderNO, payStatus, payMode, quantity1, name1, quantity2, name2, createdTime, price;
        ImageView iv1, iv2;
    }
}
