package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.AgencyAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.AgencyEntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_agency)
public class AgencyActivity extends BaseActivity {


    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;

    @ViewInject(R.id.agencylist)
    private ListView agencylist;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    //没有数据
    @ViewInject(R.id.nodata_layout)
    private RelativeLayout nodata_layout;
    @ViewInject(R.id.select_again)
    private TextView select_again;


    private int currentPage = 1;
    private int pageSize = 50;

    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;   // 当前窗口可见项总数
    private int totalPageNum;           //总页数
    private boolean isFirsrPage = true;//是否第一页
    private boolean isFirsrRequest = true;//是否第一次加载

    private final int REQUSTCODE = 1009;
    private final int RESULTCODE = 1010;

    private List<AgencyEntity> alist;
    private AgencyAdapter adapter;
    private String agencyname;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }

                case 8: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                }

                case 9: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {
        alist = new ArrayList<>();
        loadFirstInfo();
    }

    private void loadFirstInfo() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", agencyname);//渠道名称
        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "1");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 8, 9);
    }

    private void loadInfo() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELLIST);
        requestParams.addBodyParameter("name", agencyname);//渠道名称
        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);
        requestParams.addBodyParameter("templateId", "");
        requestParams.addBodyParameter("type", "1");//渠道类型 1代表分销商 2代表门店
        requestParams.addBodyParameter("divisionCode", "");//区域编码
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void parseJSON(String result) {

        try {
            JSONObject json = new JSONObject(result);

            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");

            if (ResponseCode.isSuccess(code, desc)) {

                currentPage++;

                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");
                JSONArray list = JsonUitls.getJSONArray(resultjson, "list");

                if (isFirsrPage && list.length() == 0) {
                    nodata_layout.setVisibility(View.VISIBLE);
                    if (isFirsrRequest) {
                        select_again.setVisibility(View.GONE);
                    } else {
                        select_again.setVisibility(View.VISIBLE);
                    }
                    return;
                } else {
                    nodata_layout.setVisibility(View.GONE);
                }
                isFirsrRequest = false;

                for (int i = 0; i < list.length(); i++) {
                    JSONObject item = list.getJSONObject(i);
                    String id = JsonUitls.getString(item, "id");
                    String name = JsonUitls.getString(item, "name");
                    String channelLevel = JsonUitls.getString(item, "channelLevel");
                    String staffNum = JsonUitls.getString(item, "staffNum");
                    String statusDesc = JsonUitls.getString(item, "statusDesc");
                    String approveStatusDesc = JsonUitls.getString(item, "approveStatusDesc");
                    String status = JsonUitls.getString(item, "status");
                    String approveStatus = JsonUitls.getString(item, "approveStatus");
                    alist.add(new AgencyEntity(id, name, channelLevel + "级经销商", staffNum, statusDesc, approveStatusDesc, status, approveStatus));
                }

                if (isFirsrPage) {
                    totalPageNum = resultjson.getInt("totalPageNum");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isFirsrPage) {
            isFirsrPage = false;
            adapter = new AgencyAdapter(alist, this);
            agencylist.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            agencylist.setSelection(visibleLastIndex - visibleItemCount + 2);
        }
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgencyActivity.this, AgencySelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        select_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgencyActivity.this, AgencySelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        agencylist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                AgencyActivity.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;

            }
        });

        agencylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(AgencyActivity.this, AgencyDetailActivity.class);
                intent.putExtra("id", alist.get(position).getId());
                intent.putExtra("num", alist.get(position).getStaffNum());
                startActivity(intent);
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstInfo();
            }
        });

    }

    private void loadmore() {
        loadInfo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUSTCODE && resultCode == RESULTCODE) {

            currentPage = 1;
            visibleLastIndex = 0;
            visibleItemCount = 0;
            alist = new ArrayList<>();
            isFirsrPage = true;

            agencyname = data.getStringExtra("name");

            loadFirstInfo();
        }
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
