package store.viomi.com.system.fragment.c;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.activity.AdvanceOrderActivity;
import store.viomi.com.system.activity.AgencyDailyActivity;
import store.viomi.com.system.activity.OrderProfileActivity;
import store.viomi.com.system.activity.SalesProfileActivity;
import store.viomi.com.system.activity.StoreDailyActivity;
import store.viomi.com.system.activity.c.COrderReportActivity;
import store.viomi.com.system.activity.c.CSalesReportActivity;
import store.viomi.com.system.adapter.UpdateTxAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.constants.Config;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.service.DownloadService;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.SharePreferencesUtils;
import store.viomi.com.system.utils.ToastUtil;
import store.viomi.com.system.widget.AutoScrollViewPager;
import store.viomi.com.system.widget.SmallRadioView;
import viomi.com.umsdk.MyMobclickAgent;

@ContentView(R.layout.fragment_table_c)
public class TableCFragment extends BaseFragment {

    @ViewInject(R.id.ptr_scroll)
    private PullToRefreshScrollView ptr_scroll;

    //轮播图
    @ViewInject(R.id.auto_vp)
    private AutoScrollViewPager auto_vp;
    @ViewInject(R.id.smallpoint)
    private SmallRadioView smallpoint;

    @ViewInject(R.id.order_count)
    private TextView order_count;
    @ViewInject(R.id.sales_count)
    private TextView sales_count;
    @ViewInject(R.id.month_order)
    private TextView month_order;
    @ViewInject(R.id.month_salse)
    private TextView month_salse;

    //今日、本月数据
    @ViewInject(R.id.today_order)
    private RelativeLayout today_order;
    @ViewInject(R.id.today_sales)
    private RelativeLayout today_sales;
    @ViewInject(R.id.cur_month_order)
    private RelativeLayout cur_month_order;
    @ViewInject(R.id.cur_month_sales)
    private RelativeLayout cur_month_sales;

    //数据报表
    @ViewInject(R.id.agency_daily)
    private LinearLayout agency_daily;
    //预付款提货
    @ViewInject(R.id.advance_order)
    private LinearLayout advance_order;
    //销售概况
    @ViewInject(R.id.sales_profile)
    private LinearLayout sales_profile;
    //订单概况
    @ViewInject(R.id.order_profile)
    private LinearLayout order_profile;
    //销售数据
    @ViewInject(R.id.sale_account)
    private LinearLayout sale_account;
    //订单数据
    @ViewInject(R.id.order_account)
    private LinearLayout order_account;
    //门店日报
    @ViewInject(R.id.store_daily)
    private LinearLayout store_daily;

    private static TableCFragment fragment;
    private List<ImageView> imglist;

    private String result1;
    private String result2;

    private String downlink = "";
    private String version = "";

    private final static int PERMISSION_WRITE_EX_REQCODE = 100100;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0:
                    result1 = (String) msg.obj;
                    LogUtil.mlog("viomi1", result1);
                    loaddata2();
                    break;
                case 1:
                    result2 = (String) msg.obj;
                    LogUtil.mlog("viomi2", result2);
                    parseJSON();

                    break;
                case 2:
                    break;
                case 3:
                    break;

                case 5:
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("update", result);
                    parseJson(result);
                    break;
                case 6:
                    loadingfinish();
                    break;

                case 7:
                    result1 = (String) msg.obj;
                    LogUtil.mlog("viomi1", result1);
                    refreshdata2();
                    break;
                case 8:
                    result2 = (String) msg.obj;
                    LogUtil.mlog("viomi2", result2);
                    parseJSON();
                    ptr_scroll.onRefreshComplete();
                    break;
                case 9:
                    ptr_scroll.onRefreshComplete();
                    ResponseCode.onErrorHint(msg.obj);
                    break;

            }
        }
    };


    public static TableCFragment getInstance() {
        if (fragment == null) {
            synchronized (TableCFragment.class) {
                if (fragment == null) {
                    fragment = new TableCFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    protected void init() {

        int[] imgs = new int[]{R.drawable.banner1, R.drawable.banner2, R.drawable.banner3};
        imglist = new ArrayList<>();
        for (int i = 0; i < imgs.length; i++) {
            ImageView iv = new ImageView(getActivity());
            iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            iv.setImageResource(imgs[i]);
            imglist.add(iv);
        }
        smallpoint.setCount(imglist.size());
        auto_vp.setCustomAdapterDatas(imglist);
        auto_vp.startAutoScroll();

        loaddata();
        reqUpdate();
    }

    private void loaddata() {
        //今日
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST);
        requestParams.addBodyParameter("type", "1");
        RequstUtils.getRquest(requestParams, mhandler, 0, 2);
    }

    private void loaddata2() {
        //本月
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST);
        requestParams.addBodyParameter("type", "0");
        RequstUtils.getRquest(requestParams, mhandler, 1, 3);
    }

    private void refreshdata() {
        //今日
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST);
        requestParams.addBodyParameter("type", "1");
        RequstUtils.getRquest(requestParams, mhandler, 7, 9);
    }

    private void refreshdata2() {
        //本月
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST);
        requestParams.addBodyParameter("type", "0");
        RequstUtils.getRquest(requestParams, mhandler, 8, 9);
    }

    private void parseJSON() {
        try {
            JSONObject json1 = new JSONObject(result1);
            JSONObject json2 = new JSONObject(result2);

            JSONObject mobBaseRes1 = JsonUitls.getJSONObject(json1, "mobBaseRes");
            String code1 = JsonUitls.getString(mobBaseRes1, "code");
            if ("100".equals(code1)) {

                JSONObject resultjson1 = JsonUitls.getJSONObject(mobBaseRes1, "result");
                String turnOver = JsonUitls.getString(resultjson1, "turnOver");
                String orderCount = JsonUitls.getString(resultjson1, "orderCount");
                order_count.setText("null".equals(orderCount) ? "-" : orderCount);
                sales_count.setText("null".equals(turnOver) ? "¥ -" : "¥ " + turnOver);
            }


            JSONObject mobBaseRes2 = JsonUitls.getJSONObject(json2, "mobBaseRes");
            String code2 = JsonUitls.getString(mobBaseRes2, "code");
            if ("100".equals(code2)) {

                JSONObject resultjson2 = JsonUitls.getJSONObject(mobBaseRes2, "result");
                String turnOver = JsonUitls.getString(resultjson2, "turnOver");
                String orderCount = JsonUitls.getString(resultjson2, "orderCount");

                month_order.setText("null".equals(orderCount) ? "-" : orderCount);
                month_salse.setText("null".equals(turnOver) ? "¥ -" : "¥ " + turnOver);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void initListener() {

        ptr_scroll.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ScrollView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ScrollView> refreshView) {
                refreshdata();
            }
        });

        auto_vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int i = position % imglist.size();
                smallpoint.setIndex(i);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        agency_daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon1");

                Intent intent = new Intent(getActivity(), AgencyDailyActivity.class);
                startActivity(intent);
            }
        });

        advance_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon2");

                Intent intent = new Intent(getActivity(), AdvanceOrderActivity.class);
                startActivity(intent);
            }
        });

        //销售概况
        sales_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon3");

                Intent intent = new Intent(getActivity(), SalesProfileActivity.class);
                startActivity(intent);
            }
        });

        //订单概况
        order_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon4");

                Intent intent = new Intent(getActivity(), OrderProfileActivity.class);
                startActivity(intent);
            }
        });

        //销售数据
        sale_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon5");

                Intent intent = new Intent(getActivity(), CSalesReportActivity.class);
                startActivity(intent);
            }
        });

        //订单数据
        order_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon6");

                Intent intent = new Intent(getActivity(), COrderReportActivity.class);
                startActivity(intent);
            }
        });

        //门店日报
        store_daily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_main_icon7");

                Intent intent = new Intent(getActivity(), StoreDailyActivity.class);
                startActivity(intent);
            }
        });

        today_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CSalesReportActivity.class);
                startActivity(intent);
            }
        });
        today_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CSalesReportActivity.class);
                startActivity(intent);
            }
        });
        cur_month_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CSalesReportActivity.class);
                intent.putExtra("type", "month");
                startActivity(intent);
            }
        });
        cur_month_sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CSalesReportActivity.class);
                intent.putExtra("type", "month");
                startActivity(intent);
            }
        });

    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

    private void reqUpdate() {
        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.UPDATE);
        requestParams.addBodyParameter("type", "version");
        requestParams.addBodyParameter("package", "store.viomi.com.system");
        requestParams.addBodyParameter("channel", Config.UPDATEENVIRONMENT);
        requestParams.addBodyParameter("p", "1");
        requestParams.addBodyParameter("l", "1");
        RequstUtils.getRquest(requestParams, mhandler, 5, 6);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONArray data = JsonUitls.getJSONArray(json, "data");
            if (data != null && data.length() > 0) {
                JSONObject item = data.getJSONObject(0);
                String detail = JsonUitls.getString(item, "detail");
                version = JsonUitls.getString(item, "code");
                downlink = JsonUitls.getString(item, "url");
                if (!Config.getCurrentversion(getActivity()).equals(version) && !"210".equals(version)) {
                    showUpdateDialog(detail);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(String code) {
        final Dialog dialog = new Dialog(getActivity(), R.style.selectorDialog);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.update_dialog_layout, null);
        dialog.setContentView(view);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);
        ListView tv_list = (ListView) view.findViewById(R.id.tv_list);
        TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
        TextView ok_update = (TextView) view.findViewById(R.id.ok_update);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < version.length(); i++) {
            sb.append(version.charAt(i));
            if (i != version.length() - 1) {
                sb.append(".");
            }
        }
        String version_a = sb.toString();
        update_title.setText("云分销V" + version_a + "版本发布啦，马上下载体验吧！");

        String[] splits = code.split("#");
        UpdateTxAdapter adapter = new UpdateTxAdapter(splits, getActivity());
        tv_list.setAdapter(adapter);

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUpdate();
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    private void downloadUpdate() {

        SharePreferencesUtils.getInstance().setDownlink(getActivity(), downlink);
        SharePreferencesUtils.getInstance().setAppversion(getActivity(),version);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int i = getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (i == -1) {
                ToastUtil.show(HintText.PERMISSIONDENY_WRITEEX);
                getActivity().requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EX_REQCODE);
                return;
            }
        }

        ToastUtil.show(HintText.BACKGROUNDDOWNLOAD);
        Intent intent = new Intent(getActivity(), DownloadService.class);
        intent.putExtra("downlink", downlink);
        intent.putExtra("version", version);
        getActivity().startService(intent);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
    }

}
