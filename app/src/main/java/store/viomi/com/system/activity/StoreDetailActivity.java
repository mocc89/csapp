package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.CommonVPAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_store_detail)
public class StoreDetailActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.agency_vp)
    private ViewPager agency_vp;

    @ViewInject(R.id.dotLayout)
    private LinearLayout dotLayout;

    @ViewInject(R.id.parentname)
    private TextView parentname;

    @ViewInject(R.id.channel_name)
    private TextView channel_name;

    @ViewInject(R.id.status)
    private TextView statustv;

    @ViewInject(R.id.agency_logo)
    private ImageView agency_logo;

    @ViewInject(R.id.staff_num)
    private TextView staff_num;

    @ViewInject(R.id.contact_name)
    private TextView contact_name;

    @ViewInject(R.id.moblie)
    private TextView moblie;

    @ViewInject(R.id.templateName)
    private TextView template_Name;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    private String num;
    private boolean isdestroy = false;

    private int prePosition;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi", result);
                    parseJSON(result);
                    break;
                case 1:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    String result2 = (String) msg.obj;
                    LogUtil.mlog("viomi2", result2);
                    if (!isdestroy) {
                        parseJSON2(result2);
                    }
                    break;

            }
        }
    };
    private String store_id;


    @Override
    protected void init() {
        Intent intent = getIntent();
        store_id = intent.getStringExtra("id");
        num = intent.getStringExtra("num");

        loadInfo();

    }

    private void loadInfo() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.CHANNELDETAIL + store_id + ".json");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);

        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.CHANNELIMG);
        requestParams2.addBodyParameter("channelId", store_id);
        RequstUtils.getRquest(requestParams2, mhandler, 2, 3);
    }

    private void parseJSON(String result) {
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");

                String parentName = JsonUitls.getString(resultjson, "parentName");
                String channelname = JsonUitls.getString(resultjson, "name");

                String contactName = JsonUitls.getString(resultjson, "contactName");
                String contactMobile = JsonUitls.getString(resultjson, "contactMobile");
                String templateName = JsonUitls.getString(resultjson, "templateName");

                String status = JsonUitls.getString(resultjson, "status");
                String approveStatus = JsonUitls.getString(resultjson, "approveStatus");
                String statusDesc = JsonUitls.getString(resultjson, "statusDesc");
                String approveStatusDesc = JsonUitls.getString(resultjson, "approveStatusDesc");

                parentname.setText(parentName);
                channel_name.setText(channelname);

                if (!("1".equals(approveStatus) && "1".equals(status))) {
                    statustv.setTextColor(getResources().getColor(R.color.noaudit));
                } else {
                    statustv.setTextColor(getResources().getColor(R.color.detailaudit));
                }

                if ("1".equals(approveStatus)) {
                    statustv.setText(statusDesc);
                } else {
                    statustv.setText(approveStatusDesc);
                }

                staff_num.setText(num + "人");
                contact_name.setText(contactName);
                moblie.setText(contactMobile);
                template_Name.setText(templateName);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void parseJSON2(String result2) {
        try {
            JSONObject json = new JSONObject(result2);
            String code = JsonUitls.getString(json, "code");
            if ("100".equals(code)) {

                int screenWidth = getWindowManager().getDefaultDisplay().getWidth();

                DisplayMetrics dm = new DisplayMetrics();
                dm = getResources().getDisplayMetrics();
                float density = 4;

                int imgHight = (int) (180 * density);

                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");
                String baseImg = JsonUitls.getString(resultjson, "baseImg");
                String titleImg = JsonUitls.getString(resultjson, "titleImg");
                String unionImg = JsonUitls.getString(resultjson, "unionImg");

                List<ImageView> imglist = new ArrayList<>();

                if (!"-".equals(baseImg)) {
                    ImageView iv1 = new ImageView(this);
                    iv1.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv1, baseImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv1);

                    View view = new View(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    view.setLayoutParams(params);
                    view.setBackgroundResource(R.drawable.dot_backg);
                    view.setEnabled(false);
                    dotLayout.addView(view);
                }

                if (!"-".equals(titleImg)) {
                    ImageView iv2 = new ImageView(this);
                    iv2.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv2, titleImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv2);

                    View view = new View(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    view.setLayoutParams(params);
                    view.setBackgroundResource(R.drawable.dot_backg);
                    view.setEnabled(false);
                    dotLayout.addView(view);
                }

                if (!"-".equals(unionImg)) {
                    ImageView iv3 = new ImageView(this);
                    iv3.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    x.image().bind(iv3, unionImg + "&thumb=1&w=" + screenWidth + "&h=" + imgHight);
                    imglist.add(iv3);

                    View view = new View(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    params.leftMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getResources().getDisplayMetrics());
                    view.setLayoutParams(params);
                    view.setBackgroundResource(R.drawable.dot_backg);
                    view.setEnabled(false);
                    dotLayout.addView(view);
                }

                CommonVPAdapter adapter = new CommonVPAdapter(imglist);
                agency_vp.setAdapter(adapter);

                if (dotLayout.getChildCount()>0) {
                    dotLayout.getChildAt(0).setEnabled(true);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        agency_vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //设置前一个View为白色
                dotLayout.getChildAt(prePosition).setEnabled(false);
                //设置当前View为红色
                dotLayout.getChildAt(position ).setEnabled(true);
                //将当前位置赋给prePosition
                prePosition = position ;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadInfo();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isdestroy = true;
    }
}