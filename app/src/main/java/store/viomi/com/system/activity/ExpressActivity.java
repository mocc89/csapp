package store.viomi.com.system.activity;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.adapter.DataSpinnerAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

/**
 * 物流详情
 * Created by hailang on 2018/3/23 0023.
 */

public class ExpressActivity extends BaseActivity {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.head_title)
    RelativeLayout headTitle;
    @BindView(R.id.tv1)
    TextView tv1;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.order_source)
    RelativeLayout orderSource;
    @BindView(R.id.tv2)
    TextView tv2;
    @BindView(R.id.tv_product_name)
    TextView tvProductName;
    @BindView(R.id.tv3)
    TextView tv3;
    @BindView(R.id.tv_express_name)
    TextView tvExpressName;
    @BindView(R.id.tv4)
    TextView tv4;
    @BindView(R.id.tv_express_no)
    TextView tvExpressNo;
    @BindView(R.id.tv5)
    TextView tv5;
    @BindView(R.id.tv_express_phone)
    TextView tvExpressPhone;
    @BindView(R.id.reconnect_btn)
    TextView reconnectBtn;
    @BindView(R.id.reconnect_layout)
    RelativeLayout reconnectLayout;
    @BindView(R.id.progressBar1)
    ProgressBar progressBar1;
    @BindView(R.id.loading)
    RelativeLayout loading;
    @BindView(R.id.listview_wuliu)
    ListView listviewWuliu;
    long orderId;
    OrderListBean bean;
    Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);

                    List<String> listP = new ArrayList<>();
                    for (OrderListBean bean : datalist) {
                        listP.add(bean.getOrderNO());
                    }
                    DataSpinnerAdapter proviceAdapter = new DataSpinnerAdapter(activity, listP);
                    spinner.setAdapter(proviceAdapter);
                    break;
                case 1:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    loadingfinish();
                    reconnectLayout.setVisibility(View.GONE);
                    String result2 = (String) msg.obj;
                    LogUtil.mlog("oook", result2);
                    parseJSONWuliu(result2);
                    WuliuAdapter adapter = new WuliuAdapter();
                    listviewWuliu.setAdapter(adapter);
                    break;
                case 3:
                    loadingfinish();
//                    reconnectLayout.setVisibility(View.VISIBLE);
                    break;
            }
            return false;
        }
    });
    @BindView(R.id.layout_products)
    RelativeLayout layoutProducts;
    private List<OrderListBean> datalist = new ArrayList<>();

    private void parseJSON(String result) {

        try {
            JSONObject mobBaseRes = new JSONObject(result);
            if (mobBaseRes.has("mobBaseRes"))
                mobBaseRes = mobBaseRes.getJSONObject("mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess2(code, desc)) {

                JSONArray list = JsonUitls.getJSONArray(mobBaseRes, "result");


                for (int i = 0; i < list.length(); i++) {
                    JSONObject data = list.getJSONObject(i);
                    String orderNO = JsonUitls.getString(data, "orderCode");
                    String payMode = JsonUitls.getString(data, "payTypeDesc");
                    String payStatus = JsonUitls.getString(data, "dealPhaseDesc");
                    String payPrice = JsonUitls.getString(data, "paymentPrice");

                    String createdTime = data.getString("createdTime");

                    String linkmanName = JsonUitls.getString(data, "linkmanName");
                    String address = JsonUitls.getString(data, "address");
                    String linkmanPhone = JsonUitls.getString(data, "linkmanPhone");
                    String discount = JsonUitls.getString(data, "discount");
                    String settleStatesDesc = JsonUitls.getString(data, "settleStatesDesc");
                    double deliveryFee = JsonUitls.getDouble(data, "deliveryFee");

                    List<OrderListProductBean> productBeanList = new ArrayList<>();

                    JSONArray skuInfoList = JsonUitls.getJSONArray(data, "orderSkus");

                    for (int j = 0; j < skuInfoList.length(); j++) {
                        JSONObject info = skuInfoList.getJSONObject(j);
                        String name = JsonUitls.getString(info, "name");
                        String quantity = JsonUitls.getString(info, "quantity");
                        String imgUrl = JsonUitls.getString(info, "imgUrl");
                        String paymentPrice = JsonUitls.getString(info, "paymentPrice");
                        OrderListProductBean productBean = new OrderListProductBean(name, quantity, imgUrl, paymentPrice);
                        productBean.skuName = JsonUitls.getString(info, "skuName");
                        productBeanList.add(productBean);
                    }

//                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList);
                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList, linkmanName, address, linkmanPhone, discount, settleStatesDesc, deliveryFee);

                    bean.channelName = JsonUitls.getString(data, "channelName");
                    bean.orderStatusStr = JsonUitls.getString(data, "orderStatusStr");
                    bean.totalPrice = JsonUitls.getString(data, "totalPrice");
                    bean.expressType = JsonUitls.getString(data, "expressType");
                    bean.expressCorpId = JsonUitls.getString(data, "expressCorpId");
                    bean.expressNumber = JsonUitls.getString(data, "expressNumber");

                    datalist.add(bean);
                }

            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseJSONWuliu(String result) {
        Wuliu wuliu = new Wuliu();
        wuliu.time = "";
        wuliu.remark = bean.fullDivisionName + " " + bean.getAddress();
        listWuliu.add(wuliu);
        try {
            JSONObject mobBaseRes = new JSONObject(result);
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess2(code, desc)) {
                JSONArray list = JsonUitls.getJSONArray(mobBaseRes, "result").getJSONObject(0).getJSONArray("routes");
                for (int i = 0; i < list.length(); i++) {
                    Wuliu wuliu1 = new Wuliu();
                    JSONObject data = list.getJSONObject(i);
                    wuliu1.time = JsonUitls.getString(data, "time");
                    wuliu1.remark = JsonUitls.getString(data, "remark");
                    listWuliu.add(wuliu1);
                }
            } else {

            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void init() {
        setContentView(R.layout.activity_express);
        ButterKnife.bind(this);
        bean = (OrderListBean) getIntent().getSerializableExtra("order");
        orderId = bean.orderId;
        if (bean.orderStatus == 5) {//已拆单
            loadInfo();
        } else {
            layoutProducts.setVisibility(View.GONE);
            orderSource.setVisibility(View.GONE);
            setView(bean);
        }
    }

    private void loadInfo() {
        String url = MURL.QUERYSPLITINFO;
        url += orderId;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(url);
        loading();
        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void loadTracking(OrderListBean bean) {
        String url = MURL.TRACKING;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(url);
        requestParams.addBodyParameter("expressMailNo", bean.expressNumber);
        requestParams.addBodyParameter("expressCorpId", bean.expressCorpId);
        loading();
        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 2, 3);
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setView(datalist.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setView(OrderListBean bean) {
//        bean.expressType = "顺丰";
//        bean.expressCorpId = "29";
//        bean.expressNumber = "80064661655";


        List<OrderListProductBean> productBeans = bean.getProductBeanList();
        String text = "";
        if (productBeans != null)
            for (int i = 0; i < productBeans.size(); i++) {
                OrderListProductBean bean1 = productBeans.get(i);
                text += bean1.skuName + "*" + bean1.getQuantity();
                if (i != productBeans.size() - 1)
                    text += "\n";
            }
        tvProductName.setText(text);
        tvExpressName.setText(bean.expressType);
        tvExpressNo.setText(bean.expressNumber);
        loadTracking(bean);
    }

    class Wuliu {
        public String time;
        public String remark;
    }

    List<Wuliu> listWuliu = new ArrayList<>();

    class WuliuAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listWuliu.size();
        }

        @Override
        public Object getItem(int i) {
            return listWuliu.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder holder;
            if (view == null) {
                view = View.inflate(activity, R.layout.layout_wuliu_item, null);
                holder = new ViewHolder(view);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.vStep.setVisibility(View.GONE);
            holder.tvTime.setTextColor(Color.parseColor("#FF999999"));
            holder.tvRemark.setTextColor(Color.parseColor("#FF999999"));
            if (i == 0) {
                holder.vStep.setVisibility(View.GONE);
                holder.ivStep.setImageResource(R.drawable.ic_addr);
            } else if (i == 1) {
                holder.vStep.setVisibility(View.VISIBLE);
                holder.tvTime.setTextColor(Color.parseColor("#FF53A8E2"));
                holder.tvRemark.setTextColor(Color.parseColor("#FF53A8E2"));
                holder.ivStep.setImageResource(R.drawable.iv_wuliu_top);
            } else if (i == listWuliu.size() - 1) {
                holder.ivStep.setImageResource(R.drawable.iv_wuliu_bottom);
            } else {
                holder.vStep.setVisibility(View.VISIBLE);
                holder.ivStep.setImageResource(R.drawable.iv_wuliu_step);
            }
            holder.tvTime.setText(listWuliu.get(i).time);
            holder.tvRemark.setText(listWuliu.get(i).remark);
            return view;
        }

        class ViewHolder {
            @BindView(R.id.tv_time)
            TextView tvTime;
            @BindView(R.id.iv_step)
            ImageView ivStep;
            @BindView(R.id.v_step)
            View vStep;
            @BindView(R.id.tv_remark)
            TextView tvRemark;

            ViewHolder(View view) {
                ButterKnife.bind(this, view);
            }
        }
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

}
