package store.viomi.com.system.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.ChannelPurchaseOrderResp;
import store.viomi.com.system.utils.DisplayUtil;

public class PuductAdapter extends BaseAdapter {
    private List<ChannelPurchaseOrderResp> list;
    private Context context;
    int maxHeight;
    /**
     * 最大值
     */
    private long numCount;
    /**
     * 0订单数 1提货额
     */
    private int mode;

    /**
     * @param list
     * @param context
     * @param numCount
     * @param mode     0订单数 1提货额
     */
    public PuductAdapter(List<ChannelPurchaseOrderResp> list, Context context, long numCount, int mode) {
        this.list = list;
        this.context = context;
        this.numCount = numCount;
        this.mode = mode;
        maxHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, context.getResources().getDisplayMetrics());
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.puduct_item, null);
            viewHolder.tv_dark_sale = (ImageView) convertView.findViewById(R.id.tv_dark_sale);
            viewHolder.tv_dark_unsale = (ImageView) convertView.findViewById(R.id.tv_dark_unsale);
            viewHolder.tv_sale_datetime = (TextView) convertView.findViewById(R.id.tv_sale_datetime);
            viewHolder.ll_item = (LinearLayout) convertView.findViewById(R.id.ll_item);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        float gn = DisplayUtil.getdensity(context);
        ChannelPurchaseOrderResp cp = list.get(position);
        double hei = 0;
        if (mode == 0) {
            hei = maxHeight * Long.parseLong(cp.orderNum) / numCount;
        } else {
            hei = maxHeight * Long.parseLong(cp.orderFee) / numCount;
        }
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) viewHolder.tv_dark_sale.getLayoutParams();
        layoutParams.height = (int) hei;
        viewHolder.tv_dark_sale.setLayoutParams(layoutParams);

        double hei1 = 0;
        if (mode == 0) {
            hei1 = maxHeight * Long.parseLong(cp.refundOrderNum) / numCount;
        } else {
            hei1 = maxHeight * Long.parseLong(cp.refundOrderFee) / numCount;
        }
        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) viewHolder.tv_dark_unsale.getLayoutParams();
        layoutParams2.height = (int) hei1;
        viewHolder.tv_dark_unsale.setLayoutParams(layoutParams2);

        viewHolder.tv_sale_datetime.setText(list.get(position).dataTime);
        if (cp.selected) {
            viewHolder.tv_dark_sale.setBackgroundColor(context.getResources().getColor(R.color.color_dark_blue_selected));
            viewHolder.tv_dark_unsale.setBackgroundColor(context.getResources().getColor(R.color.color_light_blue_selected));
        } else {
            viewHolder.tv_dark_sale.setBackgroundColor(context.getResources().getColor(R.color.color_dark_blue));
            viewHolder.tv_dark_unsale.setBackgroundColor(context.getResources().getColor(R.color.color_light_blue));
        }
        return convertView;
    }

    private static class ViewHolder {
        ImageView tv_dark_sale;
        ImageView tv_dark_unsale;
        TextView tv_sale_datetime;
        LinearLayout ll_item;
    }
}
