package store.viomi.com.system.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;


public class ClerkSelectActivity extends AppCompatActivity {

    private final int RESULTCODE = 1008;
    private ImageView back;
    private EditText username;
    private EditText phone;
    private EditText channel_name;
    private Spinner spinner;
    private Button select;

    private String role = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clerk_select);

        back = (ImageView) findViewById(R.id.back);
        username = (EditText) findViewById(R.id.username);
        phone = (EditText) findViewById(R.id.phone);
        channel_name = (EditText) findViewById(R.id.channel_name);
        spinner = (Spinner) findViewById(R.id.spinner);
        select = (Button) findViewById(R.id.select);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        LinearLayout lin = (LinearLayout) findViewById(R.id.lin);

        lin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        List<String> data = new ArrayList<String>();
        data.add("请选择促销员类型");
        data.add("兼职促销员");
        data.add("全职促销员");
        data.add("店长");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, data);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        role = "";
                        break;
                    case 1:
                        role = "25";
                        break;
                    case 2:
                        role = "23";
                        break;
                    case 3:
                        role = "21";
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                role = "";
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();

                intent.putExtra("username", username.getText().toString());
                intent.putExtra("phone", phone.getText().toString());
                intent.putExtra("channel_name", channel_name.getText().toString());
                intent.putExtra("role", role);

                setResult(RESULTCODE, intent);
                finish();
            }
        });
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}
