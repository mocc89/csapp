package store.viomi.com.system.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.activity.OrderDetailActivity;
import store.viomi.com.system.adapter.OrderListYufuAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;
import store.viomi.com.system.bean.OrderSearchParameter;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

/**
 * Created by Mocc on 2017/8/24
 */

@ContentView(R.layout.fragment_order_list)
public class OrderListYufuFragment extends BaseFragment {

    @ViewInject(R.id.orderlist)
    private ListView orderlist;

    @ViewInject(R.id.order_count)
    private TextView order_count;
    @ViewInject(R.id.tv_time)
    private TextView tv_time;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;
    //提示无数据
    @ViewInject(R.id.no_resouce_layout)
    private RelativeLayout no_resouce_layout;
    //提示无数据
    @ViewInject(R.id.tv_no_resource2)
    private TextView tv_no_resource2;


    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;       // 当前窗口可见项总数

    private int currentPage = 1;
    private int pageSize = 50;

    private long beginTime;
    private long endTime;

    private boolean isFirsrPage = true;

    private List<OrderListBean> datalist;

    private int totalCount;
    private int totalPageNum;
    private OrderListYufuAdapter adapter;
    OrderSearchParameter param;

    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (finish)
                return false;
            switch (msg.what) {
                case 7:
                    currentPage++;
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result2 = (String) msg.obj;
                    parseJSON2(result2);
                    break;
                case 9:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
            }
            return false;
        }
    });

    public static OrderListYufuFragment getInstance() {
        OrderListYufuFragment of = new OrderListYufuFragment();
        Bundle bundle = new Bundle();
        of.setArguments(bundle);
        return of;
    }

    public static OrderListYufuFragment getInstance(String orderType, String startTime, String endTime) {
        OrderListYufuFragment of = new OrderListYufuFragment();
        Bundle bundle = new Bundle();
        bundle.putString("startTime", startTime);
        bundle.putString("endTime", endTime);
        of.setArguments(bundle);
        return of;
    }


    private void loadFirstListType3() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.PREPAYMENT_ORDER);

        requestParams.addBodyParameter("pageNum", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf1.format(new Date(beginTime * 1000L));
        String format1 = sdf1.format(new Date(endTime * 1000L));

        requestParams.addParameter("beginTime", format + "");
        requestParams.addParameter("endTime", format1 + "");


        requestParams.addParameter("channelId", "");//渠道ID
        requestParams.addParameter("channelName", param.sellerName);//渠道名称  商家名称
        requestParams.addParameter("receiver", "");//收货人姓名
        requestParams.addParameter("receiverMobile", "");//收货人手机
        requestParams.addParameter("productName", param.productName);//商品名称
        requestParams.addParameter("skuIds", "");//商品sku
        requestParams.addParameter("orderType", "");//订单类型:1 - 预付款, 2:小米提货, 3:物料提货
        if (param.orderStatus > 0)
            requestParams.addParameter("orderStatus", param.orderStatus);//订单状态, 多个订单状态使用 "," 拼接传递 QUERY
        else
            requestParams.addParameter("orderStatus", "");//订单状态, 多个订单状态使用 "," 拼接传递 QUERY
        requestParams.addParameter("invoiceFlag", "");//开票状态
        requestParams.addParameter("platformType", "");//平台类型:1 - 天猫;2 - 京东;3 - 苏宁;4 - 其他
        requestParams.addParameter("platformOrderNum", "");//平台订单编号
        requestParams.addParameter("orderCode", param.orderNo);//订单编码
        requestParams.addParameter("sentTimeBegin", "");//发货开始时间
        requestParams.addParameter("sentTimeEnd", "");//发货结束时间
        requestParams.addParameter("expressNo", "");//快递单号
        if (param.district != null)
            requestParams.addParameter("division", param.district.code);//收货地址编码
        else
            requestParams.addParameter("division", "");//收货地址编码
        requestParams.addParameter("limitOriginOrder", 1);//*限制原始订单: 0-不限制，1-限制 app端 这个参数要传1，限制查询原始订单
        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 7, 9);

    }


    private void parseJSON2(String result) {

        try {
            JSONObject mobBaseRes = new JSONObject(result);
            if (mobBaseRes.has("mobBaseRes")) {
                mobBaseRes = mobBaseRes.getJSONObject("mobBaseRes");
            }
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess2(code, desc)) {

                JSONObject resultJSON = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray list = JsonUitls.getJSONArray(resultJSON, "list");

                if (list.length() == 0 && currentPage == 2) {
                    no_resouce_layout.setVisibility(View.VISIBLE);
                } else {
                    no_resouce_layout.setVisibility(View.GONE);
                }

                for (int i = 0; i < list.length(); i++) {
                    JSONObject data = list.getJSONObject(i);
                    String orderNO = JsonUitls.getString(data, "orderCode");
                    String payMode = JsonUitls.getString(data, "payTypeDesc");
                    String payStatus = JsonUitls.getString(data, "dealPhaseDesc");
                    String payPrice = JsonUitls.getString(data, "paymentPrice");

                    String createdTime = data.getString("createdTime");

                    String linkmanName = JsonUitls.getString(data, "linkmanName");
                    String address = JsonUitls.getString(data, "address");
                    String linkmanPhone = JsonUitls.getString(data, "linkmanPhone");
                    String discount = JsonUitls.getString(data, "discount");
                    String settleStatesDesc = JsonUitls.getString(data, "settleStatesDesc");
                    double deliveryFee = JsonUitls.getDouble(data, "deliveryFee");

                    List<OrderListProductBean> productBeanList = new ArrayList<>();

                    JSONArray skuInfoList = JsonUitls.getJSONArray(data, "orderSkus");

                    for (int j = 0; j < skuInfoList.length(); j++) {
                        JSONObject info = skuInfoList.getJSONObject(j);
                        String name = JsonUitls.getString(info, "name");
                        String quantity = JsonUitls.getString(info, "quantity");
                        String imgUrl = JsonUitls.getString(info, "imgUrl");
                        String paymentPrice = JsonUitls.getString(info, "paymentPrice");
                        OrderListProductBean productBean = new OrderListProductBean(name, quantity, imgUrl, paymentPrice);
                        productBean.skuName = JsonUitls.getString(info, "skuName");
                        productBean.price = JsonUitls.getString(info, "price");
                        productBeanList.add(productBean);
                    }

//                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList);
                    OrderListBean bean = new OrderListBean(orderNO, payMode, payStatus, payPrice, createdTime, productBeanList, linkmanName, address, linkmanPhone, discount, settleStatesDesc, deliveryFee);

                    bean.channelName = JsonUitls.getString(data, "channelName");
                    bean.orderStatusStr = JsonUitls.getString(data, "orderStatusStr");
                    bean.totalPrice = JsonUitls.getString(data, "totalPrice");
                    datalist.add(bean);
                }
                if (isFirsrPage) {
                    totalCount = resultJSON.getInt("totalCount");
                    totalPageNum = resultJSON.getInt("totalPageNum");
                }

                if (isFirsrPage) {
                    setView();
                } else {
                    adapter.notifyDataSetChanged();
                    orderlist.setSelection(visibleLastIndex - visibleItemCount + 2);
                }
            } else {
                no_resouce_layout.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setView() {
        if (getActivity() == null)
            return;
        isFirsrPage = false;
        order_count.setText("" + totalCount);
        adapter = new OrderListYufuAdapter(getActivity(), datalist);
        orderlist.setAdapter(adapter);
    }

    private void loadmore() {
        loadFirstListType3();
    }

    SimpleDateFormat sdf1;

    @Override
    protected void init() {
        if (param == null) {
            param = new OrderSearchParameter();
            endTime = System.currentTimeMillis() / 1000L;
            beginTime = endTime - 60L * 60L * 24L * 30L;
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                beginTime = sdf1.parse(sdf1.format(new Date(beginTime * 1000L))).getTime() / 1000L;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            param.end = endTime;
            param.start = beginTime;
        }
        sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        datalist = new ArrayList<>();
        tv_time.setText(sdf1.format(new Date(beginTime * 1000L)) + "至" + sdf1.format(new Date(endTime * 1000L)));
        loadFirstListType3();
    }


    public void onNewFilter(OrderSearchParameter param) {
        this.param = param;
        currentPage = 1;
        isFirsrPage = true;
        beginTime = this.param.start;
        endTime = this.param.end;
        if (orderlist == null)
            return;
        tv_time.setText(sdf1.format(new Date(beginTime * 1000L)) + "至" + sdf1.format(new Date(endTime * 1000L)));
        datalist.clear();
        loadFirstListType3();
    }

    @Override
    protected void initListener() {

        orderlist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                OrderListYufuFragment.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;

            }
        });

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), OrderDetailActivity.class);
                OrderListBean bean = datalist.get((int) id);
                intent.putExtra("ordercode", bean.getOrderNO());
                startActivity(intent);
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstListType3();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }

    boolean finish;

    @Override
    public void onDestroy() {
        finish = true;
        mhandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
