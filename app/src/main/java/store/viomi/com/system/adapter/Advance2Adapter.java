package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.bean.ChannelPurchaseOrderDetailResp;
import store.viomi.com.system.bean.ChannelPurchaseOrderResp;

/**
 * Created by viomi on 2017/1/3.
 */

public class Advance2Adapter extends BaseAdapter {

    private List<ChannelPurchaseOrderDetailResp> list;
    private Context context;
    private LayoutInflater inflater;

    public Advance2Adapter(List<ChannelPurchaseOrderDetailResp> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder1 = null;
        ChannelPurchaseOrderDetailResp resp = list.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_advance_order_2_item, null);
            holder1 = new ViewHolder(convertView);
            convertView.setTag(holder1);
        } else holder1 = (ViewHolder) convertView.getTag();

        holder1.tvShopName.setText(resp.channelName);
        holder1.tvAgentName.setText(resp.secondChannel);
        holder1.tvCityName.setText(resp.rootChannel);
        holder1.tvGoodNum.setText(resp.productNum + "");
        holder1.tvOrderNum.setText(resp.orderNum + "");
        holder1.tvOrderFee.setText(resp.orderFee + "");
        return convertView;
    }


    static class ViewHolder {
        @BindView(R.id.tv_shop_name)
        TextView tvShopName;
        @BindView(R.id.tv_agent_name)
        TextView tvAgentName;
        @BindView(R.id.tv_city_name)
        TextView tvCityName;
        @BindView(R.id.tv_good_num)
        TextView tvGoodNum;
        @BindView(R.id.tv_order_num)
        TextView tvOrderNum;
        @BindView(R.id.tv_order_fee)
        TextView tvOrderFee;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
