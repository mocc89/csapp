package store.viomi.com.system.bean;

/**
 * Created by hailang on 2018/3/19 0019.
 */

public class ChannelPurchaseOrderDetailResp {
    public long channelId;
    public long orderFee;
    public long orderNum;
    public long productNum;
    public String channelName;
    public String secondChannel;
    public String rootChannel;
}
