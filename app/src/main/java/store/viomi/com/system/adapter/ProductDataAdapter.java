package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.ProductFormBean;
import store.viomi.com.system.utils.DisplayUtil;

/**
 * Created by Mocc on 2017/9/30
 */

public class ProductDataAdapter extends BaseAdapter {

    private List<List<ProductFormBean>> list;
    private Context context;
    private LayoutInflater inflater;
    private int screenWidrh;

    public ProductDataAdapter(List<List<ProductFormBean>> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
        screenWidrh = DisplayUtil.getScreenWidth(context);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.product_data_item_layout, null);
        LinearLayout lin = (LinearLayout) convertView.findViewById(R.id.item_lin);

        if (position % 2 == 0) {
            lin.setBackgroundResource(R.color.item_white);
        } else {
            lin.setBackgroundResource(R.color.item_dark);
        }

        for (int i = 0; i < list.get(position).size(); i++) {

            if (position == 0) {
                TextView t = (TextView) inflater.inflate(R.layout.product_data_item_header, null);
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(screenWidrh / 3, ViewGroup.LayoutParams.MATCH_PARENT);
                t.setLayoutParams(layoutParams);
                t.setText(list.get(position).get(i).getName());
                lin.addView(t);
            } else {
                if (i == 0) {
                    TextView t = (TextView) inflater.inflate(R.layout.product_data_item_body, null);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(screenWidrh / 3, ViewGroup.LayoutParams.MATCH_PARENT);
                    t.setLayoutParams(layoutParams);
                    t.setText(list.get(position).get(i).getName());
                    lin.addView(t);
                } else {
                    TextView t = (TextView) inflater.inflate(R.layout.product_data_item_body2, null);
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(screenWidrh / 3, ViewGroup.LayoutParams.MATCH_PARENT);
                    t.setLayoutParams(layoutParams);
                    if (i == 2) {
                        t.setText("￥" + list.get(position).get(i).getName());
                    } else {
                        t.setText(list.get(position).get(i).getName());
                    }
                    lin.addView(t);
                }
            }
        }
        return convertView;
    }
}
