package store.viomi.com.system.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.MainActivity;
import store.viomi.com.system.MainBActivity;
import store.viomi.com.system.MainCActivity;
import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.Config;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.MD5Util;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.SharePreferencesUtils;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_login)
public class LoginActivity extends BaseActivity {

    @ViewInject(R.id.sc)
    private ScrollView sc;

    //账号
    @ViewInject(R.id.account)
    private EditText account;
    //密码
    @ViewInject(R.id.password)
    private EditText password;

    //忘记密码
    @ViewInject(R.id.forget_password)
    private TextView forget_password;

    //登录按钮
    @ViewInject(R.id.login_in)
    private Button login_in;

    //加载中
    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;

    private boolean cancancel = false;
    private Callback.Cancelable cancelable;
    private SharedPreferences sp;
    private String md5String;
    private boolean isdefaultlogin = false;


    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    judgeLogin(result);
                    break;
                }

                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }

            }
        }
    };


    @Override
    protected void init() {
        // if(!isTaskRoot())，判断该Activity是不是任务空间的源Activity
        // “非”也就是说是被系统重新实例化出来，如果你就放在launcher
        // Activity中话，这里可以直接return了。
        if (!isTaskRoot()) {
            finish();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            //底下屏幕透明导航栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        }


        sp = this.getSharedPreferences("mysp", MODE_PRIVATE);
        String last_account = sp.getString("account", "");
        String last_md5 = sp.getString("md5String", "");

        this.account.setText(last_account);

        if (last_md5.length() > 0) {
            isdefaultlogin = true;
            loginMethod(last_account, last_md5);
        }

    }


    //手动进行登陆的方法
    private void loginMethod() {
        String account_num = account.getText().toString();
        String password_num = password.getText().toString();

        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.LOGINURL);
        requestParams.addBodyParameter("account", account_num);
        md5String = MD5Util.getMD5String(password_num);
        requestParams.addBodyParameter("pwdMD5", md5String);
        requestParams.addBodyParameter("sourceSystem", "Android");
        requestParams.addBodyParameter("version", Config.getCurrentversion(activity));
        requestParams.addBodyParameter("deviceId", getDeviceid());
        requestParams.addBodyParameter("systemVersion", getsystemVersion());

        //提示请输入用户名
        if (account_num.length() == 0) {
            ToastUtil.show(HintText.EMPTYNAME);
            return;
        }

        //提示请输入密码
        if (password_num.length() == 0) {
            ToastUtil.show(HintText.EMPTYPASSWORD);
            return;
        }
        isdefaultlogin = false;
        loading();

        //发起get请求
        cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private String getDeviceid() {
        String m_szDevIDShort = "";

        try {
            m_szDevIDShort = "35" +
                    Build.BOARD.length() % 10 +
                    Build.BRAND.length() % 10 +
                    Build.CPU_ABI.length() % 10 +
                    Build.DEVICE.length() % 10 +
                    Build.DISPLAY.length() % 10 +
                    Build.HOST.length() % 10 +
                    Build.ID.length() % 10 +
                    Build.MANUFACTURER.length() % 10 +
                    Build.MODEL.length() % 10 +
                    Build.PRODUCT.length() % 10 +
                    Build.TAGS.length() % 10 +
                    Build.TYPE.length() % 10 +
                    Build.USER.length() % 10;
        } catch (Exception e) {
        }


        return m_szDevIDShort;
    }

    private String getsystemVersion() {
        String os = android.os.Build.VERSION.RELEASE + android.os.Build.MODEL;
        return os;
    }


    //自动登录
    private void loginMethod(String account, String md5) {
        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.LOGINURL);
        requestParams.addBodyParameter("account", account);
        requestParams.addBodyParameter("pwdMD5", md5);
        requestParams.addBodyParameter("sourceSystem", "Android");
        requestParams.addBodyParameter("version", Config.getCurrentversion(activity));
        requestParams.addBodyParameter("deviceId", getDeviceid());
        requestParams.addBodyParameter("systemVersion", getsystemVersion());

        loading();

        //发起get请求
        cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void judgeLogin(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (isdefaultlogin && "902".equals(code)) {
                View view = LayoutInflater.from(this).inflate(R.layout.password_modified_dialog, null);
                final Dialog dialog = new Dialog(this, R.style.selectorDialog);
                dialog.setContentView(view);
                TextView ok = (TextView) view.findViewById(R.id.ok);
                dialog.setCancelable(false);
                dialog.show();
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                return;
            }

            if (ResponseCode.isSuccess(code, desc)) {

                String token = JsonUitls.getString(mobBaseRes, "token");

                JSONObject user = JsonUitls.getJSONObject(mobBaseRes, "user");
                String user_name = JsonUitls.getString(user, "name");

                JSONArray roles = mobBaseRes.getJSONArray("roles");

                JSONObject channel = JsonUitls.getJSONObject(mobBaseRes, "channel");
                String channel_name = JsonUitls.getString(channel, "name");

                SharePreferencesUtils.getInstance().setToken(this,token);
                SharePreferencesUtils.getInstance().setUser_name(this,user_name);
                SharePreferencesUtils.getInstance().setChannel_name(this,channel_name);
                if (!isdefaultlogin) {
                    saveUserInfo();
                }

                String roles_str = roles.toString();

                if (roles_str != null) {

                    if (roles_str.contains("SUPER_ADMIN")) {
                        //ToastUtil.show("super");
                        //超级管理模板
                        SharePreferencesUtils.getInstance().setRole(this,"A");
                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                        this.finish();
                    } else {
                        if (roles_str.contains("DIVISION_ADMIN")) {
                            //ToastUtil.show("区域管理员模板开发中！");
                            //区域管理模板
                            SharePreferencesUtils.getInstance().setRole(this,"B");
                            Intent intent = new Intent(this, MainBActivity.class);
                            startActivity(intent);
                            this.finish();

                        } else {
                            if (roles_str.contains("CITY_AGENT")) {
                                //ToastUtil.show("city");
                                //城市运营模板
                                SharePreferencesUtils.getInstance().setRole(this,"C");
                                Intent intent = new Intent(this, MainCActivity.class);
                                startActivity(intent);
                                this.finish();
                            } else {
//                                SharePreferencesUtils.getInstance().setRole(this,"A");
//                                Intent intent = new Intent(this, MainActivity.class);
//                                startActivity(intent);
//                                this.finish();
                                ToastUtil.show(HintText.UNABLELOGIN);
                                return;
                            }
                        }
                    }

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveUserInfo() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("account", account.getText().toString());
        editor.putString("md5String", md5String);
        editor.commit();
    }


    @Override
    protected void initListener() {

        login_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginMethod();
            }
        });

        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPwActivity.class);
                startActivity(intent);
            }
        });

        sc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

    }


    //加载中
    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
        cancancel = true;
    }

    //加载完
    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
        cancancel = false;
    }


    //后退键逻辑
    @Override
    public void onBackPressed() {
        if (cancelable != null && cancancel) {
            cancelable.cancel();
            loadingfinish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
    }
}
