package store.viomi.com.system.bean;

/**
 * Created by Mocc on 2017/10/7
 */

public class ProductFormBean {

    //商品名称
    private String name;
    //
    private int sortIdx;

    public ProductFormBean() {
    }

    public ProductFormBean(String name, int sortIdx) {
        this.name = name;
        this.sortIdx = sortIdx;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSortIdx() {
        return sortIdx;
    }

    public void setSortIdx(int sortIdx) {
        this.sortIdx = sortIdx;
    }
}
