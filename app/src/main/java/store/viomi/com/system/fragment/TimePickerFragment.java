package store.viomi.com.system.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import store.viomi.com.system.R;
import store.viomi.com.system.widget.DatePickerTwo;
import store.viomi.com.timepickerlibrary.timepicker.DateUtils;
import store.viomi.com.timepickerlibrary.timepicker.WheelView;

/**
 * Created by hailang on 2018/3/23 0023.
 */

public class TimePickerFragment extends Fragment {
    Activity activity;
    @BindView(R.id.tv_time_start)
    TextView tvTimeStart;
    @BindView(R.id.tv_time_end)
    TextView tvTimeEnd;
    @BindView(R.id.layout_time)
    LinearLayout layoutTime;
    @BindView(R.id.yearView)
    WheelView yearView;
    @BindView(R.id.monthView)
    WheelView monthView;
    @BindView(R.id.dayView)
    WheelView dayView;
    @BindView(R.id.dayView_label)
    TextView dayViewLabel;
    @BindView(R.id.dayView_label_end)
    TextView dayViewLabelEnd;
    @BindView(R.id.layout_wheel_start)
    LinearLayout layoutWheelStart;
    @BindView(R.id.yearView_end)
    WheelView yearViewEnd;
    @BindView(R.id.monthView_end)
    WheelView monthViewEnd;
    @BindView(R.id.dayView_end)
    WheelView dayViewEnd;
    @BindView(R.id.layout_wheel_end)
    LinearLayout layoutWheelEnd;
    @BindView(R.id.layout_wheel)
    FrameLayout layoutWheel;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.layout_button)
    LinearLayout layoutButton;
    @BindView(R.id.activity_advance_order)
    RelativeLayout activityAdvanceOrder;
    private ArrayList<String> years = new ArrayList<String>();
    private ArrayList<String> months = new ArrayList<String>();
    private ArrayList<String> days = new ArrayList<String>();
    //已选中的开始时间
    private int selectedYearIndex = 0, selectedMonthIndex = 0, selectedDayIndex = 0;
    //已选中的结束时间
    private int selectedYearIndexEnd = 0, selectedMonthIndexEnd = 0, selectedDayIndexEnd = 0;
    private int startYear = 2016, startMonth = 1, startDay = 1;
    private int endYear = 2020, endMonth = 12, endDay = 31;
    private long startTime, endtime;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();
        View view = View.inflate(getContext(), R.layout.date_picker_two, null);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    protected void init() {
        layoutButton.setVisibility(View.GONE);
        Calendar calendar = Calendar.getInstance(Locale.CHINA);
        calendar.setTime(new Date(System.currentTimeMillis()));
        int year_end = calendar.get(Calendar.YEAR);
        int month_end = calendar.get(Calendar.MONTH);
        int day_end = calendar.get(Calendar.DAY_OF_MONTH);
        endYear = year_end;
        setSelectedItemEnd(year_end, month_end + 1, day_end);

        long time = calendar.getTimeInMillis();
        long day30 = 60L * 60L * 24L * 1000L * 30L;
        time = time - day30;
        calendar.setTime(new Date(time));
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        setSelectedItem(year, month + 1, day);

        yearView.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        yearView.setLineVisible(true);
        yearView.setLineColor(Color.parseColor("#53A8E2"));
        yearViewEnd.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        yearViewEnd.setLineVisible(true);
        yearViewEnd.setLineColor(Color.parseColor("#53A8E2"));

        monthView.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        monthView.setLineVisible(true);
        monthView.setLineColor(Color.parseColor("#53A8E2"));
        monthViewEnd.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        monthViewEnd.setLineVisible(true);
        monthViewEnd.setLineColor(Color.parseColor("#53A8E2"));

        dayView.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        dayView.setLineVisible(true);
        dayView.setLineColor(Color.parseColor("#53A8E2"));
        dayViewEnd.setTextColor(activity.getResources().getColor(R.color.black), Color.parseColor("#53A8E2"));
        dayViewEnd.setLineVisible(true);
        dayViewEnd.setLineColor(Color.parseColor("#53A8E2"));

        if (selectedYearIndex == 0) {
            yearView.setItems(years);
        } else {
            yearView.setItems(years, selectedYearIndex);
        }
        yearView.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedYearIndex = selectedIndex;
                //需要根据年份及月份动态计算天数
                int year = DateUtils.trimZero(item);
                changeDayData(year, changeMonthData(year));
                monthView.setItems(months, selectedMonthIndex);
                dayView.setItems(days, selectedDayIndex);
                setTime();
            }
        });
        yearViewEnd.setItems(years, selectedYearIndexEnd);
        yearViewEnd.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedYearIndexEnd = selectedIndex;
                //需要根据年份及月份动态计算天数
                int year = DateUtils.trimZero(item);
                changeDayData(year, changeMonthData(year));
                monthViewEnd.setItems(months, selectedMonthIndexEnd);
                dayViewEnd.setItems(days, selectedDayIndexEnd);
                setTime();
            }
        });
        if (selectedMonthIndex == 0) {
            monthView.setItems(months);
        } else {
            monthView.setItems(months, selectedMonthIndex);
        }
        monthView.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedMonthIndex = selectedIndex;
                changeDayData(DateUtils.trimZero(years.get(selectedYearIndex)), DateUtils.trimZero(item));
                dayView.setItems(days, selectedDayIndex);
                setTime();
            }
        });
        monthViewEnd.setItems(months, selectedMonthIndexEnd);
        monthViewEnd.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedMonthIndexEnd = selectedIndex;
                changeDayData(DateUtils.trimZero(years.get(selectedYearIndexEnd)), DateUtils.trimZero(item));
                dayViewEnd.setItems(days, selectedDayIndexEnd);
                setTime();
            }
        });
        dayView.setItems(days, selectedDayIndex);
        dayView.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedDayIndex = selectedIndex;
                setTime();
            }
        });
        dayViewEnd.setItems(days, selectedDayIndexEnd);
        dayViewEnd.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
            @Override
            public void onSelected(boolean isUserScroll, int selectedIndex, String item) {
                selectedDayIndexEnd = selectedIndex;
                setTime();
            }
        });
        if (modeNianyue == true) {
            dayViewEnd.setVisibility(View.GONE);
            dayView.setVisibility(View.GONE);
            dayViewLabel.setVisibility(View.GONE);
            dayViewLabelEnd.setVisibility(View.GONE);
        }
        initListener();
    }

    boolean modeNianyue;

    public void setModeNianYue() {
        modeNianyue = true;
        if (dayViewEnd == null)
            return;
        dayViewEnd.setVisibility(View.GONE);
        dayView.setVisibility(View.GONE);
        dayViewLabel.setVisibility(View.GONE);
        dayViewLabelEnd.setVisibility(View.GONE);
        setTime();
    }

    /**
     * 开始时间 秒
     *
     * @return
     */
    public long getStartTime() {
        Calendar calendar = Calendar.getInstance(Locale.CHINA);
        calendar.set(Calendar.YEAR, Integer.parseInt(getSelectedYear()));
        calendar.set(Calendar.MONTH, Integer.parseInt(getSelectedMonth()) - 1);
        if (modeNianyue) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(getSelectedDay()));
        }
        calendar.set(Calendar.HOUR_OF_DAY, 00);
        calendar.set(Calendar.MINUTE, 00);
        calendar.set(Calendar.SECOND, 00);
        calendar.set(Calendar.MILLISECOND, 000);
        return calendar.getTimeInMillis() / 1000;
    }

    /**
     * 结束时间 秒
     *
     * @return
     */
    public long getEndTime() {
        Calendar calendar = Calendar.getInstance(Locale.CHINA);
        calendar.set(Calendar.YEAR, Integer.parseInt(getSelectedYearEnd()));
        calendar.set(Calendar.MONTH, Integer.parseInt(getSelectedMonthEnd()) - 1);
        if (modeNianyue) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));      // 获取前月的最后一天
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(getSelectedDayEnd()));
        }
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis() / 1000;
    }

    void setTime() {
        if (modeNianyue) {
            tvTimeStart.setText(getSelectedYear() + "年" + getSelectedMonth() + "月");
            tvTimeEnd.setText(getSelectedYearEnd() + "年" + getSelectedMonthEnd() + "月");
        } else {
            tvTimeStart.setText(getSelectedYear() + "年" + getSelectedMonth() + "月" + getSelectedDay() + "日");
            tvTimeEnd.setText(getSelectedYearEnd() + "年" + getSelectedMonthEnd() + "月" + getSelectedDayEnd() + "日");
        }

    }

    /**
     * 设置默认选中的年月日
     */
    public void setSelectedItem(int year, int month, int day) {
        changeYearData();
        changeMonthData(year);
        changeDayData(year, month);
        selectedYearIndex = findItemIndex(years, year);
        selectedMonthIndex = findItemIndex(months, month);
        selectedDayIndex = findItemIndex(days, day);
    }

    public void setSelectedItemEnd(int year, int month, int day) {
        changeYearData();
        changeMonthData(year);
        changeDayData(year, month);
        selectedYearIndexEnd = findItemIndex(years, year);
        selectedMonthIndexEnd = findItemIndex(months, month);
        selectedDayIndexEnd = findItemIndex(days, day);
    }

    private int findItemIndex(ArrayList<String> items, int item) {
        //折半查找有序元素的索引，效率应该高于items.indexOf(...)
        int index = Collections.binarySearch(items, item, new Comparator<Object>() {
            @Override
            public int compare(Object lhs, Object rhs) {
                String lhsStr = lhs.toString();
                String rhsStr = rhs.toString();
                lhsStr = lhsStr.startsWith("0") ? lhsStr.substring(1) : lhsStr;
                rhsStr = rhsStr.startsWith("0") ? rhsStr.substring(1) : rhsStr;
                return Integer.parseInt(lhsStr) - Integer.parseInt(rhsStr);
            }
        });
        if (index < 0) {
            index = 0;
        }
        return index;
    }

    public String getSelectedYear() {
        return years.get(selectedYearIndex);
    }

    public String getSelectedMonth() {
        return months.get(selectedMonthIndex);
    }

    public String getSelectedDay() {
        if (selectedDayIndex >= days.size()) {
            selectedDayIndex = days.size() - 1;
        }
        return days.get(selectedDayIndex);
    }

    public String getSelectedYearEnd() {
        return years.get(selectedYearIndexEnd);
    }

    public String getSelectedMonthEnd() {
        return months.get(selectedMonthIndexEnd);
    }

    public String getSelectedDayEnd() {
        if (selectedDayIndexEnd >= days.size()) {
            selectedDayIndexEnd = days.size() - 1;
        }
        return days.get(selectedDayIndexEnd);
    }

    private void changeYearData() {
        years.clear();
        if (startYear == endYear) {
            years.add(String.valueOf(startYear));
        } else if (startYear < endYear) {
            //年份正序
            for (int i = startYear; i <= endYear; i++) {
                years.add(String.valueOf(i));
            }
        } else {
            //年份逆序
            for (int i = startYear; i >= endYear; i--) {
                years.add(String.valueOf(i));
            }
        }
    }

    private int changeMonthData(int year) {
        String preSelectMonth = months.size() > selectedMonthIndex ? months.get(selectedMonthIndex) : null;
        months.clear();
        if (startYear == endYear) {
            for (int i = startMonth; i <= endMonth; i++) {
                months.add(DateUtils.fillZero(i));
            }
        } else if (year == startYear) {
            for (int i = startMonth; i <= 12; i++) {
                months.add(DateUtils.fillZero(i));
            }
        } else if (year == endYear) {
            for (int i = 1; i <= endMonth; i++) {
                months.add(DateUtils.fillZero(i));
            }
        } else {
            for (int i = 1; i <= 12; i++) {
                months.add(DateUtils.fillZero(i));
            }
        }
        int preSelectMonthIndex = preSelectMonth == null ? 0 : months.indexOf(preSelectMonth);
        selectedMonthIndex = preSelectMonthIndex == -1 ? 0 : preSelectMonthIndex;
        return DateUtils.trimZero(months.get(selectedMonthIndex));
    }

    private void changeDayData(int year, int month) {
        String preSelectDay = days.size() > selectedDayIndex ? days.get(selectedDayIndex) : null;
        days.clear();
        int maxDays = DateUtils.calculateDaysInMonth(year, month);
        if (year == startYear && month == startMonth) {
            for (int i = startDay; i <= maxDays; i++) {
                days.add(DateUtils.fillZero(i));
            }
            int preSelectDayIndex = preSelectDay == null ? 0 : days.indexOf(preSelectDay);
            selectedDayIndex = preSelectDayIndex == -1 ? 0 : preSelectDayIndex;
        } else if (year == endYear && month == endMonth) {
            for (int i = 1; i <= endDay; i++) {
                days.add(DateUtils.fillZero(i));
            }
            int preSelectDayIndex = preSelectDay == null ? 0 : days.indexOf(preSelectDay);
            selectedDayIndex = preSelectDayIndex == -1 ? 0 : preSelectDayIndex;
        } else {
            for (int i = 1; i <= maxDays; i++) {
                days.add(DateUtils.fillZero(i));
            }
            if (selectedDayIndex >= maxDays) {
                selectedDayIndex = days.size() - 1;
            }
        }
    }

    protected void initListener() {
        tvTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTimeFilter(false);
                layoutWheelStart.setVisibility(View.VISIBLE);
                layoutWheelEnd.setVisibility(View.GONE);
            }
        });
        tvTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTimeFilter(true);
                layoutWheelStart.setVisibility(View.GONE);
                layoutWheelEnd.setVisibility(View.VISIBLE);
                yearViewEnd.setSelectedIndex(selectedYearIndexEnd);
                monthViewEnd.setSelectedIndex(selectedMonthIndexEnd);
                dayViewEnd.setSelectedIndex(selectedDayIndexEnd);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmit();
            }
        });

    }

    boolean timeFilterEnd;

    void setTimeFilter(boolean end) {
        timeFilterEnd = end;
        if (end) {
            tvTimeStart.setTextColor(Color.parseColor("#999999"));
            tvTimeEnd.setTextColor(Color.parseColor("#333333"));
            tvTimeStart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            tvTimeEnd.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        } else {
            tvTimeStart.setTextColor(Color.parseColor("#333333"));
            tvTimeEnd.setTextColor(Color.parseColor("#999999"));
            tvTimeStart.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            tvTimeEnd.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }
    }

    public void setOnDatePickTwoListener(DatePickerTwo.OnDatePickTwoListener onDatePickListener) {
        this.onDatePickListener = onDatePickListener;
    }

    DatePickerTwo.OnDatePickTwoListener onDatePickListener;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    public interface OnDatePickTwoListener {
        void onDatePicked(String year, String month, String day, String year_end, String month_end, String day_end);
    }

    protected void onSubmit() {
        if (onDatePickListener == null) {
            return;
        }
        String year = getSelectedYear();
        String month = getSelectedMonth();
        String day = getSelectedDay();
        String year_end = getSelectedYearEnd();
        String month_end = getSelectedMonthEnd();
        String day_end = getSelectedDayEnd();
        onDatePickListener.onDatePicked(year, month, day, year_end, month_end, day_end);
    }

}
