package store.viomi.com.system.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by jlam on 2015/11/20 16:16.
 */
public class AutoScrollViewPager<T extends View> extends ViewPager {

    private long delayTime;
    private static final long DEFAULT_DELAY_TIME = 4000;
    private List<T> mDatas;
    private MyHandler handler;
    private OnViewPageChangeListener onViewPageChangeListener;

    public void setOnViewPageChangeListener(OnViewPageChangeListener onViewPageChangeListener) {
        this.onViewPageChangeListener = onViewPageChangeListener;

    }

    private void setListener() {
        if (onViewPageChangeListener != null) {
            this.addOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    onViewPageChangeListener.onPageScrolled(position % mDatas.size(), positionOffset, positionOffsetPixels);
                }

                @Override
                public void onPageSelected(int position) {
                    onViewPageChangeListener.onPageSelected(position % mDatas.size());
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    onViewPageChangeListener.onPageScrollStateChanged(state);
                }
            });
        }

    }

    private static final String TAG = "fire";
    private CustomViewPagerAdapter adapter;

    public AutoScrollViewPager(Context context) {
        this(context, null);
    }

    public AutoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        handler = new MyHandler();

    }


    public void setCustomAdapterDatas(List<T> datas) {
        this.mDatas = datas;
        adapter = new CustomViewPagerAdapter();
        adapter.notifyDataSetChanged();
        this.setAdapter(adapter);
        this.setCurrentItem(0);
        setListener();
    }

    public void setDelayTime(long delayTime) {
        this.delayTime = delayTime;
    }


    public void startAutoScroll() {
        if (delayTime != 0) {
            startAutoScroll(delayTime);
        } else {
            startAutoScroll(DEFAULT_DELAY_TIME);
        }
    }

    /**
     * 开始划
     */
    public void startAutoScroll(long delayTime) {
        if (mDatas.size() > 1) {
            this.delayTime = delayTime;
            handler.removeMessages(MSG_AUTO_SCROLL);
            handler.sendEmptyMessageDelayed(MSG_AUTO_SCROLL, delayTime);
        }
    }

    /**
     * 停止划
     */
    public void stopAutoScroll() {
        handler.removeMessages(MSG_AUTO_SCROLL);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                startAutoScroll();
                break;
            default:
                stopAutoScroll();
                break;
        }
        return super.onTouchEvent(ev);
    }


    class CustomViewPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            try {
                container.addView(mDatas.get(position % mDatas.size()));
            } catch (Exception e) {
//                e.printStackTrace();
//                Log.d(TAG, "position+" + position);
            }
            return mDatas.get(position % mDatas.size());
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            if (mDatas.size() > 3) {
                container.removeView(mDatas.get(position % mDatas.size()));
            }
        }
    }

    private static final int MSG_AUTO_SCROLL = 0;

    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_AUTO_SCROLL:
                    setCurrentItem(getCurrentItem() + 1);
                    sendEmptyMessageDelayed(MSG_AUTO_SCROLL, delayTime);
                    break;
                default:
                    super.handleMessage(msg);
                    break;
            }
            super.handleMessage(msg);
        }
    }

    public interface OnViewPageChangeListener {
        void onPageScrolled(int position, float positionOffset, int positionOffsetPixels);

        void onPageSelected(int position);

        void onPageScrollStateChanged(int state);
    }
}
