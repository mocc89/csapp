package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.AdvanceEntity;
import store.viomi.com.system.callback.ItemOpenCallBack;

/**
 * Created by viomi on 2017/1/3.
 */

public class AdvanceAdapter extends BaseAdapter {

    private List<AdvanceEntity> list;
    private Context context;
    private LayoutInflater inflater;
    private static final int TYPECOUNT = 3;
    private static final int TYPE1 = 0;
    private static final int TYPE2 = 1;
    private static final int TYPE3 = 2;
    private ItemOpenCallBack itemOpenCallBack;

    public AdvanceAdapter(List<AdvanceEntity> list, Context context, ItemOpenCallBack itemOpenCallBack) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.itemOpenCallBack = itemOpenCallBack;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return TYPECOUNT;
    }

    @Override
    public int getItemViewType(int position) {
        int vType = list.get(position).getType();
        return vType;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder1 holder1 = null;
        ViewHolder2 holder2 = null;
        ViewHolder3 holder3 = null;

        int vtype = getItemViewType(position);

        if (convertView == null) {

            switch (vtype) {
                case TYPE1: {
                    convertView = inflater.inflate(R.layout.advance_item_layout1, null);
                    holder1 = new ViewHolder1();
                    holder1.date = (TextView) convertView.findViewById(R.id.date);
                    holder1.order_count = (TextView) convertView.findViewById(R.id.order_count);
                    holder1.more = (ImageView) convertView.findViewById(R.id.more);
                    holder1.item = (RelativeLayout) convertView.findViewById(R.id.item);
                    convertView.setTag(holder1);
                    break;
                }
                case TYPE2: {
                    convertView = inflater.inflate(R.layout.advance_item_layout2, null);
                    holder2 = new ViewHolder2();
                    holder2.store_name = (TextView) convertView.findViewById(R.id.store_name);
                    holder2.agency_name = (TextView) convertView.findViewById(R.id.agency_name);
                    holder2.city_name = (TextView) convertView.findViewById(R.id.city_name);
                    holder2.cleaner_count = (TextView) convertView.findViewById(R.id.cleaner_count);
                    holder2.store_order_count = (TextView) convertView.findViewById(R.id.store_order_count);
                    holder2.store_order_sum = (TextView) convertView.findViewById(R.id.store_order_sum);
                    convertView.setTag(holder2);
                    break;
                }

                case TYPE3: {
                    convertView = inflater.inflate(R.layout.advance_item_layout3, null);
                    holder3 = new ViewHolder3();
                    convertView.setTag(holder3);
                    break;
                }
            }

        } else {

            switch (vtype) {
                case TYPE1: {
                    holder1 = (ViewHolder1) convertView.getTag();
                    break;
                }
                case TYPE2: {
                    holder2 = (ViewHolder2) convertView.getTag();
                    break;
                }
                case TYPE3: {
                    holder3 = (ViewHolder3) convertView.getTag();
                    break;
                }
            }

        }


        switch (vtype) {
            case TYPE1: {
                holder1.date.setText(list.get(position).getTime2());
                holder1.order_count.setText("¥" + String.format("%.2f", list.get(position).getStoreOrderSum()));

                if (list.get(position).isopen()) {
                    holder1.more.setImageResource(R.drawable.up);
                } else {
                    holder1.more.setImageResource(R.drawable.down);
                }

                holder1.item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (itemOpenCallBack != null) {
                            itemOpenCallBack.openClickCallBack(list.get(position).getTag());
                        }
                    }
                });
                break;
            }
            case TYPE2: {
                holder2.store_name.setText(list.get(position).getStoreName());
                holder2.agency_name.setText(list.get(position).getAgencyName());
                holder2.city_name.setText(list.get(position).getCityName());
                holder2.cleaner_count.setText(list.get(position).getCleanerCount() + "");
                holder2.store_order_count.setText(list.get(position).getStoreOrderCount() + "");
                holder2.store_order_sum.setText("¥" + String.format("%.2f", list.get(position).getStoreOrderSum()));
                break;
            }

            case TYPE3: {
                break;
            }
        }

        return convertView;
    }

    class ViewHolder1 {
        TextView date, order_count;
        ImageView more;
        RelativeLayout item;
    }

    class ViewHolder2 {
        TextView store_name, agency_name, city_name, cleaner_count, store_order_count, store_order_sum;
    }

    class ViewHolder3 {

    }
}
