package store.viomi.com.system.bean;

/**
 * Created by hailang on 2018/3/27 0027.
 */

public class Product {
    public String name;
    public String type_name;
    public long pending_count;
    public long stock;
}
