package store.viomi.com.system.bean;

/**
 * Created by viomi on 2017/1/10.
 */

public class AgencyDailyEntity {

    private long beginDate;
    private long endDate;
    private int orderCount;
    private double orderAmount;
    private int userCount;
    private int staffCount;
    private int parttimeCount;
    private int terminalCount;
    private String datelabel;
    private String datelabel2;
    private int cunrentParam;

    public AgencyDailyEntity() {
    }

    public AgencyDailyEntity(long beginDate, long endDate, int userCount, String datelabel, String datelabel2) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.userCount = userCount;
        this.datelabel = datelabel;
        this.datelabel2 = datelabel2;
    }

    public AgencyDailyEntity(long beginDate, long endDate, String datelabel, String datelabel2, int orderCount, double orderAmount, int userCount, int staffCount, int parttimeCount, int terminalCount, int cunrentParam) {
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.datelabel = datelabel;
        this.datelabel2 = datelabel2;
        this.orderCount = orderCount;
        this.orderAmount = orderAmount;
        this.userCount = userCount;
        this.staffCount = staffCount;
        this.parttimeCount = parttimeCount;
        this.terminalCount = terminalCount;
        this.cunrentParam = cunrentParam;
    }

    public long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(long beginDate) {
        this.beginDate = beginDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(int staffCount) {
        this.staffCount = staffCount;
    }

    public int getParttimeCount() {
        return parttimeCount;
    }

    public void setParttimeCount(int parttimeCount) {
        this.parttimeCount = parttimeCount;
    }

    public int getTerminalCount() {
        return terminalCount;
    }

    public void setTerminalCount(int terminalCount) {
        this.terminalCount = terminalCount;
    }

    public String getDatelabel() {
        return datelabel;
    }

    public void setDatelabel(String datelabel) {
        this.datelabel = datelabel;
    }

    public String getDatelabel2() {
        return datelabel2;
    }

    public void setDatelabel2(String datelabel2) {
        this.datelabel2 = datelabel2;
    }

    public int getCunrentParam() {
        return cunrentParam;
    }

    public void setCunrentParam(int cunrentParam) {
        this.cunrentParam = cunrentParam;
    }
}
