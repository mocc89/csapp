package store.viomi.com.system.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_forget_pw)
public class ForgetPwActivity extends BaseActivity {

    @ViewInject(R.id.lin)
    private LinearLayout lin;

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.input_account)
    private EditText input_account;

    @ViewInject(R.id.id_code)
    private EditText id_code;


    @ViewInject(R.id.get_code)
    private TextView get_code;


    @ViewInject(R.id.confirm)
    private Button confirm;

    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;


    private final static int REQUESTCODE = 1003;
    private final static int RESPONSECODE = 1004;

    private int time_count = 60;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJson(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }

                case 2: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJson2(result);
                    break;
                }
                case 3: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }
                case 4: {
                    time_count--;
                    get_code.setText(time_count + "秒后重新获取");
                    break;
                }
                case 5: {
                    time_count=60;
                    get_code.setEnabled(true);
                    get_code.setText("重发验证码");
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {

    }


    private void sendCode() {
        String accountNum = input_account.getText().toString();

        if (accountNum.isEmpty()) {
            ToastUtil.show(HintText.CANNOTEMPTY);
            return;
        }

        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.SENDCODE);
        requestParams.addBodyParameter("mobile", accountNum);

        loading();

        RequstUtils.postRquest(requestParams, mhandler, 0, 1);

    }

    private void parseJson(String result) {

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                ToastUtil.show("发送成功");
                setSendButton();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setSendButton() {
        get_code.setText("60秒后重新获取");
        get_code.setEnabled(false);
        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 60; i++) {
                    SystemClock.sleep(1000);
                    mhandler.sendEmptyMessage(4);
                }
                mhandler.sendEmptyMessage(5);
            }
        }).start();

    }


    private void checkCode() {
        String code = id_code.getText().toString();
        if (code.isEmpty()) {
            ToastUtil.show(HintText.CANNOTEMPTY);
            return;
        }

        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.RESTPASSWORD);
        requestParams.addBodyParameter("mobile", input_account.getText().toString());
        requestParams.addBodyParameter("authCode", code);

        loading();
        RequstUtils.postRquest(requestParams, mhandler, 2, 3);
    }

    private void parseJson2(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                showDialog();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.password_reset_dialog_layout, null);
        final Dialog dialog = new Dialog(this, R.style.selectorDialog);
        dialog.setContentView(view);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                setResult(RESPONSECODE);
                finish();
            }
        });
    }


    @Override
    protected void initListener() {

        lin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        get_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCode();
            }
        });


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCode();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUESTCODE && resultCode == RESPONSECODE) {
            finish();
        }
    }

    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
        }
        super.onBackPressed();
    }
}
