package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/10.
 */

public class ProductEntity {

    private String quantity;
    public String paymentPriceItem;
    public String name;
    private String imgUrl;
    public String skuName;
    public String price;

    public ProductEntity() {
    }

    public ProductEntity(String quantity, String paymentPriceItem, String name, String imgUrl) {
        this.quantity = quantity;
        this.paymentPriceItem = paymentPriceItem;
        this.name = name;
        this.imgUrl = imgUrl;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPaymentPriceItem() {
        return paymentPriceItem;
    }

    public void setPaymentPriceItem(String paymentPriceItem) {
        this.paymentPriceItem = paymentPriceItem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
