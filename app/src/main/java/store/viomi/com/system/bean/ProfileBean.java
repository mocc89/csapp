package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/11.
 */

public class ProfileBean {

    private int type;
    private String name;
    private int order;
    private double sales;
    private String salestitle;
    private String ordertitle;

    public ProfileBean() {
    }

    public ProfileBean(int type, String name, String salestitle, String ordertitle) {
        this.type = type;
        this.name = name;
        this.salestitle = salestitle;
        this.ordertitle = ordertitle;
    }

    public ProfileBean(int type, String name, int order, double sales) {
        this.type = type;
        this.name = name;
        this.order = order;
        this.sales = sales;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public double getSales() {
        return sales;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }

    public String getSalestitle() {
        return salestitle;
    }

    public void setSalestitle(String salestitle) {
        this.salestitle = salestitle;
    }

    public String getOrdertitle() {
        return ordertitle;
    }

    public void setOrdertitle(String ordertitle) {
        this.ordertitle = ordertitle;
    }
}
