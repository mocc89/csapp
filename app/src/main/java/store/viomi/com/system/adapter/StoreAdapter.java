package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.AgencyEntity;

/**
 * Created by viomi on 2016/11/16.
 */

public class StoreAdapter extends BaseAdapter {
    private List<AgencyEntity> list;
    private Context context;
    private LayoutInflater inflater;

    public StoreAdapter(List<AgencyEntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder=null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.store_item_layout, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.level = (TextView) convertView.findViewById(R.id.level);
            holder.count = (TextView) convertView.findViewById(R.id.count);
            holder.status = (TextView) convertView.findViewById(R.id.status);
            holder.itemlayout = (LinearLayout) convertView.findViewById(R.id.itemlayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(list.get(position).getName());
        holder.level.setText(list.get(position).getChannelLevel());
        holder.count.setText(list.get(position).getStaffNum());

        if (!("1".equals(list.get(position).getApproveStatus()) && "1".equals(list.get(position).getStatus()))) {
            holder.status.setTextColor(context.getResources().getColor(R.color.noaudit));
        } else {
            holder.status.setTextColor(context.getResources().getColor(R.color.audit));
        }

        if ("1".equals(list.get(position).getApproveStatus())) {
            holder.status.setText(list.get(position).getStatusDesc());
        } else {
            holder.status.setText(list.get(position).getApproveStatusDesc());
        }

        if (position % 2 == 0) {
            holder.itemlayout.setBackgroundResource(R.color.item_white);
        } else {
            holder.itemlayout.setBackgroundResource(R.color.item_dark);
        }

        return convertView;
    }

    class ViewHolder {
        TextView name, level, count, status;
        LinearLayout itemlayout;
    }
}
