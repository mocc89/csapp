package store.viomi.com.system.bean;

public class ChannelPurchaseOrderResp {
    public String dataTime;
    public String orderFee;
    public String orderNum;
    public String refundOrderFee;
    public String refundOrderNum;
    public String waresNum;
    public boolean selected;
}