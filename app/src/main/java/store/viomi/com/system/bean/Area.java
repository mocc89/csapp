package store.viomi.com.system.bean;

import java.io.Serializable;

/**
 * Created by hailang on 2018/4/24 0024.
 */

public class Area implements Serializable {
    public int id;
    public int version;
    public int code;
    public int parent;
    public int level;
    public int display;
    public String name;
}
