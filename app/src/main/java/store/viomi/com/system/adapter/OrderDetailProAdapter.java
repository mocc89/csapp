package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.xutils.image.ImageOptions;
import org.xutils.x;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.ProductEntity;

/**
 * Created by viomi on 2016/11/10.
 */

public class OrderDetailProAdapter extends BaseAdapter {

    private List<ProductEntity> list;
    private Context context;
    private LayoutInflater inflater;

    public OrderDetailProAdapter(List<ProductEntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_detail_product_item, null);
            holder = new ViewHolder();
            holder.product_name = (TextView) convertView.findViewById(R.id.product_name);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.count = (TextView) convertView.findViewById(R.id.count);
            holder.product_logo = (ImageView) convertView.findViewById(R.id.product_logo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.product_name.setText(list.get(position).getName());
        holder.price.setText("¥ " + list.get(position).getPaymentPriceItem());
        holder.count.setText("*" + list.get(position).getQuantity());
        ImageOptions imageOptions = new ImageOptions.Builder()
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .setFailureDrawableId(R.drawable.defult_img)
                .setLoadingDrawableId(R.drawable.defult_img)
                .build();
        x.image().bind(holder.product_logo, list.get(position).getImgUrl(), imageOptions);
        return convertView;
    }

    class ViewHolder {
        TextView product_name, price, count;
        ImageView product_logo;
    }
}
