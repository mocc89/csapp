package store.viomi.com.system.bean;

/**
 * Created by viomi on 2017/1/3.
 */

/*
* type 参数 必须 0 or 1
* */

public class AdvanceEntity {

    private int type;
    private String time;
    private String time2;
    private int orderCount;
    private long begintime;
    private long endtime;
    private String storeName;
    private String agencyName;
    private String cityName;
    private int cleanerCount;
    private int storeOrderCount;
    private double storeOrderSum;
    private boolean isopen;
    private int tag;

    public AdvanceEntity(int type) {
        this.type = type;
    }

    public AdvanceEntity(int type, String time, int orderCount, long begintime, long endtime, boolean isopen, int tag) {
        this.type = type;
        this.time = time;
        this.orderCount = orderCount;
        this.begintime = begintime;
        this.endtime = endtime;
        this.isopen = isopen;
        this.tag = tag;
    }

    public AdvanceEntity(int type, String time, String time2, int orderCount, long begintime, long endtime, boolean isopen, int tag) {
        this.type = type;
        this.time = time;
        this.time2 = time2;
        this.orderCount = orderCount;
        this.begintime = begintime;
        this.endtime = endtime;
        this.isopen = isopen;
        this.tag = tag;
    }

    public AdvanceEntity(int type, String time, String time2, int orderCount, long begintime, long endtime, double storeOrderSum, boolean isopen, int tag) {
        this.type = type;
        this.time = time;
        this.time2 = time2;
        this.orderCount = orderCount;
        this.begintime = begintime;
        this.endtime = endtime;
        this.storeOrderSum = storeOrderSum;
        this.isopen = isopen;
        this.tag = tag;
    }

    public AdvanceEntity(int type, String storeName, String agencyName, String cityName, int cleanerCount, int storeOrderCount, double storeOrderSum) {
        this.type = type;
        this.storeName = storeName;
        this.agencyName = agencyName;
        this.cityName = cityName;
        this.cleanerCount = cleanerCount;
        this.storeOrderCount = storeOrderCount;
        this.storeOrderSum = storeOrderSum;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCleanerCount() {
        return cleanerCount;
    }

    public void setCleanerCount(int cleanerCount) {
        this.cleanerCount = cleanerCount;
    }

    public int getStoreOrderCount() {
        return storeOrderCount;
    }

    public void setStoreOrderCount(int storeOrderCount) {
        this.storeOrderCount = storeOrderCount;
    }

    public double getStoreOrderSum() {
        return storeOrderSum;
    }

    public void setStoreOrderSum(double storeOrderSum) {
        this.storeOrderSum = storeOrderSum;
    }

    public boolean isopen() {
        return isopen;
    }

    public void setIsopen(boolean isopen) {
        this.isopen = isopen;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public long getBegintime() {
        return begintime;
    }

    public void setBegintime(long begintime) {
        this.begintime = begintime;
    }

    public long getEndtime() {
        return endtime;
    }

    public void setEndtime(long endtime) {
        this.endtime = endtime;
    }

    public String getTime2() {
        return time2;
    }

    public void setTime2(String time2) {
        this.time2 = time2;
    }
}
