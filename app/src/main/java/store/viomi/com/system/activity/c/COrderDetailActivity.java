package store.viomi.com.system.activity.c;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.OrderDetailProAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.ProductEntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_order_detail)
public class COrderDetailActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.order_Code)
    private TextView order_Code;

    @ViewInject(R.id.delivery_name)
    private TextView delivery_name;

    @ViewInject(R.id.delivery_address)
    private TextView delivery_address;

    @ViewInject(R.id.delivery_phone)
    private TextView delivery_phone;

    @ViewInject(R.id.productList)
    private ListView productList;

    @ViewInject(R.id.pay_method)
    private TextView pay_method;

    @ViewInject(R.id.pay_state)
    private TextView pay_state;

    @ViewInject(R.id.coupon)
    private TextView coupon;

    @ViewInject(R.id.order_carriage)
    private TextView order_carriage;

    @ViewInject(R.id.order_count)
    private TextView order_count;

    @ViewInject(R.id.created_time)
    private TextView created_time;

    @ViewInject(R.id.balance_status)
    private TextView balance_status;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;


    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("oook", result);
                    parseJSON(result);
                    break;
                }

                case 1: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    };
    private String store_code;


    @Override
    protected void init() {
        Intent intent = getIntent();
        store_code = intent.getStringExtra("ordercode");

        loadInfo();

    }

    private void loadInfo() {
        String url = MURL.ORDERDETAILC + store_code;
        RequestParams requestParams = RequstUtils.getHasTokenInstance(url);
        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void parseJSON(String result) {
        try {
            JSONObject json = new JSONObject(result);

            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");

            if (ResponseCode.isSuccess(code, desc)) {

                JSONObject resultJson = json.getJSONObject("result");

                //订单号
                String orderCode = JsonUitls.getString(resultJson, "orderCode");
                order_Code.setText(orderCode);

                //收货人姓名
                String linkmanName = JsonUitls.getString(resultJson, "linkmanName");
                delivery_name.setText(linkmanName);

                //收货地址
                String fullDivisionName = JsonUitls.getString(resultJson, "fullDivisionName");
                String address = JsonUitls.getString(resultJson, "address");
                String completeAddress = fullDivisionName + address;
                delivery_address.setText(completeAddress);

                //收货人电话
                String linkmanPhone = JsonUitls.getString(resultJson, "linkmanPhone");
                delivery_phone.setText(linkmanPhone);

                //支付方式
                String payTypeDesc = JsonUitls.getString(resultJson, "payTypeDesc");
                pay_method.setText(payTypeDesc);

                //支付状态
                String payStatusDesc = JsonUitls.getString(resultJson, "dealPhaseDesc");
                pay_state.setText(payStatusDesc);

                //优惠金额
                String discount = JsonUitls.getString(resultJson, "discount");
                coupon.setText("¥ " + discount);

                //运费
                double deliveryFee = JsonUitls.getDouble(resultJson, "deliveryFee");
                order_carriage.setText("¥ "+deliveryFee);

                //订单金额
                String paymentPrice = JsonUitls.getString(resultJson, "paymentPrice");
                order_count.setText("¥ " + paymentPrice);

                //创建时间
                long time = resultJson.getLong("createdTime");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String createdTime = sdf.format(new Date(time));
                created_time.setText(createdTime);

                //结算状态
                JSONObject profit = JsonUitls.getJSONObject(resultJson, "profit");
                String settleStatesDesc = JsonUitls.getString(profit, "statusDesc");
                if ("null".equals(settleStatesDesc)) {
                    settleStatesDesc = "-";
                }
                balance_status.setText(settleStatesDesc);

                //产品列表
                JSONArray skuInfoList = JsonUitls.getJSONArray(resultJson, "skuInfoList");
                List<ProductEntity> productEntityList = new ArrayList<>();
                for (int i = 0; i < skuInfoList.length(); i++) {
                    JSONObject data = skuInfoList.getJSONObject(i);
                    String quantity = JsonUitls.getString(data, "quantity");
                    String paymentPriceItem = JsonUitls.getString(data, "paymentPrice");
                    String name = JsonUitls.getString(data, "name");
                    String imgUrl = JsonUitls.getString(data, "imgUrl");
                    productEntityList.add(new ProductEntity(quantity, paymentPriceItem, name, imgUrl));
                }

                OrderDetailProAdapter adapter = new OrderDetailProAdapter(productEntityList, this);
                productList.setAdapter(adapter);
                resetListView(productList,adapter);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void resetListView(ListView listView, OrderDetailProAdapter adapter) {
        int count = adapter.getCount();
        int totalHeight = 0;
        for (int i = 0; i < count; i++) {
            View view = adapter.getView(i, null, listView);
            view.measure(0, 0);
            int measuredHeight = view.getMeasuredHeight();
            totalHeight += measuredHeight;
        }
        ViewGroup.LayoutParams lp = listView.getLayoutParams();
        lp.height = totalHeight + listView.getDividerHeight() * (count - 1);
        listView.setLayoutParams(lp);
        listView.requestLayout();
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadInfo();
            }
        });
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
