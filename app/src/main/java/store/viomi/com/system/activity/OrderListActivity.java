package store.viomi.com.system.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.OrderSearchParameter;
import store.viomi.com.system.fragment.OrderListFragment;
import store.viomi.com.system.fragment.OrderListThirdFragment;
import store.viomi.com.system.fragment.OrderListYufuFragment;

@ContentView(R.layout.activity_order_list)
public class OrderListActivity extends BaseActivity {
    final int REQUEST_SEARCH = 12;
    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;


    @ViewInject(R.id.tv1)
    private TextView tv1;
    @ViewInject(R.id.tv2)
    private TextView tv2;
    @ViewInject(R.id.tv3)
    private TextView tv3;
    @ViewInject(R.id.tv4)
    private TextView tv4;

    @ViewInject(R.id.line1)
    private View line1;
    @ViewInject(R.id.line2)
    private View line2;
    @ViewInject(R.id.line3)
    private View line3;
    @ViewInject(R.id.line4)
    private View line4;

    private Fragment currentFragment = new Fragment();
    //云米商城
    private OrderListFragment fg1;
    //米家
    private OrderListThirdFragment fg2;
    //天貓：2018-4-9未开通
    private OrderListThirdFragment fg3;
    private OrderListYufuFragment fg4;

    @Override
    protected void init() {
        fg1 = OrderListFragment.getInstance();
        fg2 = OrderListThirdFragment.getInstance("1");
        fg3 = OrderListThirdFragment.getInstance("2");
        fg4 = OrderListYufuFragment.getInstance();
        setNavigation(1);
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderListActivity.this, OrderSelectNewActivity.class);
                startActivityForResult(intent, REQUEST_SEARCH);
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(1);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(2);
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(3);
            }
        });
        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(4);
            }
        });
    }

    private void setNavigation(int type) {

        switch (type) {
            case 1: {
                tv1.setTextColor(getResources().getColor(R.color.selectItem));
                tv2.setTextColor(getResources().getColor(R.color.unselectItem));
                tv3.setTextColor(getResources().getColor(R.color.unselectItem));
                tv4.setTextColor(getResources().getColor(R.color.unselectItem));
                line1.setVisibility(View.VISIBLE);
                line2.setVisibility(View.INVISIBLE);
                line3.setVisibility(View.INVISIBLE);
                line4.setVisibility(View.INVISIBLE);
                break;
            }

            case 2: {
                tv2.setTextColor(getResources().getColor(R.color.selectItem));
                tv1.setTextColor(getResources().getColor(R.color.unselectItem));
                tv3.setTextColor(getResources().getColor(R.color.unselectItem));
                tv4.setTextColor(getResources().getColor(R.color.unselectItem));
                line2.setVisibility(View.VISIBLE);
                line1.setVisibility(View.INVISIBLE);
                line3.setVisibility(View.INVISIBLE);
                line4.setVisibility(View.INVISIBLE);
                break;
            }
            case 3: {
                tv3.setTextColor(getResources().getColor(R.color.selectItem));
                tv2.setTextColor(getResources().getColor(R.color.unselectItem));
                tv1.setTextColor(getResources().getColor(R.color.unselectItem));
                tv4.setTextColor(getResources().getColor(R.color.unselectItem));
                line3.setVisibility(View.VISIBLE);
                line2.setVisibility(View.INVISIBLE);
                line1.setVisibility(View.INVISIBLE);
                line4.setVisibility(View.INVISIBLE);
                break;
            }
            case 4: {
                tv3.setTextColor(getResources().getColor(R.color.unselectItem));
                tv2.setTextColor(getResources().getColor(R.color.unselectItem));
                tv1.setTextColor(getResources().getColor(R.color.unselectItem));
                tv4.setTextColor(getResources().getColor(R.color.selectItem));
                line3.setVisibility(View.INVISIBLE);
                line2.setVisibility(View.INVISIBLE);
                line1.setVisibility(View.INVISIBLE);
                line4.setVisibility(View.VISIBLE);
                break;
            }
            default:
                break;
        }


        Fragment fragment = null;
        switch (type) {
            case 1: {
                if (fg1 == null) {
                    fg1 = OrderListFragment.getInstance();
                }
                fragment = fg1;
                break;
            }
            case 2: {
                if (fg2 == null) {
                    fg2 = OrderListThirdFragment.getInstance("1");
                }
                fragment = fg2;
                break;
            }
            case 3: {

                if (fg3 == null) {
                    fg3 = OrderListThirdFragment.getInstance("2");
                }
                fragment = fg3;
                break;
            }
            case 4: {

                if (fg4 == null) {
                    fg4 = OrderListYufuFragment.getInstance();
                }
                fragment = fg4;
                break;
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.hide(currentFragment).show(fragment);
        } else {
            transaction.hide(currentFragment).add(R.id.lin, fragment);
        }
        transaction.commit();
        currentFragment = fragment;
    }


    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_SEARCH) {
                OrderSearchParameter param = (OrderSearchParameter) data.getSerializableExtra("param");
                if (param.souceType == 0) {
                    if (fg1 != null) fg1.onNewFilter(param);
                    setNavigation(1);
                } else if (param.souceType == 1) {
                    setNavigation(2);
                    if (fg2 != null) fg2.onNewFilter(param);
                } else if (param.souceType == 2) {
                    setNavigation(3);
                    if (fg3 != null) fg3.onNewFilter(param);
                } else if (param.souceType == 3) {
                    setNavigation(4);
                    if (fg4 != null) fg4.onNewFilter(param);
                }
            }
        }
    }
}
