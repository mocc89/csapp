package store.viomi.com.system.base;

public class ChannelPurchaseOrderTotalDTO {


    public String orderFee;
    public String orderNum;
    public String refundOrderFee;
    public String refundOrderNum;
    public String waresNum;

}
