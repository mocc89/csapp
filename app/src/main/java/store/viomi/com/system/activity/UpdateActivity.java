package store.viomi.com.system.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.UpdateTxAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.Config;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.service.DownloadService;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;


@ContentView(R.layout.activity_update)
public class UpdateActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.check)
    private Button check;
    @ViewInject(R.id.tv_update)
    private TextView tvUpdate;

    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;
    private final static int PERMISSION_WRITE_EX_REQCODE = 100100;


    private String downlink = "";
    private String version = "";

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 5:
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("update", result);
                    parseJson(result);
                    break;
                case 6:
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
            }
        }
    };


    @Override
    protected void init() {
        try {
            tvUpdate.setText("当前版本：" + getPackageManager().
                    getPackageInfo(getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }


    private void reqUpdate() {
        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.UPDATE);
        requestParams.addBodyParameter("type", "version");
        requestParams.addBodyParameter("package", "store.viomi.com.system");
        requestParams.addBodyParameter("channel", Config.UPDATEENVIRONMENT);
        requestParams.addBodyParameter("p", "1");
        requestParams.addBodyParameter("l", "1");
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 5, 6);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONArray data = JsonUitls.getJSONArray(json, "data");
            if (data != null && data.length() > 0) {
                JSONObject item = data.getJSONObject(0);
                String detail = JsonUitls.getString(item, "detail");
                version = JsonUitls.getString(item, "code");
                downlink = JsonUitls.getString(item, "url");
                if (!Config.getCurrentversion(activity).equals(version) && !"210".equals(version)) {
                    showUpdateDialog(detail);
                } else {
                    ToastUtil.show(HintText.UPDATENONEED);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog(String code) {
        final Dialog dialog = new Dialog(this, R.style.selectorDialog);
        View view = LayoutInflater.from(this).inflate(R.layout.update_dialog_layout, null);
        dialog.setContentView(view);

        TextView update_title = (TextView) view.findViewById(R.id.update_title);
        ListView tv_list = (ListView) view.findViewById(R.id.tv_list);
        TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
        TextView ok_update = (TextView) view.findViewById(R.id.ok_update);

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < version.length(); i++) {
            sb.append(version.charAt(i));
            if (i != version.length() - 1) {
                sb.append(".");
            }
        }
        String version_a = sb.toString();
        update_title.setText("云分销V" + version_a + "版本发布啦，马上下载体验吧！");

        String[] splits = code.split("#");
        UpdateTxAdapter adapter = new UpdateTxAdapter(splits, this);
        tv_list.setAdapter(adapter);

        no_thanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ok_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadUpdate();
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    private void downloadUpdate() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int i = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (i == -1) {
                ToastUtil.show(HintText.PERMISSIONDENY_WRITEEX);
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_WRITE_EX_REQCODE);
                return;
            }
        }

        ToastUtil.show(HintText.BACKGROUNDDOWNLOAD);
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("downlink", downlink);
        intent.putExtra("version", version);
        startService(intent);

    }


    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reqUpdate();
            }
        });
    }


    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_WRITE_EX_REQCODE) {

            int grantResult = grantResults[0];
            if (grantResult == 0) {
                ToastUtil.show(HintText.BACKGROUNDDOWNLOAD);
                Intent intent = new Intent(this, DownloadService.class);
                intent.putExtra("downlink", downlink);
                intent.putExtra("version", version);
                startService(intent);
            } else {
                ToastUtil.show(HintText.PERMISSIONDENY_WRITEEX);
            }
        }
    }

}
