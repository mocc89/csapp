package store.viomi.com.system.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.AgencyDailyAdapter;
import store.viomi.com.system.adapter.DataSpinnerAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.AgencyDailyDetailEntity;
import store.viomi.com.system.bean.AgencyDailyEntity;
import store.viomi.com.system.callback.ADItemOpenCallBack;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.DisplayUtil;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.NumberUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.TimeUtils;
import store.viomi.com.system.widget.PinnedSectionListView;

@ContentView(R.layout.fragment_agency_daily)
public class AgencyDailyFragment extends BaseFragment implements ADItemOpenCallBack {

    @ViewInject(R.id.dailylist)
    private PinnedSectionListView dailylist;

    //加载中布局
    @ViewInject(R.id.loading_layout)
    private RelativeLayout loading_layout;
    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    private String typeNo;
    private List<AgencyDailyEntity> adelist;
    private List<AgencyDailyDetailEntity> addelist;
    private AgencyDailyAdapter adapter;
    private int currentSelect;
    private boolean isdestroy;

    private TextView currentCount;
    private TextView time;
    private LinearLayout lin_container;
    private HorizontalScrollView horsc;


    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if (!isdestroy) {
                switch (msg.what) {
                    case 1: {
                        reconnect_layout.setVisibility(View.GONE);
                        String result = (String) msg.obj;
                        LogUtil.mlog("viomi", result);
                        parseJson(result);
                        break;

                    }
                    case 2: {
                        loadingfinish();
                        reconnect_layout.setVisibility(View.VISIBLE);
                        break;
                    }
                    case 3: {
                        loadingfinish();
                        String result = (String) msg.obj;
                        LogUtil.mlog("viomi", result);
                        psrseJson2(result);
                        break;

                    }
                    case 4: {
                        loadingfinish();
                        ResponseCode.onErrorHint(msg.obj);
                        break;
                    }
                }
            }
        }
    };
    private List<String> datas;
    private int typeCurrent;


    public static AgencyDailyFragment newInstance(String type, String text) {
        AgencyDailyFragment fragment = new AgencyDailyFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        args.putString("text", text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void init() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNo = arguments.getString("type", "null");
        }

        addelist = new ArrayList<>();
        loaddata();
    }

    private void loaddata() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.AGENCYDAILY);
        requestParams.addBodyParameter("type", typeNo);
        loading();
        RequstUtils.getRquest(requestParams, mhandler, 1, 2);
    }

    private void parseJson(String result) {
        adelist = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");

            if (ResponseCode.isSuccess(code, desc)) {
                JSONArray resultArray = JsonUitls.getJSONArray(json, "result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject item = resultArray.getJSONObject(resultArray.length() - i - 1);
                    long beginDate = JsonUitls.getLong(item, "beginDate");
                    long endDate = JsonUitls.getLong(item, "endDate");
                    JSONObject data = JsonUitls.getJSONObject(item, "data");

                    int orderCount = JsonUitls.getInt(data, "orderCount");
                    double orderAmount = JsonUitls.getDouble(data, "orderAmount");
                    int userCount = JsonUitls.getInt(data, "userCount");
                    int staffCount = JsonUitls.getInt(data, "staffCount");
                    int parttimeCount = JsonUitls.getInt(data, "parttimeCount");
                    int terminalCount = JsonUitls.getInt(data, "terminalCount");

                    String datelabel = null;
                    String datelabel2 = null;

                    switch (typeNo) {
                        case "1":
                            datelabel = TimeUtils.stampToMMdd(beginDate);
                            datelabel2 = TimeUtils.stampToMMdd(beginDate);
                            if (i == (resultArray.length() - 1)) {
                                datelabel = "昨日";
                                datelabel2 = "昨日";
                            }
                            break;
                        case "2":
                            datelabel = TimeUtils.stampToMMdd(beginDate) + "-\n" + TimeUtils.stampToMMdd(endDate);
                            datelabel2 = TimeUtils.stampToMMdd(beginDate) + "-" + TimeUtils.stampToMMdd(endDate);
                            if (i == (resultArray.length() - 1)) {
                                datelabel = "本周";
                                datelabel2 = "本周";
                            }
                            break;
                        case "3":
                            datelabel = String.format("%tB", new Date(beginDate));
                            datelabel2 = String.format("%tB", new Date(beginDate));

                            if ("一月".equals(datelabel) || "十二月".equals(datelabel)) {
                                datelabel = datelabel + "\n" + new SimpleDateFormat("yyyy").format(new Timestamp(beginDate));
                            }

                            if (i == (resultArray.length() - 1)) {
                                datelabel2 = "本月";
                            }
                            break;
                    }

                    int cunrentParam = 0;
                    switch (typeCurrent) {
                        case 0:
                            cunrentParam = userCount;
                            break;
                        case 1:
                            cunrentParam = parttimeCount;
                            break;
                        case 2:
                            cunrentParam = staffCount;
                            break;
                        case 3:
                            cunrentParam = terminalCount;
                            break;
                        case 4:
                            cunrentParam = (int) orderAmount;
                            break;
                        case 5:
                            cunrentParam = orderCount;
                            break;
                    }
                    adelist.add(new AgencyDailyEntity(beginDate, endDate, datelabel, datelabel2, orderCount, orderAmount, userCount, staffCount, parttimeCount, terminalCount, cunrentParam));
                }
                setdata();
                columnClick(currentSelect);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setdata() {

        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.agency_daily_header, null);
        lin_container = (LinearLayout) headerView.findViewById(R.id.lin_container);
        horsc = (HorizontalScrollView) headerView.findViewById(R.id.horsc);
        time = (TextView) headerView.findViewById(R.id.time);
        currentCount = (TextView) headerView.findViewById(R.id.currentCount);
        Spinner spinner = (Spinner) headerView.findViewById(R.id.spinner);

        datas = new ArrayList<String>();
        datas.add("新增微信会员数");
        datas.add("新增兼职店员数");
        datas.add("新增店员数");
        datas.add("新增门店数");
        datas.add("订单金额");
        datas.add("订单数");
        DataSpinnerAdapter selectAdapter = new DataSpinnerAdapter(getActivity(), datas);
        spinner.setAdapter(selectAdapter);
//        spinner.setSelection(2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeCurrent = position;
                if (addelist.size() > 0) {
                    addelist.get(0).setParamer_title(datas.get(position));
                }

                switch (position) {
                    case 0:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("" + addelist.get(i).getUserCount());
                            addelist.get(i).setSortParam(addelist.get(i).getUserCount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam(adelist.get(i).getUserCount());
                        }
                        break;
                    case 1:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("" + addelist.get(i).getParttimeCount());
                            addelist.get(i).setSortParam(addelist.get(i).getParttimeCount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam(adelist.get(i).getParttimeCount());
                        }
                        break;
                    case 2:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("" + addelist.get(i).getStaffCount());
                            addelist.get(i).setSortParam(addelist.get(i).getStaffCount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam(adelist.get(i).getStaffCount());
                        }
                        break;
                    case 3:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("" + addelist.get(i).getTerminalCount());
                            addelist.get(i).setSortParam(addelist.get(i).getTerminalCount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam(adelist.get(i).getTerminalCount());
                        }
                        break;
                    case 4:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("¥" + NumberUtil.getValue2(addelist.get(i).getOrderAmount()));
                            addelist.get(i).setSortParam(addelist.get(i).getOrderAmount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam((int) adelist.get(i).getOrderAmount());
                        }
                        break;
                    case 5:
                        for (int i = 0; i < addelist.size(); i++) {
                            addelist.get(i).setSelectParams("" + addelist.get(i).getOrderCount());
                            addelist.get(i).setSortParam(addelist.get(i).getOrderCount());
                        }
                        for (int i = 0; i < adelist.size(); i++) {
                            adelist.get(i).setCunrentParam(adelist.get(i).getOrderCount());
                        }
                        break;
                }


                float max = 0;
                for (int i = 0; i < adelist.size(); i++) {
                    if (adelist.get(i).getCunrentParam() > max) {
                        max = adelist.get(i).getCunrentParam();
                    }
                }
                int mymaxheight = (int) (DisplayUtil.getdensity(getActivity()) * 175);
                if (max <= 0) {
                    max = 1;
                    mymaxheight = 0;
                }

                for (int i = 0; i < lin_container.getChildCount(); i++) {
                    View childAt = lin_container.getChildAt(i);
                    TextView amountv = (TextView) childAt.findViewById(R.id.amount);
                    ViewGroup.LayoutParams layoutParams = amountv.getLayoutParams();
                    layoutParams.height = (int) (mymaxheight * (adelist.get(i).getCunrentParam() / max)) + 1;
                    amountv.setLayoutParams(layoutParams);
                }
                currentCount.setText(adelist.get(currentSelect).getCunrentParam() + "");
                addelist = sortArray(addelist);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        float max = 0;
        for (int i = 0; i < adelist.size(); i++) {
            if (adelist.get(i).getCunrentParam() > max) {
                max = adelist.get(i).getCunrentParam();
            }
        }
        int mymaxheight = (int) (DisplayUtil.getdensity(getActivity()) * 175);
        if (max <= 0) {
            max = 1;
            mymaxheight = 0;
        }


        for (int i = 0; i < adelist.size(); i++) {
            View child = LayoutInflater.from(getActivity()).inflate(R.layout.agency_daily_header_item, null);
            TextView label = (TextView) child.findViewById(R.id.label);
            TextView amount = (TextView) child.findViewById(R.id.amount);
            RelativeLayout click_area = (RelativeLayout) child.findViewById(R.id.click_area);

            if (i == adelist.size() - 1) {
                amount.setBackgroundResource(R.color.agencyselectItem);
                label.setTextColor(getResources().getColor(R.color.selectItem));
            }

            ViewGroup.LayoutParams layoutParams = amount.getLayoutParams();
            layoutParams.height = (int) (mymaxheight * (adelist.get(i).getCunrentParam() / max)) + 1;

            label.setText(adelist.get(i).getDatelabel());

            final int finalI = i;
            click_area.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    currentSelect = finalI;
                    currentCount.setText(adelist.get(currentSelect).getCunrentParam() + "");
                    time.setText(adelist.get(currentSelect).getDatelabel2());

                    for (int k = 0; k < lin_container.getChildCount(); k++) {
                        View vk = lin_container.getChildAt(k);
                        TextView amountv = (TextView) vk.findViewById(R.id.amount);
                        TextView labeltv = (TextView) vk.findViewById(R.id.label);
                        amountv.setBackgroundResource(R.color.agencyunselectItem);
                        labeltv.setTextColor(getResources().getColor(R.color.unselectItem));
                    }

                    View vk = lin_container.getChildAt(currentSelect);
                    TextView amountv = (TextView) vk.findViewById(R.id.amount);
                    TextView labeltv = (TextView) vk.findViewById(R.id.label);
                    amountv.setBackgroundResource(R.color.agencyselectItem);
                    labeltv.setTextColor(getResources().getColor(R.color.selectItem));
                    columnClick(currentSelect);
                }
            });

            lin_container.addView(child);
        }

        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                horsc.fullScroll(ScrollView.FOCUS_RIGHT);
            }
        }, 100);

        if (adelist.size() > 0) {
            currentSelect = adelist.size() - 1;
            currentCount.setText(adelist.get(currentSelect).getUserCount() + "");
            time.setText(adelist.get(currentSelect).getDatelabel2());
        }

        addelist.add(new AgencyDailyDetailEntity(0, datas.get(typeCurrent)));
        adapter = new AgencyDailyAdapter(addelist, getActivity(), this);
        dailylist.addHeaderView(headerView);
        dailylist.setAdapter(adapter);
    }

    private void columnClick(int currentSelect) {
        RequestParams req = RequstUtils.getHasTokenInstance(MURL.AGENCYDAILYSEARCH);
        req.addBodyParameter("beginDate", TimeUtils.stampToyyyyMMdd(adelist.get(currentSelect).getBeginDate()));
        req.addBodyParameter("endDate", TimeUtils.stampToyyyyMMdd(adelist.get(currentSelect).getEndDate()));
        loading();
        RequstUtils.getRquest(req, mhandler, 3, 4);
    }

    private void psrseJson2(String result) {
        addelist.clear();
        addelist.add(new AgencyDailyDetailEntity(0, adelist.get(currentSelect).getDatelabel(), datas.get(typeCurrent)));
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                JSONArray resultArray = JsonUitls.getJSONArray(json, "result");
                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject item = resultArray.getJSONObject(i);
                    String agencyName = JsonUitls.getString(item, "secondChannel");
                    String cityName = JsonUitls.getString(item, "rootChannel");

                    int uc = JsonUitls.getInt(item, "userCount");
                    int pc = JsonUitls.getInt(item, "parttimeCount");
                    int sc = JsonUitls.getInt(item, "staffCount");
                    int tc = JsonUitls.getInt(item, "terminalCount");
                    int oc = JsonUitls.getInt(item, "orderCount");
                    double oa = JsonUitls.getDouble(item, "orderAmount");

                    String selectParam = "";
                    double sortParam = 0;

                    switch (typeCurrent) {
                        case 0:
                            selectParam = "" + uc;
                            sortParam = uc;
                            break;
                        case 1:
                            selectParam = "" + pc;
                            sortParam = pc;
                            break;
                        case 2:
                            selectParam = "" + sc;
                            sortParam = sc;
                            break;
                        case 3:
                            selectParam = "" + tc;
                            sortParam = tc;
                            break;
                        case 4:
                            selectParam = "¥" + NumberUtil.getValue2(oa);
                            sortParam = oa;
                            break;
                        case 5:
                            selectParam = "" + oc;
                            sortParam = oc;
                            break;
                    }

                    AgencyDailyDetailEntity entity = new AgencyDailyDetailEntity(1, agencyName, cityName, uc, tc, sc, pc, false, oc, oa, selectParam, sortParam);
                    addelist.add(entity);
                }
                if (resultArray.length() == 0) {
                    addelist.add(new AgencyDailyDetailEntity(2, datas.get(typeCurrent)));
                }
                addelist = sortArray(addelist);
                adapter.notifyDataSetChanged();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //排序
    private List<AgencyDailyDetailEntity> sortArray(List<AgencyDailyDetailEntity> dataList) {

        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 1; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getSortParam() < dataList.get(j + 1).getSortParam()) {
                    AgencyDailyDetailEntity temp1 = dataList.get(j);
                    AgencyDailyDetailEntity temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }
        return dataList;
    }


    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loaddata();
            }
        });
    }

    @Override
    protected void loading() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_layout.setVisibility(View.GONE);
    }

    @Override
    public void openClickCallBack(int position) {
        AgencyDailyDetailEntity entity = addelist.get(position);
        entity.setIsopen(!entity.isopen());
        addelist.set(position, entity);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestroy = true;
        mhandler.removeCallbacksAndMessages(null);
    }
}
