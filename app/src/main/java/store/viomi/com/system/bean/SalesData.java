package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/3.
 */

public class SalesData {

    private int type;
    private String name;
    private String order;
    private String sales;
    private String percentage;

    public SalesData() {
    }

    public SalesData(int type, String name, String order, String sales, String percentage) {
        this.type = type;
        this.name = name;
        this.order = order;
        this.sales = sales;
        this.percentage = percentage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
}
