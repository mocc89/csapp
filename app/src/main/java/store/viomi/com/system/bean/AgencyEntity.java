package store.viomi.com.system.bean;

/**
 * Created by viomi on 2016/11/16.
 */

public class AgencyEntity {

    private String id;
    private String name;
    private String channelLevel;
    private String staffNum;
    private String statusDesc;
    private String approveStatusDesc;
    private String status;
    private String approveStatus;

    public AgencyEntity() {
    }

    public AgencyEntity(String id, String name, String channelLevel, String staffNum, String statusDesc, String approveStatusDesc, String status, String approveStatus) {
        this.id = id;
        this.name = name;
        this.channelLevel = channelLevel;
        this.staffNum = staffNum;
        this.statusDesc = statusDesc;
        this.approveStatusDesc = approveStatusDesc;
        this.status = status;
        this.approveStatus = approveStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChannelLevel() {
        return channelLevel;
    }

    public void setChannelLevel(String channelLevel) {
        this.channelLevel = channelLevel;
    }

    public String getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(String staffNum) {
        this.staffNum = staffNum;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getApproveStatusDesc() {
        return approveStatusDesc;
    }

    public void setApproveStatusDesc(String approveStatusDesc) {
        this.approveStatusDesc = approveStatusDesc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }
}
