package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.AgencyDailyREntity;

/**
 * Created by viomi on 2017/1/9.
 */

public class AgencyDailyResultAdapter extends BaseAdapter {

    private List<AgencyDailyREntity> list;
    private Context context;
    private LayoutInflater inflater;

    public AgencyDailyResultAdapter(List<AgencyDailyREntity> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.agency_daily_result_item_layout, null);
            holder = new ViewHolder();
            holder.date = (TextView) convertView.findViewById(R.id.date);
            holder.agency_name = (TextView) convertView.findViewById(R.id.agency_name);
            holder.city_name = (TextView) convertView.findViewById(R.id.city_name);
            holder.user_count = (TextView) convertView.findViewById(R.id.user_count);
            holder.new_store_count = (TextView) convertView.findViewById(R.id.new_store_count);
            holder.new_clerk_count = (TextView) convertView.findViewById(R.id.new_clerk_count);
            holder.new_pt_clerk_count = (TextView) convertView.findViewById(R.id.new_pt_clerk_count);
            holder.order_count = (TextView) convertView.findViewById(R.id.order_count);
            holder.order_amount = (TextView) convertView.findViewById(R.id.order_amount);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.date.setText(list.get(position).getDay());
        holder.agency_name.setText(list.get(position).getSecondChannel());
        holder.city_name.setText(list.get(position).getRootChannel());
        holder.user_count.setText(list.get(position).getUserCount());
        holder.new_store_count.setText(list.get(position).getTerminalCount());
        holder.new_clerk_count.setText(list.get(position).getStaffCount());
        holder.new_pt_clerk_count.setText(list.get(position).getParttimeCount());
        holder.order_count.setText(list.get(position).getOrderCount());
        holder.order_amount.setText("¥"+list.get(position).getOrderAmount());
        return convertView;
    }

    class ViewHolder {
        TextView date, agency_name, city_name, user_count, new_store_count, new_clerk_count, new_pt_clerk_count,order_count,order_amount;
    }
}
