package store.viomi.com.system.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.fragment.AdvanceFragment;

@ContentView(R.layout.activity_advance_order)
public class AdvanceOrderActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;

    @ViewInject(R.id.tv1)
    private TextView tv1;
    @ViewInject(R.id.tv2)
    private TextView tv2;
    @ViewInject(R.id.tv3)
    private TextView tv3;

    @ViewInject(R.id.line1)
    private View line1;
    @ViewInject(R.id.line2)
    private View line2;
    @ViewInject(R.id.line3)
    private View line3;

    private Fragment currentFragment = new Fragment();
    private Fragment fg1;
    private Fragment fg2;
    private Fragment fg3;

    @Override
    protected void init() {
        setNavigation(tv1, tv2, tv3, line1, line2, line3, 1);

    }

    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdvanceOrderActivity.this, AdvanceSelectActivity.class);
                startActivity(intent);
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv1, tv2, tv3, line1, line2, line3, 1);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv2, tv1, tv3, line2, line1, line3, 2);
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv3, tv2, tv1, line3, line2, line1, 3);
            }
        });
    }

    private void setNavigation(TextView tv1, TextView tv2, TextView tv3, View line1, View line2, View line3, int type) {
        tv1.setTextColor(getResources().getColor(R.color.selectItem));
        tv2.setTextColor(getResources().getColor(R.color.unselectItem));
        tv3.setTextColor(getResources().getColor(R.color.unselectItem));
        line1.setVisibility(View.VISIBLE);
        line2.setVisibility(View.INVISIBLE);
        line3.setVisibility(View.INVISIBLE);

        Fragment fragment = null;
        switch (type) {
            case 1: {
                if (fg1 == null) {
                    fg1 = AdvanceFragment.newInstance("1", "近7天-预付款金额");
                }
                fragment = fg1;
                break;
            }
            case 2: {
                if (fg2 == null) {
                    fg2 = AdvanceFragment.newInstance("2", "近7周-预付款金额");
                }
                fragment = fg2;
                break;
            }
            case 3: {

                if (fg3 == null) {
                    fg3 = AdvanceFragment.newInstance("3", "近7月-预付款金额");
                }
                fragment = fg3;
                break;
            }
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.hide(currentFragment).show(fragment);
        } else {
            transaction.hide(currentFragment).add(R.id.lin, fragment);
        }
        transaction.commit();
        currentFragment = fragment;

    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }

}
