package store.viomi.com.system.other;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.List;

/**
 * Created by viomi on 2016/11/10.
 */

public class LebalSet implements IAxisValueFormatter {

    private List<String> list;

    public LebalSet(List<String> list) {
        this.list = list;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        if (list.size() > (int) value) {
            return list.get((int) value);
        } else {
            return "";
        }
    }

    @Override
    public int getDecimalDigits() {
        return 0;
    }
}
