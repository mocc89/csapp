package store.viomi.com.system.fragment.c;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.activity.LoginActivity;
import store.viomi.com.system.activity.ModifyPwActivity;
import store.viomi.com.system.activity.UpdateActivity;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.utils.SharePreferencesUtils;
import viomi.com.umsdk.MyMobclickAgent;

import static android.content.Context.MODE_PRIVATE;


@ContentView(R.layout.fragment_home_a)
public class HomeCFragment extends BaseFragment {

    //用户名
    @ViewInject(R.id.user_name)
    private TextView user_name;

    //渠道名
    @ViewInject(R.id.channel_name)
    private TextView channel_name;


    //修改密码
    @ViewInject(R.id.modify_password)
    private RelativeLayout modify_password;


    //版本更新
    @ViewInject(R.id.check_update)
    private RelativeLayout check_update;

    //退出登录
    @ViewInject(R.id.login_out)
    private RelativeLayout login_out;

    private static HomeCFragment fragment;
    private SharedPreferences sp;

    public static HomeCFragment getInstance() {
        if (fragment == null) {
            synchronized (HomeCFragment.class) {
                if (fragment == null) {
                    fragment = new HomeCFragment();
                }
            }
        }
        return fragment;
    }


    @Override
    protected void init() {
        user_name.setText(SharePreferencesUtils.getInstance().getUser_name(getActivity()));
        channel_name.setText(SharePreferencesUtils.getInstance().getChannel_name(getActivity()) + " | 城市运营");
        sp = getActivity().getSharedPreferences("mysp", MODE_PRIVATE);

    }

    @Override
    protected void initListener() {


        //修改密码
        modify_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_mine_icon1");

                Intent intent = new Intent(getActivity(), ModifyPwActivity.class);
                startActivity(intent);
            }
        });


        //版本更新
        check_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_mine_icon2");

                Intent intent = new Intent(getActivity(), UpdateActivity.class);
                startActivity(intent);
            }
        });


        //退出登录
        login_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMobclickAgent.onEvent(getActivity(), "city_mine_icon3");

                final Dialog dialog = new Dialog(getActivity(), R.style.selectorDialog);
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.loginout_dialog_layout, null);
                dialog.setContentView(view);

                TextView no_thanks = (TextView) view.findViewById(R.id.no_thanks);
                TextView ok_do = (TextView) view.findViewById(R.id.ok_do);

                no_thanks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                ok_do.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("md5String", "");
                        editor.commit();
                        SharePreferencesUtils.getInstance().setToken(getActivity(), "");
                        SharePreferencesUtils.getInstance().setUser_name(getActivity(), "");
                        SharePreferencesUtils.getInstance().setChannel_name(getActivity(), "");
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


    }

    @Override
    protected void loading() {
    }

    @Override
    protected void loadingfinish() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment = null;
    }
}
