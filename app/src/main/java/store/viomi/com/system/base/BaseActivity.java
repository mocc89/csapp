package store.viomi.com.system.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.umeng.analytics.MobclickAgent;

import org.xutils.x;

/**
 * Created by viomi on 2016/10/18.
 */

public abstract class BaseActivity extends AppCompatActivity {

    public final String TAG = BaseActivity.this.getClass().getSimpleName();
    public Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        x.view().inject(this);
        init();
        initListener();
    }

    protected abstract void init();

    protected abstract void initListener();

    protected abstract void loading();

    protected abstract void loadingfinish();

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        this.finish();
        super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(getClass().getName());
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(getClass().getName());
        MobclickAgent.onPause(this);
    }
}
