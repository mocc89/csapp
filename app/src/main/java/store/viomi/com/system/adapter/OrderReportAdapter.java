package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.OrderData;

/**
 * Created by viomi on 2016/11/8.
 */

public class OrderReportAdapter extends BaseAdapter {

    private List<OrderData> list;
    private Context context;
    private LayoutInflater inflater;

    public OrderReportAdapter(List<OrderData> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_list_item, null);
            holder = new ViewHolder();

            holder.orderCode = (TextView) convertView.findViewById(R.id.orderCode);
            holder.linkmanName = (TextView) convertView.findViewById(R.id.linkmanName);
            holder.linkmanPhone = (TextView) convertView.findViewById(R.id.linkmanPhone);
            holder.createdTime = (TextView) convertView.findViewById(R.id.createdTime);
            holder.price = (TextView) convertView.findViewById(R.id.price);
            holder.orderNO = (TextView) convertView.findViewById(R.id.orderNO);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        OrderData data = list.get(position);
        holder.orderCode.setText("订单编号：" + data.getOrderCode());
        holder.linkmanName.setText(data.getLinkmanName() + "  |  ");
        holder.linkmanPhone.setText(data.getLinkmanPhone());
        holder.createdTime.setText(data.getCreatedTime());
        holder.price.setText("¥ " + data.getPrice());
        holder.orderNO.setText("" + (position + 1));

        return convertView;
    }

    class ViewHolder {
        TextView orderCode, linkmanName, linkmanPhone, createdTime, price, orderNO;
    }
}
