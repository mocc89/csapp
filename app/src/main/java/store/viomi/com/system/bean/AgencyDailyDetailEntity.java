package store.viomi.com.system.bean;

/**
 * Created by viomi on 2017/1/11.
 */

public class AgencyDailyDetailEntity {

    private String agencyName;
    private String cityName;
    private int userCount;
    private int terminalCount;
    private int staffCount;
    private int parttimeCount;
    private boolean isopen;
    private String date;
    private int orderCount;
    private double orderAmount;
    private int vType;
    private String paramer_title;
    private String selectParams;
    private double sortParam;


    public AgencyDailyDetailEntity(int vType, String paramer_title) {
        this.vType = vType;
        this.paramer_title = paramer_title;
    }

    public AgencyDailyDetailEntity(int vType, String date, String paramer_title) {
        this.vType = vType;
        this.date = date;
        this.paramer_title = paramer_title;
    }

    public AgencyDailyDetailEntity(int vType, String agencyName, String cityName, int userCount, int terminalCount, int staffCount, int parttimeCount, boolean isopen, int orderCount, double orderAmount, String selectParams, double sortParam) {
        this.vType = vType;
        this.agencyName = agencyName;
        this.cityName = cityName;
        this.userCount = userCount;
        this.terminalCount = terminalCount;
        this.staffCount = staffCount;
        this.parttimeCount = parttimeCount;
        this.isopen = isopen;
        this.orderCount = orderCount;
        this.orderAmount = orderAmount;
        this.selectParams = selectParams;
        this.sortParam = sortParam;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public int getTerminalCount() {
        return terminalCount;
    }

    public void setTerminalCount(int terminalCount) {
        this.terminalCount = terminalCount;
    }

    public int getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(int staffCount) {
        this.staffCount = staffCount;
    }

    public int getParttimeCount() {
        return parttimeCount;
    }

    public void setParttimeCount(int parttimeCount) {
        this.parttimeCount = parttimeCount;
    }

    public boolean isopen() {
        return isopen;
    }

    public void setIsopen(boolean isopen) {
        this.isopen = isopen;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public int getvType() {
        return vType;
    }

    public void setvType(int vType) {
        this.vType = vType;
    }

    public String getParamer_title() {
        return paramer_title;
    }

    public void setParamer_title(String paramer_title) {
        this.paramer_title = paramer_title;
    }

    public String getSelectParams() {
        return selectParams;
    }

    public void setSelectParams(String selectParams) {
        this.selectParams = selectParams;
    }

    public double getSortParam() {
        return sortParam;
    }

    public void setSortParam(double sortParam) {
        this.sortParam = sortParam;
    }
}
