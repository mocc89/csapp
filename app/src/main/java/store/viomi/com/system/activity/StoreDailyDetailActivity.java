package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.StoreDailyEntity;

public class StoreDailyDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_daily_detail);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        StoreDailyEntity entity = (StoreDailyEntity) extras.getSerializable("item");

        ImageView back = (ImageView) findViewById(R.id.back);
        TextView time = (TextView) findViewById(R.id.time);
        TextView store_name = (TextView) findViewById(R.id.store_name);
        TextView city = (TextView) findViewById(R.id.city);
        TextView agency = (TextView) findViewById(R.id.agency);
        TextView order = (TextView) findViewById(R.id.order);
        TextView sales = (TextView) findViewById(R.id.sales);
        TextView wx_menber = (TextView) findViewById(R.id.wx_menber);
        TextView new_saler = (TextView) findViewById(R.id.new_saler);
        TextView new_parttime_saler = (TextView) findViewById(R.id.new_parttime_saler);

        time.setText("数据更新时间：" + entity.getDay());
        store_name.setText(entity.getChannelName());
        city.setText(entity.getRootChannel());
        agency.setText(TextUtils.isEmpty(entity.getSecondChannel())?"-":entity.getSecondChannel());
        order.setText(entity.getOrderCount());
        sales.setText("¥ " + entity.getOrderAmount());
        wx_menber.setText(entity.getUserCount());
        new_saler.setText(entity.getStaffCount());
        new_parttime_saler.setText(entity.getParttimeCount());

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.finish();
    }
}
