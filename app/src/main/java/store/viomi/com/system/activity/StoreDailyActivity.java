package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.StoreDailyAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.StoreDailyEntity;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_store_daily)
public class StoreDailyActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;

    @ViewInject(R.id.storelist)
    private ListView storelist;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    //没有数据
    @ViewInject(R.id.nodata_layout)
    private RelativeLayout nodata_layout;
    @ViewInject(R.id.select_again)
    private TextView select_again;

    private List<StoreDailyEntity> slist;
    private StoreDailyAdapter adapter;

    private int currentPage = 1;
    private int pageSize = 50;

    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;   // 当前窗口可见项总数
    private int totalPageNum;           //总页数
    private boolean isFirsrPage = true;//是否第一页
    private boolean isFirsrRequest=true;

    private final int REQUSTCODE = 1111;
    private final int RESULTCODE = 1112;

    private String beginTime = "";
    private String endTime = "";
    private String channelName = "";
    private String disagency = "";

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi1", result);
                    parseJson(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }
                case 8: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    LogUtil.mlog("viomi1", result);
                    parseJson(result);
                    break;
                }

                case 9: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {
        slist = new ArrayList<>();
        loadFirstdata();
    }

    private void loadFirstdata() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.STOREDAILY);
        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);
        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);
        requestParams.addBodyParameter("channelName", channelName);//渠道名称
        requestParams.addBodyParameter("rootChannel", "");//所属城市运营名称
        requestParams.addBodyParameter("secondChannel", disagency);//所属分销商名称

        loading();
        RequstUtils.getRquest(requestParams, mhandler, 8, 9);
    }

    private void loaddata() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.STOREDAILY);
        requestParams.addBodyParameter("pageNum", "" + currentPage);
        requestParams.addBodyParameter("pageSize", "" + pageSize);
        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);
        requestParams.addBodyParameter("channelName", channelName);//渠道名称
        requestParams.addBodyParameter("rootChannel", "");//所属城市运营名称
        requestParams.addBodyParameter("secondChannel", disagency);//所属分销商名称

        loading();
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                currentPage++;

                JSONObject resultjson = JsonUitls.getJSONObject(json, "result");
                JSONArray list = JsonUitls.getJSONArray(resultjson, "list");

                if (isFirsrPage && list.length() == 0) {
                    nodata_layout.setVisibility(View.VISIBLE);
                    if (isFirsrRequest) {
                        select_again.setVisibility(View.GONE);
                    } else {
                        select_again.setVisibility(View.VISIBLE);
                    }
                    return;
                } else {
                    nodata_layout.setVisibility(View.GONE);
                }
                isFirsrRequest = false;

                for (int i = 0; i < list.length(); i++) {
                    JSONObject item = list.getJSONObject(i);
                    String day = JsonUitls.getString(item, "day");
                    String channelName = JsonUitls.getString(item, "channelName");//门店名
                    String userCount = JsonUitls.getString(item, "userCount");//微信新会员
                    String orderCount = JsonUitls.getString(item, "orderCount");//订单数
                    String rootChannel = JsonUitls.getString(item, "rootChannel");
                    String secondChannel = JsonUitls.getString(item, "secondChannel");
                    String orderAmount = JsonUitls.getString(item, "orderAmount");
                    String staffCount = JsonUitls.getString(item, "staffCount");
                    String parttimeCount = JsonUitls.getString(item, "parttimeCount");
                    StoreDailyEntity entity = new StoreDailyEntity(day, channelName, userCount, orderCount, rootChannel, secondChannel, orderAmount, staffCount, parttimeCount);
                    slist.add(entity);
                }

                if (isFirsrPage) {
                    totalPageNum = resultjson.getInt("totalPageNum");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isFirsrPage) {
            isFirsrPage = false;
            adapter = new StoreDailyAdapter(slist, this);
            storelist.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            storelist.setSelection(visibleLastIndex - visibleItemCount + 2);
        }
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoreDailyActivity.this, StoreDailySelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        select_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoreDailyActivity.this, StoreDailySelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        storelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(StoreDailyActivity.this, StoreDailyDetailActivity.class);
                intent.putExtra("item", slist.get(position));
                startActivity(intent);
            }
        });

        storelist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                StoreDailyActivity.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstdata();
            }
        });
    }

    private void loadmore() {
        loaddata();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUSTCODE && resultCode == RESULTCODE) {
            beginTime = data.getStringExtra("starttime");
            endTime = data.getStringExtra("endtime");
            channelName = data.getStringExtra("name");
            disagency = data.getStringExtra("disagency");

            visibleLastIndex = 0;
            visibleItemCount = 0;
            currentPage = 1;
            isFirsrPage = true;
            slist = new ArrayList<>();
            loadFirstdata();
        }
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
