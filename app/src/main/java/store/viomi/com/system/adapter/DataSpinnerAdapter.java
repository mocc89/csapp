package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;

/**
 * Created by zll on 2017/1/16.
 */

public class DataSpinnerAdapter extends BaseAdapter {
    private Context context;
    private List<String> list;
    private LayoutInflater inflater;

    public DataSpinnerAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
        if (this.list == null)
            this.list = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.my_spinner_layout, null);
        TextView tv = (TextView) convertView.findViewById(R.id.tv);
        tv.setText(list.get(position));
        return convertView;
    }

}
