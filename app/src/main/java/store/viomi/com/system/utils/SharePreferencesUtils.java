package store.viomi.com.system.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Mocc on 2017/8/29
 */

public class SharePreferencesUtils {

    private volatile static SharePreferencesUtils instance;

    private SharePreferencesUtils() {
    }

    public static SharePreferencesUtils getInstance() {
        if (instance == null) {
            synchronized (SharePreferencesUtils.class) {
                if (instance == null) {
                    instance = new SharePreferencesUtils();
                }
            }
        }
        return instance;
    }


    public String getUser_name(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String user_name = sp.getString("user_name", "");
        return user_name;
    }

    public void setUser_name(Context context, String user_name) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("user_name", user_name);
        editor.commit();
    }

    public String getChannel_name(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String channel_name = sp.getString("channel_name", "");
        return channel_name;
    }

    public void setChannel_name(Context context, String channel_name) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("channel_name", channel_name);
        editor.commit();
    }

    public String getToken(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String mytoken = sp.getString("token", "");
        LogUtil.mlog("getToken", mytoken);
        return mytoken;
//        return "bpsncnx8znnvjdsz";
    }

    public void setToken(Context context, String token) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("token", token);
        editor.commit();
    }

    public String getDownlink(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String downlink = sp.getString("downlink", "");
        return downlink;
    }

    public void setDownlink(Context context, String downlink) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("downlink", downlink);
        editor.commit();
    }

    public String getAppversion(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String appversion = sp.getString("appversion", "");
        return appversion;
    }

    public void setAppversion(Context context, String appversion) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("appversion", appversion);
        editor.commit();
    }


    public String getRole(Context context) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        String role = sp.getString("role", "");
        return role;
    }

    public void setRole(Context context, String role) {
        SharedPreferences sp = context.getSharedPreferences("mysp", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("role", role);
        editor.commit();
    }
}
