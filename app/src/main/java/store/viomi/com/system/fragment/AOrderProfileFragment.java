package store.viomi.com.system.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.OrderProfileAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.ProfileBean;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.widget.PinnedSectionListView;

import static com.github.mikephil.charting.utils.ColorTemplate.rgb;

/**
 * Created by viomi on 2016/11/11.
 */


@ContentView(R.layout.a_order_profile_fragment)
public class AOrderProfileFragment extends BaseFragment {

    @ViewInject(R.id.sale_fg_loading)
    private RelativeLayout sale_fg_loading;
    @ViewInject(R.id.pslistView)
    private PinnedSectionListView pslistView;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    private String typeNO;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasload;

    private boolean isdestroy = false;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0:
                    if (!isdestroy) {
                        loadingfinish();
                        hasload = true;
                        String result1 = (String) msg.obj;
                        LogUtil.mlog("viomi1", result1);
                        reconnect_layout.setVisibility(View.GONE);
                        parseJson(result1);
                    }
                    break;
                case 1:
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };


    public static Fragment getInstance(String type) {
        AOrderProfileFragment fragment = new AOrderProfileFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNO = arguments.getString("type", "null");
        }
        isPrepared = true;
        if (isVisible && isPrepared) {
            loadData(typeNO);
        }
    }


    private void loadData(String type) {
        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST1);
        requestParams2.addBodyParameter("type", type);
        loading();
        RequstUtils.getRquest(requestParams2, mhandler, 0, 1);
    }


    private void parseJson(String result) {

        List<ProfileBean> dataList = new ArrayList<>();
        dataList.add(new ProfileBean(0, "渠道名称", "销售金额", "订单数"));
        int orderCount = 0;
        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if ("900".equals(code)) {
                desc = "该功能暂未开通，敬请期待";
            }
            if (ResponseCode.isSuccess(code, desc)) {

                JSONArray subReport = JsonUitls.getJSONArray(mobBaseRes, "datas");
                for (int i = 0; i < subReport.length(); i++) {

                    JSONObject item = subReport.getJSONObject(i);
                    JSONObject channelInfo = JsonUitls.getJSONObject(item, "channelInfo");
                    JSONObject salesReportBean = JsonUitls.getJSONObject(item, "salesReportBean");

                    String name = JsonUitls.getString(channelInfo, "name");
                    int order = salesReportBean.getInt("orderCount");
                    double sales = salesReportBean.getDouble("turnOver");
                    dataList.add(new ProfileBean(1, name, order, sales));
                    orderCount += order;
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (orderCount > 0) {
            setMyAdapter(dataList, orderCount);
        } else {
            setNodataAdapter(dataList, orderCount);
        }


    }

    //订单总数为0
    private void setNodataAdapter(List<ProfileBean> dataList, int orderCount) {
        View head = LayoutInflater.from(getActivity()).inflate(R.layout.order_profile_nodata_header, null);
        pslistView.addHeaderView(head);
        OrderProfileAdapter adapter = new OrderProfileAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);
    }

    //订单总数为大于0
    private void setMyAdapter(List<ProfileBean> dataList, int orderCount) {

        //排序
        dataList = sortArray(dataList);

        View head = LayoutInflater.from(getActivity()).inflate(R.layout.order_profile_header, null);
        PieChart pieChart = (PieChart) head.findViewById(R.id.pieChart);


        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);


        pieChart.setCenterText(generateCenterSpannableText(orderCount + ""));

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(55f);
        pieChart.setTransparentCircleRadius(58f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);


        //饼图上不显示标签
        pieChart.setDrawEntryLabels(false);

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        int a;
        if (dataList.size() >= 5) {
            a = 5;
        } else {
            a = dataList.size();
        }
        int topfive = 0;
        boolean showOther = true;
        for (int i = 1; i < a; i++) {
            if (dataList.get(i).getOrder() > 0) {
                entries.add(new PieEntry(dataList.get(i).getOrder() * 2, dataList.get(i).getName()));
            } else {
                showOther = false;
            }
            topfive += dataList.get(i).getOrder();
        }
        if (showOther && (orderCount - topfive > 0)) {
            entries.add(new PieEntry((orderCount - topfive) * 2, "其他"));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(2.5f);
        dataSet.setSelectionShift(4.5f);

        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(rgb("#34495e"));
        colors.add(rgb("#ff4d1c"));
        colors.add(rgb("#1d8acb"));
        colors.add(rgb("#72c156"));
        colors.add(rgb("#fae603"));


//        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();


        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(25f);
        l.setYEntrySpace(5f);
        l.setYOffset(4f);
        l.setWordWrapEnabled(true);//设置图例文字自动排列

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        pieChart.setEntryLabelTextSize(12f);

        pslistView.addHeaderView(head);
        OrderProfileAdapter adapter = new OrderProfileAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);
    }


    private List<ProfileBean> sortArray(List<ProfileBean> dataList) {

        for (int i = 0; i < dataList.size() - 1; i++) {
            for (int j = 1; j < dataList.size() - 1 - i; j++) {
                if (dataList.get(j).getOrder() < dataList.get(j + 1).getOrder()) {
                    ProfileBean temp1 = dataList.get(j);
                    ProfileBean temp2 = dataList.get(j + 1);
                    dataList.set(j, temp2);
                    dataList.set(j + 1, temp1);
                }
            }
        }


        return dataList;
    }

    private SpannableString generateCenterSpannableText(String count) {
        String value = count + "\n订单";
        SpannableString s = new SpannableString(value);
        s.setSpan(new RelativeSizeSpan(2.0f), 0, s.length() - 2, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 0, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), s.length() - 2, s.length(), 0);
        s.setSpan(new RelativeSizeSpan(1.1f), s.length() - 2, s.length(), 0);
        return s;
    }


    //处理预加载
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            if (isVisible && isPrepared && !hasload) {
                loadData(typeNO);
            }
        } else {
            isVisible = false;
        }
    }


    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(typeNO);
            }
        });
    }

    @Override
    protected void loading() {
        sale_fg_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        sale_fg_loading.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestroy = true;
    }
}
