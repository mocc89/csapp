package store.viomi.com.system.base;

public class ProductBean {

    public String name;
    public String quantity;
    public String sales;


    public ProductBean(String name, String quantity, String sales) {
        this.name = name;
        this.quantity = quantity;
        this.sales = sales;
    }
}
