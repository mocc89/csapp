package store.viomi.com.system.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_forget_pw_code)
public class ForgetPwCodeActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.phone_num)
    private TextView phone_num;

    @ViewInject(R.id.input_code)
    private EditText input_code;

    @ViewInject(R.id.done)
    private Button done;

    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;


    private String phoneNum;
    private final static int RESPONSECODE = 1004;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case 0: {
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJson(result);
                    break;
                }
                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {
        Intent intent = getIntent();
        phoneNum = "";

        if (intent != null) {
            phoneNum = intent.getStringExtra("phone_num");
        }
        phone_num.setText(phoneNum);

    }

    private void checkCode() {
        String code = input_code.getText().toString();
        if (code.isEmpty()) {
            ToastUtil.show(HintText.CANNOTEMPTY);
            return;
        }

        RequestParams requestParams = RequstUtils.getNoTokenInstance(MURL.RESTPASSWORD);
        requestParams.addBodyParameter("mobile", phoneNum);
        requestParams.addBodyParameter("authCode", code);

        loading();
        RequstUtils.postRquest(requestParams, mhandler, 0, 1);
    }

    private void parseJson(String result) {
        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            if (ResponseCode.isSuccess(code, desc)) {
                showDialog();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.password_reset_dialog_layout, null);
        final Dialog dialog = new Dialog(this,R.style.selectorDialog);
        dialog.setContentView(view);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        dialog.setCancelable(false);
        dialog.show();
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                setResult(RESPONSECODE);
                finish();
            }
        });
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCode();
            }
        });

    }


    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
    }
}
