package store.viomi.com.system.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.OrderReportAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.OrderData;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;

@ContentView(R.layout.activity_order_report)
public class OrderReportActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;

    @ViewInject(R.id.orderlist)
    private ListView orderlist;

    @ViewInject(R.id.order_count)
    private TextView order_count;

    @ViewInject(R.id.loading)
    private RelativeLayout loading;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;

    //没有数据
    @ViewInject(R.id.nodata_layout)
    private RelativeLayout nodata_layout;
    @ViewInject(R.id.select_again)
    private TextView select_again;


    private int visibleLastIndex = 0;   //最后的可视项索引
    private int visibleItemCount = 0;       // 当前窗口可见项总数

    private int currentPage = 1;
    private int pageSize = 50;

    private String beginTime = "";
    private String endTime = "";

    private boolean isFirsrPage = true;

    private List<OrderData> datalist;

    private int totalCount;
    private int totalPageNum;
    private OrderReportAdapter adapter;

    private boolean isFirsrRequest = true;//是否第一次加载


    private final int REQUSTCODE = 1005;
    private final int RESULTCODE = 1006;

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {

                case 0: {
                    currentPage++;
                    loadingfinish();
                    String result = (String) msg.obj;
                    parseJSON(result);
                    break;
                }

                case 1: {
                    loadingfinish();
                    ResponseCode.onErrorHint(msg.obj);
                    break;
                }

                case 8: {
                    currentPage++;
                    loadingfinish();
                    reconnect_layout.setVisibility(View.GONE);
                    String result = (String) msg.obj;
                    parseJSON(result);
                    break;
                }

                case 9: {
                    loadingfinish();
                    reconnect_layout.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }
    };


    @Override
    protected void init() {
        datalist = new ArrayList<>();
        loadFirstList();
    }

    private void loadFirstList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ORDERDATALIST);

        requestParams.addBodyParameter("pageNum", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");

        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);

        requestParams.addBodyParameter("channelId", "");
        requestParams.addBodyParameter("channelType", "");

        requestParams.addBodyParameter("orderCode", "");

        requestParams.addBodyParameter("invoiceStatus", "");
        requestParams.addBodyParameter("needInvoice", "");

        requestParams.addBodyParameter("payStatus", "");
        requestParams.addBodyParameter("settleStatus", "");

        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 8, 9);

    }


    private void loadList() {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ORDERDATALIST);

        requestParams.addBodyParameter("pageNum", currentPage + "");
        requestParams.addBodyParameter("pageSize", pageSize + "");

        requestParams.addBodyParameter("beginTime", beginTime);
        requestParams.addBodyParameter("endTime", endTime);

        requestParams.addBodyParameter("channelId", "");
        requestParams.addBodyParameter("channelType", "");

        requestParams.addBodyParameter("orderCode", "");

        requestParams.addBodyParameter("invoiceStatus", "");
        requestParams.addBodyParameter("needInvoice", "");

        requestParams.addBodyParameter("payStatus", "");
        requestParams.addBodyParameter("settleStatus", "");

        loading();

        Callback.Cancelable cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }


    private void parseJSON(String result) {

        try {
            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess(code, desc)) {

                JSONObject resultJSON = JsonUitls.getJSONObject(mobBaseRes, "result");
                JSONArray list = JsonUitls.getJSONArray(resultJSON, "list");

                if (isFirsrPage && list.length() == 0) {
                    nodata_layout.setVisibility(View.VISIBLE);
                    if (isFirsrRequest) {
                        select_again.setVisibility(View.GONE);
                    } else {
                        select_again.setVisibility(View.VISIBLE);
                    }
                    return;
                } else {
                    nodata_layout.setVisibility(View.GONE);
                }
                isFirsrRequest = false;

                for (int i = 0; i < list.length(); i++) {
                    JSONObject data = list.getJSONObject(i);
                    String orderCode = JsonUitls.getString(data, "orderCode");
                    String linkmanName = JsonUitls.getString(data, "payTypeDesc");
                    String linkmanPhone = JsonUitls.getString(data, "dealPhaseDesc");
                    String price = JsonUitls.getString(data, "paymentPrice");

                    long time = data.getLong("createdTime");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String createdTime = sdf.format(new Date(time));

                    OrderData orderData = new OrderData(orderCode, linkmanName, linkmanPhone, createdTime, price);
                    datalist.add(orderData);
                }

                if (isFirsrPage) {
                    totalCount = resultJSON.getInt("totalCount");
                    totalPageNum = resultJSON.getInt("totalPageNum");
                }

                if (isFirsrPage) {
                    setView();
                } else {
                    adapter.notifyDataSetChanged();
                    orderlist.setSelection(visibleLastIndex - visibleItemCount + 2);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setView() {
        isFirsrPage = false;
        order_count.setText("" + totalCount);
        adapter = new OrderReportAdapter(datalist, this);
        orderlist.setAdapter(adapter);
    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderReportActivity.this, OrderSelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        select_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderReportActivity.this, OrderSelectActivity.class);
                startActivityForResult(intent, REQUSTCODE);
            }
        });

        orderlist.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex && currentPage <= totalPageNum) {
                    //如果是自动加载,可以在这里放置异步加载数据的代码
                    loadmore();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                OrderReportActivity.this.visibleItemCount = visibleItemCount;
                visibleLastIndex = firstVisibleItem + visibleItemCount - 1;

            }
        });

        orderlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(OrderReportActivity.this, OrderDetailActivity.class);
                String order = datalist.get((int) id).getOrderCode();
                intent.putExtra("ordercode", order);
                startActivity(intent);
            }
        });

        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFirstList();
            }
        });

    }

    private void loadmore() {
        loadList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUSTCODE && resultCode == RESULTCODE) {
            beginTime = data.getStringExtra("starttime");
            endTime = data.getStringExtra("endtime");

            visibleLastIndex = 0;
            visibleItemCount = 0;

            currentPage = 1;

            isFirsrPage = true;
            datalist = new ArrayList<>();
            loadFirstList();
        }
    }

    @Override
    protected void loading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading.setVisibility(View.GONE);
    }
}
