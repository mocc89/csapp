package store.viomi.com.system.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by hailang on 2018/3/27 0027.
 */

public class ProductCat implements Parcelable {
    /**
     * {
     * {
     * "id": 306,
     * "code": null,
     * "value": "智能冰箱",
     * "type": null,
     * "status": null,
     * "parentId": 304,
     * "catalogPic": "https://cnbj2.fds.api.xiaomi.com/viomi-official/images/vmall/category/0000未知图.jpg"
     * }
     */
    public int id;
    public String code;
    public String value;
    public String type;
    public String catalogPic;
    public String status;
    public long parentId;

    public boolean selected;

    protected ProductCat(Parcel in) {
        id = in.readInt();
        code = in.readString();
        value = in.readString();
        type = in.readString();
        catalogPic = in.readString();
        status = in.readString();
        parentId = in.readLong();
        selected = in.readByte() != 0;
    }

    public ProductCat() {
    }

    public static final Creator<ProductCat> CREATOR = new Creator<ProductCat>() {
        @Override
        public ProductCat createFromParcel(Parcel in) {
            return new ProductCat(in);
        }

        @Override
        public ProductCat[] newArray(int size) {
            return new ProductCat[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(code);
        parcel.writeString(value);
        parcel.writeString(type);
        parcel.writeString(catalogPic);
        parcel.writeString(status);
        parcel.writeLong(parentId);
        parcel.writeByte((byte) (selected ? 1 : 0));
    }
}
