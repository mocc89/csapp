package store.viomi.com.system.fragment.c;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.SalesReportAdapter;
import store.viomi.com.system.base.BaseFragment;
import store.viomi.com.system.bean.SalesData;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.LogUtil;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.widget.PinnedSectionListView;

/**
 * Created by viomi on 2016/10/18.
 */


@ContentView(R.layout.c_sales_fragment)
public class CSalesFragment extends BaseFragment {

    @ViewInject(R.id.sale_fg_loading)
    private RelativeLayout sale_fg_loading;
    @ViewInject(R.id.pslistView)
    private PinnedSectionListView pslistView;

    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;


    private String typeNO;
    private boolean isVisible;
    private boolean isPrepared;
    private boolean hasload;
    private Callback.Cancelable cancelable;

    private boolean isdestroy;


    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (!isdestroy) {
                switch (msg.what) {
                    case 0:
                        loadingfinish();
                        reconnect_layout.setVisibility(View.GONE);
                        String result1 = (String) msg.obj;
                        LogUtil.mlog("viomi1", result1);
                        parseJson(result1);
                        break;
                    case 1:
                        loadingfinish();
                        reconnect_layout.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    };


    public static Fragment getInstance(String type) {
        CSalesFragment fragment = new CSalesFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected void init() {

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeNO = arguments.getString("type", "null");
        }
        isPrepared = true;
        if (isVisible && isPrepared) {
            loadData(typeNO);
        }
    }

    private void loadData(String type) {
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.SALESREPORTLIST);
        requestParams.addBodyParameter("type", type);
        loading();
        cancelable = RequstUtils.getRquest(requestParams, mhandler, 0, 1);
    }

    private void parseJson(String result) {

        hasload = true;

        String order_count_txt = "0";
        String sales_count_txt = "0";
        String percent_txt = "0";
        String sales_fee_txt = "0";
        String share_percent_txt = "0";
        List<SalesData> dataList = new ArrayList<>();

        dataList.add(new SalesData(0, "渠道名称", "订单数", "销售金额", "提成金额"));

        try {

            JSONObject json = new JSONObject(result);
            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");

            if (ResponseCode.isSuccess(code, desc)) {

                JSONArray subReport = JsonUitls.getJSONArray(mobBaseRes, "subReport");
                for (int i = 0; i < subReport.length(); i++) {
                    JSONObject item = subReport.getJSONObject(i);
                    JSONObject channelInfo = JsonUitls.getJSONObject(item, "channelInfo");
                    JSONObject salesReportBean = JsonUitls.getJSONObject(item, "salesReportBean");

                    String name = JsonUitls.getString(channelInfo, "name");
                    String order = JsonUitls.getString(salesReportBean, "orderCount");
                    String sales = "¥ " + JsonUitls.getString(salesReportBean, "turnOver");
                    String percentage = "¥ " + JsonUitls.getString(salesReportBean, "profit");
                    SalesData data = new SalesData(1, name, order, sales, percentage);
                    dataList.add(data);
                }

                JSONObject resultJson = JsonUitls.getJSONObject(mobBaseRes, "result");

                order_count_txt = JsonUitls.getString(resultJson, "orderCount");
                sales_count_txt = "¥ " + JsonUitls.getString(resultJson, "turnOver");
                percent_txt = "¥ " + JsonUitls.getString(resultJson, "profit");
                sales_fee_txt = "¥ " + JsonUitls.getString(resultJson, "salesFee");
                share_percent_txt = "¥ " + JsonUitls.getString(resultJson, "participateProfit");

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        setMyAdapter(order_count_txt, sales_count_txt, percent_txt, sales_fee_txt, share_percent_txt, dataList);


    }

    private void setMyAdapter(String order_count_txt, String sales_count_txt, String percent_txt, String sales_fee_txt, String share_percent_txt, List<SalesData> dataList) {
        View headerView = LayoutInflater.from(getActivity()).inflate(R.layout.csales_report_header, null);

        //订单数量
        TextView order_count = (TextView) headerView.findViewById(R.id.order_count);
        //销售金额
        TextView sales_count = (TextView) headerView.findViewById(R.id.sales_count);
        //提成金额
        TextView percent = (TextView) headerView.findViewById(R.id.percent);
        //销售费用
        TextView sales_free = (TextView) headerView.findViewById(R.id.sales_fee);
        //分享提成
        TextView share_percent = (TextView) headerView.findViewById(R.id.share_percent);

        order_count.setText(order_count_txt);
        sales_count.setText(sales_count_txt);
        percent.setText(percent_txt);
        sales_free.setText(sales_fee_txt);
        share_percent.setText(share_percent_txt);

        pslistView.addHeaderView(headerView);

        SalesReportAdapter adapter = new SalesReportAdapter(getActivity(), dataList);
        pslistView.setAdapter(adapter);

    }


    @Override
    protected void initListener() {
        reconnect_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadData(typeNO);
            }
        });
    }


    //处理预加载
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            if (isVisible && isPrepared && !hasload) {
                loadData(typeNO);
            }
        } else {
            isVisible = false;

        }
    }

    @Override
    protected void loading() {
        sale_fg_loading.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        sale_fg_loading.setVisibility(View.GONE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isdestroy = true;
        if (cancelable != null) {
            cancelable.cancel();
        }
    }
}
