package store.viomi.com.system.utils;

/**
 * Created by Mocc on 2017/9/4
 */

public class NumberUtil {

    //取两位
    public static String getValue2(Object o) {
        String os = String.format("%.2f", o);
        return os;
    }

    //取整
    public static String getValue0(Object o) {
        String os = String.format("%.0f", o);
        return os;
    }

//    //double强转float
//    public static float double2Float(double d1) {
//
//        if (d1>2147483647||d1<-2147483648) {
//            return (float) d1;
//        }
//
//        int castInt = (int) d1;
//        double d2=d1-castInt;
//        float result = castInt + ((float) d2);
//        return result;
//    }
}
