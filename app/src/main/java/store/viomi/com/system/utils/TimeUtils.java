package store.viomi.com.system.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by viomi on 2017/1/5.
 */

public class TimeUtils {

    public static String stampToMMdd(long stamp) {
        Timestamp ts = new Timestamp(stamp);
        String tsStr = "";
        DateFormat sdf = new SimpleDateFormat("MM/dd");
        try {
            tsStr = sdf.format(ts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tsStr;
    }

    public static String stampToyyyyMMdd(long stamp) {
        Timestamp ts = new Timestamp(stamp);
        String tsStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            tsStr = sdf.format(ts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tsStr;
    }

}
