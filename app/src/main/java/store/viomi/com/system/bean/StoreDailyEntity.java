package store.viomi.com.system.bean;

import java.io.Serializable;

/**
 * Created by viomi on 2016/11/21.
 */

public class StoreDailyEntity implements Serializable {

    private String day;
    private String channelName;
    private String userCount;
    private String orderCount;
    private String rootChannel;
    private String secondChannel;
    private String orderAmount;
    private String staffCount;
    private String parttimeCount;

    public StoreDailyEntity() {
    }

    public StoreDailyEntity(String day, String channelName, String userCount, String orderCount, String rootChannel, String secondChannel, String orderAmount, String staffCount, String parttimeCount) {
        this.day = day;
        this.channelName = channelName;
        this.userCount = userCount;
        this.orderCount = orderCount;
        this.rootChannel = rootChannel;
        this.secondChannel = secondChannel;
        this.orderAmount = orderAmount;
        this.staffCount = staffCount;
        this.parttimeCount = parttimeCount;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUserCount() {
        return userCount;
    }

    public void setUserCount(String userCount) {
        this.userCount = userCount;
    }

    public String getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(String orderCount) {
        this.orderCount = orderCount;
    }

    public String getRootChannel() {
        return rootChannel;
    }

    public void setRootChannel(String rootChannel) {
        this.rootChannel = rootChannel;
    }

    public String getSecondChannel() {
        return secondChannel;
    }

    public void setSecondChannel(String secondChannel) {
        this.secondChannel = secondChannel;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(String staffCount) {
        this.staffCount = staffCount;
    }

    public String getParttimeCount() {
        return parttimeCount;
    }

    public void setParttimeCount(String parttimeCount) {
        this.parttimeCount = parttimeCount;
    }
}
