package store.viomi.com.system.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.bean.ProfileBean;
import store.viomi.com.system.utils.NumberUtil;
import store.viomi.com.system.widget.PinnedSectionListView;

/**
 * Created by viomi on 2016/11/11.
 */

public class SalesProfileAdapter extends BaseAdapter implements PinnedSectionListView.PinnedSectionListAdapter {

    private Context context;
    private List<ProfileBean> list;
    private LayoutInflater inflater;

    public SalesProfileAdapter(Context context, List<ProfileBean> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public boolean isItemViewTypePinned(int viewType) {
        return viewType == 0;
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sales_profile_item, null);
            holder = new ViewHolder();
            holder.item = (LinearLayout) convertView.findViewById(R.id.item);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.sales = (TextView) convertView.findViewById(R.id.sales);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (position % 2 == 0) {
            holder.item.setBackgroundResource(R.color.item_white);
        } else {
            holder.item.setBackgroundResource(R.color.item_dark);
        }

        if (position != 0) {
            holder.name.setTextColor(context.getResources().getColor(R.color.sales_txt));
            holder.sales.setTextColor(context.getResources().getColor(R.color.sales_txt));
            holder.sales.setText("¥ " + NumberUtil.getValue2(list.get(position).getSales()));
        } else {
            holder.sales.setText(list.get(position).getSalestitle());
        }

        holder.name.setText(list.get(position).getName());

        if (list.get(position).getType() == 2) {
            convertView.setVisibility(View.INVISIBLE);
        } else {
            convertView.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    class ViewHolder {
        LinearLayout item;
        TextView name, sales;
    }
}
