package store.viomi.com.system.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.Advance2Adapter;
import store.viomi.com.system.adapter.PuductAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.base.ChannelPurchaseOrderTotalDTO;
import store.viomi.com.system.bean.ChannelPurchaseOrderDetailResp;
import store.viomi.com.system.bean.ChannelPurchaseOrderResp;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.ListviewRelayoutUtils;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.StringUtil;
import store.viomi.com.system.widget.HorizontalListView;
import store.viomi.com.system.widget.NoScroListView;
import store.viomi.com.system.widget.VerScroView;

@ContentView(R.layout.activity_advance_order_2_test)
public class AdvanceOrder2Activity_test extends BaseActivity {
    final int REQUEAST_FILTER = 11;
    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.select)
    private ImageView select;
    @ViewInject(R.id.tv_time)
    TextView tvTime;

    @ViewInject(R.id.tv1)
    private TextView tv1;
    @ViewInject(R.id.tv2)
    private TextView tv2;

    //订单数
    @ViewInject(R.id.orderNum)
    TextView orderNum;
    //提货款
    @ViewInject(R.id.orderFee)
    TextView orderFee;
    //退单数
    @ViewInject(R.id.refundOrderNum)
    TextView refundOrderNum;
    //退货款
    @ViewInject(R.id.refundOrderFee)
    TextView refundOrderFee;

    @ViewInject(R.id.lv_bottom)
    NoScroListView noScroListView;
    @ViewInject(R.id.ver_scrollView)
    VerScroView ver_scrollView;

    @ViewInject(R.id.line1)
    private View line1;
    @ViewInject(R.id.line2)
    private View line2;


    @ViewInject(R.id.lv_horizon)
    private HorizontalListView listView;
    @ViewInject(R.id.lv_horizon_er)
    private HorizontalListView listViewEr;

    //加载中布局
    @ViewInject(R.id.loading_layout)
    private RelativeLayout loading_layout;
    //断网重新链接
    @ViewInject(R.id.reconnect_layout)
    private RelativeLayout reconnect_layout;
    @ViewInject(R.id.reconnect_btn)
    private TextView reconnect_btn;
    @ViewInject(R.id.tv_all)
    private TextView tvAll;
    @ViewInject(R.id.tv_item)
    private TextView tvItem;
    @ViewInject(R.id.tv_total_count)
    private TextView tvTotalCount;
    @ViewInject(R.id.tv_untotal_count)
    private TextView tvUntotalCount;

    private boolean isdestroy = false;
    long startTime, endtime;
    int type = 3;
    /**
     * 最大值
     */
    private long numCount = 0;
    private long numCountER = 0;


    //列表数据
    private List<ChannelPurchaseOrderResp> chartlist = new ArrayList<>();
    //底部列表数据
    private List<ChannelPurchaseOrderDetailResp> aelist = new ArrayList<>();

    PuductAdapter puductAdapter;
    PuductAdapter puductAdapterER;


    private Handler mhandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            if (!isdestroy) {
                switch (message.what) {
                    case 0: {

                        chartlist.clear();
                        if (puductAdapter != null)
                            puductAdapter.notifyDataSetChanged();
                        if (puductAdapterER != null)
                            puductAdapterER.notifyDataSetChanged();
                        String result1 = (String) message.obj;
                        reconnect_layout.setVisibility(View.GONE);
                        parseJson(result1);
                        loadingfinish();
                        break;
                    }
                    case 1: {
//                        reconnect_layout.setVisibility(View.VISIBLE);
                        runing = false;
                        ResponseCode.onErrorHint(message.obj);
                        loadingfinish();
                        break;
                    }
                    case 2: {
                        runing = false;
                        if (pageNum == 1) {
                            aelist.clear();
                            if (btoAdapter != null)
                                btoAdapter.notifyDataSetChanged();
                        }
                        String result = (String) message.obj;
                        parseJsonList(result);
                        ListviewRelayoutUtils.reLayout(noScroListView);
                        pageNum++;
                        loadingfinish();
                        break;
                    }
                    case 3: {
                        loadingfinish();
                        ResponseCode.onErrorHint(message.obj);
                        break;
                    }
                }
            }
            return false;
        }
    });
    private ChannelPurchaseOrderTotalDTO totalDTO;

    private void loadList() {
        loading();
        //图表数据
        RequestParams requestParams = RequstUtils.getHasTokenInstance(MURL.ORDERCHART);
        requestParams.addBodyParameter("orderType", "1");
        String timeType = "day";
        if (type == 3) {
            timeType = "day";
        } else if (type == 2) {
            timeType = "month";
        } else if (type == 1) {
            timeType = "year";
        }
        requestParams.addBodyParameter("timeType", timeType);
        requestParams.addBodyParameter("beginTime", startTime + "");
        requestParams.addBodyParameter("endTime", endtime + "");
        RequstUtils.getRquest(requestParams, mhandler, 0, 1);

    }

    int pageNum = 1, totalPageNum = 1;
    boolean runing;

    private void loadBotData() {
        //报表明细数据
        if (pageNum > totalPageNum) {
            return;
        }
        if (runing)
            return;
        runing = true;
        RequestParams requestParams2 = RequstUtils.getHasTokenInstance(MURL.ORDERCHARTDETAIL);
        requestParams2.addBodyParameter("orderType", "1");
        requestParams2.addBodyParameter("beginTime", startTime + "");
        requestParams2.addBodyParameter("endTime", endtime + "");
        requestParams2.addBodyParameter("pageNum", pageNum + "");
        requestParams2.addBodyParameter("pageSize", "50");
        loading();
        RequstUtils.getRquest(requestParams2, mhandler, 2, 1);
    }

    @Override
    protected void init() {
        setNavigation(tv1, tv2, line1, line2, 1);
        endtime = System.currentTimeMillis() / 1000L;
        startTime = endtime - 60L * 60L * 24L * 30L;
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            startTime = sdf1.parse(sdf1.format(new Date(startTime * 1000L))).getTime() / 1000L;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        refreshTimeView();
        loadList();
    }

    void refreshTimeView() {
        if (type == 1) {
            tvTime.setText("按年显示");
        } else if (type == 2) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            tvTime.setText(format.format(new Date(startTime * 1000)) + " 至 " + format.format(new Date(endtime * 1000)));
        } else if (type == 3) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            tvTime.setText(format.format(new Date(startTime * 1000)) + " 至 " + format.format(new Date(endtime * 1000)));
        }
    }

    private void parseJson(String result1) {
        try {
            JSONObject json = new JSONObject(result1);

            JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
            String code = JsonUitls.getString(mobBaseRes, "code");
            String desc = JsonUitls.getString(mobBaseRes, "desc");
            JSONObject result = JsonUitls.getJSONObject(mobBaseRes, "result");
            JSONObject totalData = JsonUitls.getJSONObject(result, "totalData");
            JSONArray listData = JsonUitls.getJSONArray(result, "listData");
            if (!ResponseCode.isSuccess2(code, desc)) {
                return;
            }
            if (totalData != null) {
                totalDTO = new ChannelPurchaseOrderTotalDTO();
                totalDTO.orderFee = totalData.optString("orderFee");
                totalDTO.orderNum = totalData.optString("orderNum");
                totalDTO.refundOrderFee = totalData.optString("refundOrderFee");
                totalDTO.refundOrderNum = totalData.optString("refundOrderNum");
                totalDTO.waresNum = totalData.optString("waresNum");
            }
            if (listData != null)
                for (int i = 0; i < listData.length(); i++) {
                    JSONObject object = listData.getJSONObject(i);
                    ChannelPurchaseOrderResp resp = new ChannelPurchaseOrderResp();
                    if (type == 3) {
                        resp.dataTime = StringUtil.getDateDay(object.optString("dataTime"));
                    }

                    resp.orderFee = object.optString("orderFee");
                    resp.orderNum = object.optString("orderNum");
                    resp.refundOrderFee = object.optString("refundOrderFee");
                    resp.refundOrderNum = object.optString("refundOrderNum");
                    resp.waresNum = object.optString("waresNum");
                    if ((Long.parseLong(resp.orderNum) + Long.parseLong(resp.refundOrderNum)) > numCount) {
                        numCount = Long.parseLong(resp.orderNum) + Long.parseLong(resp.refundOrderNum);
                    }
                    if ((Long.parseLong(resp.orderFee) + Long.parseLong(resp.refundOrderFee)) > numCountER) {
                        numCountER = Long.parseLong(resp.orderFee) + Long.parseLong(resp.refundOrderFee);
                    }
                    chartlist.add(resp);
                }
            setdata();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {

        }
    }

    Advance2Adapter btoAdapter;

    private void parseJsonList(String result1) {
        try {
            JSONObject json = new JSONObject(result1);
            json = json.optJSONObject("mobBaseRes");
            String code = JsonUitls.getString(json, "code");
            String desc = JsonUitls.getString(json, "desc");
            if (!ResponseCode.isSuccess2(code, desc)) {
                return;
            }
            JSONArray result = json.getJSONObject("result").getJSONArray("list");
            totalPageNum = json.optJSONObject("result").optInt("totalPageNum");
            if (result != null)
                for (int i = 0; i < result.length(); i++) {
                    JSONObject object = result.getJSONObject(i);
                    ChannelPurchaseOrderDetailResp resp = new ChannelPurchaseOrderDetailResp();
                    resp.channelId = object.optLong("channelId");
                    resp.orderFee = object.optLong("orderFee");
                    resp.orderNum = object.optLong("orderNum");
                    resp.productNum = object.optLong("productNum");
                    resp.channelName = object.optString("terminalName");
                    resp.secondChannel = object.optString("regionalAgentName");
                    resp.rootChannel = object.optString("cityAgentName");
                    aelist.add(resp);
                }
            if (btoAdapter == null) {
                btoAdapter = new Advance2Adapter(aelist, this);
                noScroListView.setAdapter(btoAdapter);
            } else {
                btoAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //设置数据更新UI
    private void setdata() {
        puductAdapter = new PuductAdapter(chartlist, this, numCount, 0);
        puductAdapterER = new PuductAdapter(chartlist, this, numCountER, 1);
        listView.setAdapter(puductAdapter);
        listViewEr.setAdapter(puductAdapterER);
        setDataTable();
        loadBotData();
    }

    private String subStr(String str) {
        String sub = "";
        if (str.length() > 5) {
            StringBuilder builder = new StringBuilder(str);
            builder.insert(str.length() - 4, ".");
            String s = builder.toString();
            sub = s.substring(0, s.length() - 2);
        }

        return sub;
    }

    @Override
    protected void initListener() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdvanceOrder2Activity_test.this, AdvanceOrder2FilterActivity.class);
                startActivityForResult(intent, REQUEAST_FILTER);
            }
        });

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv1, tv2, line1, line2, 1);
            }
        });

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNavigation(tv2, tv1, line2, line1, 2);
            }
        });
        tvAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTab = 0;
                setDataTable();
            }
        });

        tvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTab = 1;
                setDataTable();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                onListViewItemClick(position);
            }
        });
        listViewEr.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                onListViewItemClick(position);
            }
        });
        ver_scrollView.setOnOverScrolledListener(new VerScroView.onOverScrolledListener() {
            @Override
            public void onOverScrolled(int scrollY) {
                if (scrollY == 0) {

                } else {
                    loadBotData();
                }
            }
        });
    }

    int mvisibleItemCount, visibleLastIndex;

    void onListViewItemClick(int position) {
        for (ChannelPurchaseOrderResp resp : chartlist) {
            resp.selected = false;
        }
        chartlist.get(position).selected = true;
        puductAdapter.notifyDataSetChanged();
        puductAdapterER.notifyDataSetChanged();
        currentItem = chartlist.get(position);
        currentTab = 1;
        setDataTable();
    }

    ChannelPurchaseOrderResp currentItem;
    /**
     * 0显示全部 1显示选中数据
     */
    int currentTab;

    void setDataTable() {
        if (currentTab == 0) {
            tvAll.setTextColor(Color.BLACK);
            tvAll.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            tvItem.setTextColor(Color.parseColor("#666666"));
            tvItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            orderNum.setText("订单数: " + totalDTO.orderNum);
            orderFee.setText("提货额: ￥" + (totalDTO.orderFee.length() > 5 ? subStr(totalDTO.orderFee) : totalDTO.orderFee) + "万");

            refundOrderNum.setText("退货退单数: " + totalDTO.refundOrderNum);
            refundOrderFee.setText("退货退款额: ￥" + (totalDTO.refundOrderFee.length() > 5 ? subStr(totalDTO.refundOrderFee) : totalDTO.refundOrderFee) + "万");
        } else {
            if (currentItem == null)
                return;
            tvAll.setTextColor(Color.parseColor("#666666"));
            tvAll.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            tvItem.setTextColor(Color.BLACK);
            tvItem.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

            orderNum.setText("订单数: " + currentItem.orderNum);
            orderFee.setText("提货额: ￥" + (currentItem.orderFee.length() > 5 ? subStr(currentItem.orderFee) : currentItem.orderFee) + "万");
            refundOrderNum.setText("退货退单数: " + currentItem.refundOrderNum);
            refundOrderFee.setText("退货退款额: ￥" + (currentItem.refundOrderFee.length() > 5 ? subStr(currentItem.refundOrderFee) : currentItem.refundOrderFee) + "万");
        }

    }

    @Override
    protected void onDestroy() {
        isdestroy = true;
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == REQUEAST_FILTER && data != null) {
                startTime = data.getLongExtra("start", startTime);
                endtime = data.getLongExtra("end", endtime);
                type = data.getIntExtra("timeType", 3);
                pageNum = 1;
                loadList();
                loadBotData();
                refreshTimeView();
            }
        }
    }


    private void setNavigation(TextView tv1, TextView tv2, View line1, View line2, int type) {
        tv1.setTextColor(getResources().getColor(R.color.selectItem));
        tv2.setTextColor(getResources().getColor(R.color.unselectItem));
        line1.setVisibility(View.VISIBLE);
        line2.setVisibility(View.INVISIBLE);
        //切换表格
        if (type == 1) {
            tvTotalCount.setText("订单总数");
            tvUntotalCount.setText("退货退款订单数");
            listView.setVisibility(View.VISIBLE);
            listViewEr.setVisibility(View.GONE);
        } else {
            tvTotalCount.setText("订单总额");
            tvUntotalCount.setText("退货退款额");
            listView.setVisibility(View.GONE);
            listViewEr.setVisibility(View.VISIBLE);
        }

    }

//    private void parseJsonList(String result1) {
//        try {
//            JSONObject json = new JSONObject(result1);
//            json = json.optJSONObject("mobBaseRes");
//            String code = JsonUitls.getString(json, "code");
////            String desc = JsonUitls.getString(json, "desc");
//            JSONArray result = JsonUitls.getJSONArray(json, "result");
//            if (result != null)
//                for (int i = 0; i < result.length(); i++) {
//                    JSONObject object = result.getJSONObject(i);
//                    ChannelPurchaseOrderDetailResp resp = new ChannelPurchaseOrderDetailResp();
//                    resp.channelId = object.optLong("channelId");
//                    resp.orderFee = object.optLong("orderFee");
//                    resp.orderNum = object.optLong("orderNum");
//                    resp.productNum = object.optLong("productNum");
//                    resp.channelName = object.optString("channelName");
//                    resp.secondChannel = object.optString("secondChannel");
//                    resp.rootChannel = object.optString("rootChannel");
//                    aelist.add(resp);
//                }
//            adapter = new Advance2Adapter(aelist, getActivity());
//            orderlist.setAdapter(adapter);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void loading() {
        loading_layout.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_layout.setVisibility(View.GONE);
    }

}
