package store.viomi.com.system.activity;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import store.viomi.com.system.R;
import store.viomi.com.system.adapter.OrderDetailProAdapter;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.bean.OrderListBean;
import store.viomi.com.system.bean.OrderListProductBean;
import store.viomi.com.system.bean.ProductEntity;

@ContentView(R.layout.activity_order_detail_third)
public class OrderDetailThirdActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.order_Code)
    private TextView order_Code;

    @ViewInject(R.id.delivery_name)
    private TextView delivery_name;

    @ViewInject(R.id.delivery_address)
    private TextView delivery_address;

    @ViewInject(R.id.delivery_phone)
    private TextView delivery_phone;

    @ViewInject(R.id.productList)
    private ListView productList;

    @ViewInject(R.id.pay_method)
    private TextView pay_method;

    @ViewInject(R.id.pay_state)
    private TextView pay_state;

    @ViewInject(R.id.coupon)
    private TextView coupon;

    @ViewInject(R.id.order_carriage)
    private TextView order_carriage;

    @ViewInject(R.id.order_count)
    private TextView order_count;

    @ViewInject(R.id.created_time)
    private TextView created_time;

    @ViewInject(R.id.balance_status)
    private TextView balance_status;

    private OrderListBean bean;


    @Override
    protected void init() {
        Intent intent = getIntent();
        if (intent!=null) {

            bean = (OrderListBean) intent.getSerializableExtra("orderListBean");
        }

        setView();
    }

    private void setView() {
        order_Code.setText(bean.getOrderNO());
        delivery_name.setText(bean.getLinkmanName());
        delivery_address.setText(bean.getAddress());
        delivery_phone.setText(bean.getLinkmanPhone());
        pay_method.setText(bean.getPayMode());
        pay_state.setText(bean.getPayStatus());
        coupon.setText("¥ " + bean.getDiscount());
        order_carriage.setText("¥ " + bean.getDeliveryFee());
        order_count.setText("¥ " + bean.getPayPrice());
        created_time.setText(bean.getCreatedTime());
        balance_status.setText(bean.getSettleStatesDesc());

        List<ProductEntity> productEntityList = new ArrayList<>();
        List<OrderListProductBean> productBeanList = bean.getProductBeanList();
        if (productBeanList!=null) {
            for (int i = 0; i < productBeanList.size(); i++) {
                OrderListProductBean productBean = productBeanList.get(i);
                ProductEntity entity = new ProductEntity(productBean.getQuantity(), productBean.getPaymentPrice(), productBean.getName(), productBean.getImgUrl());
                productEntityList.add(entity);
            }
        }

        OrderDetailProAdapter adapter = new OrderDetailProAdapter(productEntityList, this);
        productList.setAdapter(adapter);
        resetListView(productList,adapter);
    }

    private void resetListView(ListView listView, OrderDetailProAdapter adapter) {
        int count = adapter.getCount();
        int totalHeight = 0;
        for (int i = 0; i < count; i++) {
            View view = adapter.getView(i, null, listView);
            view.measure(0, 0);
            int measuredHeight = view.getMeasuredHeight();
            totalHeight += measuredHeight;
        }
        ViewGroup.LayoutParams lp = listView.getLayoutParams();
        lp.height = totalHeight + listView.getDividerHeight() * (count - 1);
        listView.setLayoutParams(lp);
        listView.requestLayout();
    }

    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void loading() {

    }

    @Override
    protected void loadingfinish() {

    }
}
