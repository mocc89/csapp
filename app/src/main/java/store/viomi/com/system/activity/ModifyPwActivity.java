package store.viomi.com.system.activity;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import store.viomi.com.system.R;
import store.viomi.com.system.base.BaseActivity;
import store.viomi.com.system.constants.HintText;
import store.viomi.com.system.constants.MURL;
import store.viomi.com.system.utils.JsonUitls;
import store.viomi.com.system.utils.MD5Util;
import store.viomi.com.system.utils.RequstUtils;
import store.viomi.com.system.utils.ResponseCode;
import store.viomi.com.system.utils.SharePreferencesUtils;
import store.viomi.com.system.utils.ToastUtil;

@ContentView(R.layout.activity_modify_pw)
public class ModifyPwActivity extends BaseActivity {

    @ViewInject(R.id.back)
    private ImageView back;

    @ViewInject(R.id.account)
    private EditText account;

    @ViewInject(R.id.old_password)
    private EditText old_password;

    @ViewInject(R.id.new_password)
    private EditText new_password;

    @ViewInject(R.id.new_again_password)
    private EditText new_again_password;

    @ViewInject(R.id.modify)
    private Button modify;

    @ViewInject(R.id.loading_bg)
    private RelativeLayout loading_bg;

    private SharedPreferences sp;
    private String account_num;


    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            loadingfinish();

            String result = (String) msg.obj;
            if ("".equals(result)) {
                ResponseCode.onErrorHint(msg.obj);
                return;
            }

            try {
                JSONObject json = new JSONObject(result);
                JSONObject mobBaseRes = JsonUitls.getJSONObject(json, "mobBaseRes");
                String desc = JsonUitls.getString(mobBaseRes, "desc");
                String code = JsonUitls.getString(mobBaseRes, "code");

                if (ResponseCode.isSuccess(code, desc)) {
                    ToastUtil.show(HintText.MODIFYPASSWORDOK);

                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("md5String", MD5Util.getMD5String(new_password.getText().toString()));
                    editor.commit();
                    finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };

    @Override
    protected void init() {
        sp = getSharedPreferences("mysp", MODE_PRIVATE);

        account_num = sp.getString("account", "");
        account.setText(account_num);

    }


    private void modify() {

        String old_password_txt = old_password.getText().toString();
        String new_password_txt = new_password.getText().toString();
        String new_again_password_txt = new_again_password.getText().toString();


        if (TextUtils.isEmpty(old_password_txt)) {
            ToastUtil.show(HintText.EMPTYOLDPASSWORD);
            return;
        }

        if (TextUtils.isEmpty(new_password_txt)) {
            ToastUtil.show(HintText.EMPTYNEWPASSWORD);
            return;
        }

        Pattern p = Pattern.compile("^([a-zA-Z]|[a-zA-Z0-9_]|[0-9]){6,20}$");
        Matcher m = p.matcher(new_password_txt);

        if ( !m.matches()) {
            ToastUtil.show(HintText.MODIFYPASSWORD);
            return;
        }

        if (TextUtils.isEmpty(new_again_password_txt)) {
            ToastUtil.show(HintText.EMPTYNEWPASSWORD);
            return;
        }

        if (!new_password_txt.equals(new_again_password_txt)) {
            ToastUtil.show(HintText.NOTSAMEPASSWORD);
            return;
        }


        String url = MURL.MODIFYPASSWORD;
        url = url + "?token=" + SharePreferencesUtils.getInstance().getToken(this) + "&newPwd=" + MD5Util.getMD5String(new_password_txt) + "&oldPwd=" + MD5Util.getMD5String(old_password_txt);

        loading();

        final String finalUrl = url;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String result = RequstUtils.doPut(finalUrl);
                Message msg = mhandler.obtainMessage();
                msg.obj = result;
                mhandler.sendMessage(msg);
            }
        }).start();

    }


    @Override
    protected void initListener() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modify();
            }
        });
    }


    @Override
    protected void loading() {
        loading_bg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadingfinish() {
        loading_bg.setVisibility(View.GONE);
    }
}
