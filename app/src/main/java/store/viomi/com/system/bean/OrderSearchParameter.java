package store.viomi.com.system.bean;

import java.io.Serializable;

/**
 * Created by hailang on 2018/4/24 0024.
 */

public class OrderSearchParameter implements Serializable {
    /**
     * 0云米商城 1米家 2天猫 3预付款提货
     */
    public int souceType;
    public long start;//s
    public long end;//s
    public String orderNo = "";
    public int orderStatus;
    public String productName = "";
    public String sellerName= "";
    public Area city;
    public Area province;
    public Area district;
}
