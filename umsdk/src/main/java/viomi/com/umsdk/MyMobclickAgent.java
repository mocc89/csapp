package viomi.com.umsdk;

import android.content.Context;

import com.umeng.analytics.MobclickAgent;

/**
 * Created by Mocc on 2017/8/29
 */

public class MyMobclickAgent {

    //封装以便以后统一处理
    public static void onEvent(Context context, String event) {
        MobclickAgent.onEvent(context, event);
    }
}
